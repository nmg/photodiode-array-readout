# Components

## Capacitors

We will use the Johanson Technology Inc 251R07S0R2AV4T 200fF capacitor [digikey
page](https://www.digikey.com/product-detail/en/johanson-technology-inc/251R07S0R2AV4T/712-1655-6-ND/9658916).

This capacitor has the highest insulation resistance, > 100G$\Omega$, lowest
cost and tolerance we could find, that is in a 0402 sized package. 

Thus, the max leakage current in pico-amps is:

```{python, engine.path='/usr/bin/python3'}
max_voltage = 3.3 # Volt
resistance = 100e9 # Ohm
leakage_current = max_voltage / resistance
leakage_current_pico = leakage_current*1e12
```

ISL84051
ADG726
ADG794
251R07S0R2AV4T
AD8608











