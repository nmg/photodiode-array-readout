--- 
title: "Manual on the Design and Operation of Photodiode Array Support Circuitry"
author: "Nevo Magnezi"
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
output: bookdown::gitbook
documentclass: book
bibliography: [book.bib, packages.bib]
biblio-style: apalike
link-citations: yes
description: "This manual describes how readout/support circuit for a 32 by 32 photodiode array can be designed and fabricated on a printed circuit board (PCB)."
---

# Introduction

[VERY IMPORTANT LINK TO GDRIVE!!!!!!](https://drive.google.com/drive/folders/1aYqS-rXEfxdc2Ued9qQny9q3fGb2CRFm?usp=sharing)

This manual describes how readout/support circuitry for a 32 x 32 UV photodiode array, fabricated by CoolCAD Electronics LLC, is designed and fabricated using printed circuit board (PCB) technology. The purpose of this manual is to document the efforts that the author of this manual made towards designing the readout circuit for CoolCAD Electronics LLC, so that the engineers that follow will be able to decipher the work the author contributed to. 



Over time and many design iterations, several PCB designs have been created and
fabricated. All of these designs use the QFN-64 packaged photodiode arrays as a
central component. They just use the component differently. But what do these
photodiode arrays look like? A schematic for a 4x4 version of the array is as follows
(note that the actual array is 32x32, or in the future, 64x64):

![Photodiode Array](figures/photodiode_array.png)

For a 32x32 array, there would be 32 different connections to the p-side of the
photodiodes and 32 connections to the n-side of the photodiodes, bringing the
total number of photodiodes to $32 \times 32 = 1024$. To select a single
photodiode, one must select a particular p-side connection and a particular
n-side connection, and keep the remaining connections open (i.e. high
impedance). One of the main challenges of designing for these photodiode arrays
is that they generate on the order of pico-amps of current, which means that
keeping connections "open" means that they must be very high impedance, AKA,
very low leakage. 

The following is photomicrograph of one of the actual fabricated photodiode arrays: 

![Photomicrograph of Photodiode Array](figures/array_photomicrograph.jpg)

The PCBs that have been designed or fabricated at the time of this writing, in order from least complex to most complex, are: 

1. A "Simple Soldered Board," which simply extends the 64 pins  of the QFN-64
   package to header pins
2. A "Socketed Trans-Amplification Board with DIP Switches," which: 
    1. Extends the pins of the packaged array without requiring the package to
    be soldered to the PCB. 
    2. Allows individual photodiodes or multiple photodiodes in parallel to be
    selected using mechanical DIP switches
    3. Amplifies the photocurrent and converts it to a voltage using 
    a low-leakage op-amp and a resistive T feedback network
3. An "Arduino-compatible Photodiode Array Readout Board", which:
    1. Is designed to sit on top of the Arduino-compatible Sparkfun Redboard
    Turbo or comparable Arduino Uno microcontroller boards
    2. Can be controlled by the microcontroller to select a specfic photodiode,
    integrate its photocurrent across a capacitor, and convert the analog
    voltage to a digital signal to be processed by the microcontroller


## Prerequisites 

You will need:

- The project files, which can be found on GDrive [[link]](https://drive.google.com/drive/folders/1aYqS-rXEfxdc2Ued9qQny9q3fGb2CRFm?usp=sharing)
- KiCAD EDA, if you intend to modify PCB layout or schematics [[download]](http://kicad-pcb.org/download/)
- The Arduino IDE, if you intend to program the microcontroller (MCU) [[download]](https://www.arduino.cc/en/Main/Software)
    - You will need to ensure that you will be able to program the MCU
        - This manual uses the Sparkfun Redboard Turbo [[hook-up guide]](https://learn.sparkfun.com/tutorials/redboard-turbo-hookup-guide/all)
- GNU Octave [[download]](https://www.gnu.org/software/octave/), with the *instrument control* package [[download]](https://octave.sourceforge.io/instrument-control/index.html), if you intend to communicate with your microcontroller
- CoolSPICE, if you intend to simulate models of various circuits

