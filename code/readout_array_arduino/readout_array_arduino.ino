#include <assert.h>

#define ANALOG_WRITE_RESOLUTION 10
#define ANALOG_READ_RESOLUTION 12

#define NUM_COLS 4
#define NUM_ROWS 4

#define RESET_PIN 0
#define COL_DEC_PIN_MIN 1
#define COL_DEC_PIN_MAX 2
#define COL_EN_NOT 3

#define ROW_DEC_PIN_MIN 4
#define ROW_DEC_PIN_MAX 5
#define ROW_EN_NOT 6

#define DAC_PIN A0
#define ADC_PIN A1

#define VDD 3.3 // Volts
#define DIODE_BIAS 0.3 // Volts
#define DIODE_BIAS_INT ((DIODE_BIAS / VDD) * (pow(2, ANALOG_WRITE_RESOLUTION) -1))
#define CAPACITANCE 0.1e-6 // Farads
#define FREQUENCY_KHZ 10
#define DELTA_T_MICRO (1000 / (2 * FREQUENCY_KHZ))


unsigned int col_sel = NUM_COLS - 1;
unsigned int row_sel = NUM_ROWS - 1;

void setup() 
{

    SerialUSB.begin(9600);
 //   SerialUSB.println("Test: Define Statements");
//    test_defines();
    pinMode(RESET_PIN, OUTPUT);
    pinMode(ROW_EN_NOT, OUTPUT);
    pinMode(COL_EN_NOT, OUTPUT);
    digitalWrite(ROW_EN_NOT, HIGH);
    digitalWrite(COL_EN_NOT, HIGH);
    reset_amp();   

    for (unsigned int pin = COL_DEC_PIN_MIN; pin <= COL_DEC_PIN_MAX; pin++) {
        pinMode(pin, OUTPUT);    
        digitalWrite(pin, LOW);
    }

    for (unsigned int pin = ROW_DEC_PIN_MIN; pin <= ROW_DEC_PIN_MAX; pin++) {
        pinMode(pin, OUTPUT);    
        digitalWrite(pin, LOW);
    }

    // Write reverse bias for photodiode
    analogWriteResolution(ANALOG_WRITE_RESOLUTION);
    analogWrite(DAC_PIN, DIODE_BIAS_INT);

    analogReadResolution(ANALOG_READ_RESOLUTION);
/*    while (true) {
        test_reset_voltage();
    }
*/
}

void loop() 
{
    for (int i = 0; i < NUM_ROWS; i++) {
        change_row();
        for (int j = 0; j < NUM_COLS; j++) {
            change_col();
            //SerialUSB.print(reset_amp()); 
            reset_amp();
            int v = 0;
            //if (row_sel == 0 && col_sel == 0 ) {
            //    SerialUSB.print(", ");
                v = measure_amp();
                //SerialUSB.print(v); 
                print_with_padding(v);
                SerialUSB.print(",");
            //}
        }
    }
    SerialUSB.println("");
    
}

void print_with_padding(int num) 
{
    if (num < 10) {
        SerialUSB.print("000");
    }
    else if (num < 100) {
        SerialUSB.print("00");
    }
    else if (num < 1000) {
        SerialUSB.print("0");
    }
    SerialUSB.print(num);
}
    

/** Disables the Analog MUX, increments the selected col channel, and re-enables
    the Analog MUX
*/
void change_col()
{
    digitalWrite(COL_EN_NOT, HIGH);
    col_sel = (col_sel + 1) % NUM_COLS;
    for (int pin = COL_DEC_PIN_MIN; pin <= COL_DEC_PIN_MAX; pin++) {
        if (((int) pow(2, pin - COL_DEC_PIN_MIN)) & col_sel) {
            digitalWrite(pin, HIGH);
        }
        else {
            digitalWrite(pin, LOW);
        }
    }
    digitalWrite(COL_EN_NOT, LOW);
}

/** Disables the Analog MUX, increments the selected row channel, and re-enables
    the Analog MUX
*/
void change_row()
{
    digitalWrite(ROW_EN_NOT, HIGH);
    row_sel = (row_sel + 1) % NUM_ROWS;
    for (int pin = ROW_DEC_PIN_MIN; pin <= ROW_DEC_PIN_MAX; pin++) {
        if (((int) pow(2, pin - ROW_DEC_PIN_MIN)) & row_sel) {
            digitalWrite(pin, HIGH);
        }
        else {
            digitalWrite(pin, LOW);
        }
    }
    digitalWrite(ROW_EN_NOT, LOW);
}


//float measure_amp() 
unsigned int measure_amp() 
{
    digitalWrite(RESET_PIN, LOW);
    delayMicroseconds(DELTA_T_MICRO);
    return analogRead(ADC_PIN);// * VDD * 1.0 / (2^ANALOG_READ_RESOLUTION - 1);
}


/** Resets the Transimpedance amplifer capactor and returns a reading of 
    the output after DELTA_T_MICRO microseconds. The output that is read should
    be equal to the DIODE_BIAS. 
*/
//float reset_amp() 
unsigned int reset_amp() 
{
    digitalWrite(RESET_PIN, HIGH);
    delayMicroseconds(DELTA_T_MICRO);
    return analogRead(ADC_PIN);// * VDD * 1.0 / (2^ANALOG_READ_RESOLUTION - 1);
}

// Tests 
void test_defines()
{
    assert(pow(2,COL_DEC_PIN_MAX - COL_DEC_PIN_MIN + 1) == NUM_COLS);
    assert(pow(2, ROW_DEC_PIN_MAX - ROW_DEC_PIN_MIN + 1) == NUM_ROWS);
}

void test_reset_voltage()
{
    row_sel = NUM_ROWS - 1;
    col_sel = NUM_COLS - 1;
    for (int j = 0; j < NUM_ROWS; j++) {
        change_row();
        for (int i = 0; i < NUM_COLS; i++) {
            change_col();
            int measured_voltage = reset_amp();
            SerialUSB.print(measured_voltage);
            SerialUSB.print(",");

            //bool within_range = measured_voltage > 9 * DIODE_BIAS_INT / 10 &&
            //                    measured_voltage < 11 * DIODE_BIAS_INT / 10;
           // SerialUSB.println("Reset Voltage Test");
//            assert(within_range);
            //if (!within_range) {
             //   SerialUSB.println("Reset Voltage Non-functional"); 
           // }
            
        }
    }
    SerialUSB.print(";");
}


