EESchema Schematic File Version 4
LIBS:photodiode_readout_circuit_rev3-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 3 6
Title "Trans-impedance array"
Date "2019-03-28"
Rev "1"
Comp "CoolCAD Electronics, LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5150 1500 2    50   Output ~ 0
O2
Text HLabel 5150 2000 2    50   Output ~ 0
O4
Text HLabel 850  2200 0    50   Input ~ 0
N2
Text HLabel 850  2400 0    50   Input ~ 0
N4
$Comp
L 2019-04-26_20-18-08:ADG794BRQZ-REEL7 U11
U 1 1 5CF625EB
P 7450 3700
AR Path="/5CB2800A/5CF625EB" Ref="U11"  Part="1" 
AR Path="/5CF6E8CA/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5CF713F4/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5CF73EB0/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D0B9ED9/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D0B9EEB/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D0E2164/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D0E2176/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D115D42/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D115D5B/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D1494D0/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D1494E2/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D160D66/5CF625EB" Ref="U15"  Part="1" 
AR Path="/5D16695E/5CF625EB" Ref="U19"  Part="1" 
AR Path="/5D1DAFAE/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D1EFE44/5CF625EB" Ref="U23"  Part="1" 
F 0 "U11" H 8250 4187 60  0000 C CNN
F 1 "ADG794" H 8250 4081 60  0000 C CNN
F 2 "footprints:ADG794BRQZ-REEL7" H 8250 4040 60  0001 C CNN
F 3 "" H 7450 3700 60  0000 C CNN
	1    7450 3700
	1    0    0    -1  
$EndComp
$Comp
L 2019-04-26_19-51-24:AD8608ARUZ-REEL U10
U 1 1 5CF625F2
P 7450 1650
AR Path="/5CB2800A/5CF625F2" Ref="U10"  Part="1" 
AR Path="/5CF6E8CA/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5CF713F4/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5CF73EB0/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D0B9ED9/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D0B9EEB/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D0E2164/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D0E2176/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D115D42/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D115D5B/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D1494D0/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D1494E2/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D160D66/5CF625F2" Ref="U14"  Part="1" 
AR Path="/5D16695E/5CF625F2" Ref="U18"  Part="1" 
AR Path="/5D1DAFAE/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D1EFE44/5CF625F2" Ref="U22"  Part="1" 
F 0 "U10" H 8250 2037 60  0000 C CNN
F 1 "AD8608" H 8250 1931 60  0000 C CNN
F 2 "footprints:AD8608ARUZ-REEL" H 8250 1890 60  0001 C CNN
F 3 "" H 7450 1650 60  0000 C CNN
	1    7450 1650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR029
U 1 1 5CF625F9
P 7150 3800
AR Path="/5CB2800A/5CF625F9" Ref="#PWR029"  Part="1" 
AR Path="/5CF6E8CA/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CF625F9" Ref="#PWR039"  Part="1" 
AR Path="/5D16695E/5CF625F9" Ref="#PWR049"  Part="1" 
AR Path="/5D1DAFAE/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CF625F9" Ref="#PWR059"  Part="1" 
F 0 "#PWR029" H 7150 3550 50  0001 C CNN
F 1 "GND" H 7000 3750 50  0000 C CNN
F 2 "" H 7150 3800 50  0001 C CNN
F 3 "" H 7150 3800 50  0001 C CNN
	1    7150 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 3800 7450 3800
NoConn ~ 7450 4000
NoConn ~ 7450 4200
NoConn ~ 7450 4400
NoConn ~ 7450 4600
$Comp
L power:GND #PWR032
U 1 1 5CF62604
P 9050 4400
AR Path="/5CB2800A/5CF62604" Ref="#PWR032"  Part="1" 
AR Path="/5CF6E8CA/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CF62604" Ref="#PWR042"  Part="1" 
AR Path="/5D16695E/5CF62604" Ref="#PWR052"  Part="1" 
AR Path="/5D1DAFAE/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CF62604" Ref="#PWR062"  Part="1" 
F 0 "#PWR032" H 9050 4150 50  0001 C CNN
F 1 "GND" H 9055 4227 50  0000 C CNN
F 2 "" H 9050 4400 50  0001 C CNN
F 3 "" H 9050 4400 50  0001 C CNN
	1    9050 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 1650 7450 1750
Connection ~ 7450 1750
Wire Wire Line
	7450 1750 7450 1850
Connection ~ 7450 1850
Wire Wire Line
	7450 1850 7450 1950
Wire Wire Line
	7450 1650 7200 1650
Connection ~ 7450 1650
Text GLabel 7200 1650 0    50   Input ~ 0
bias_op_amps
Text GLabel 7450 3700 0    50   Input ~ 0
reset_op_amps
Wire Wire Line
	6800 2050 7450 2050
Wire Wire Line
	6700 2150 7450 2150
Wire Wire Line
	7450 2250 6600 2250
Wire Wire Line
	6500 2350 7450 2350
Wire Wire Line
	9250 1650 9050 1650
Wire Wire Line
	9450 1850 9050 1850
Wire Wire Line
	9050 1950 9550 1950
Wire Wire Line
	7450 3900 7300 3900
Wire Wire Line
	7300 3900 7300 3950
$Comp
L power:GND #PWR030
U 1 1 5CF6262A
P 7300 2850
AR Path="/5CB2800A/5CF6262A" Ref="#PWR030"  Part="1" 
AR Path="/5CF6E8CA/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CF6262A" Ref="#PWR040"  Part="1" 
AR Path="/5D16695E/5CF6262A" Ref="#PWR050"  Part="1" 
AR Path="/5D1DAFAE/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CF6262A" Ref="#PWR060"  Part="1" 
F 0 "#PWR030" H 7300 2600 50  0001 C CNN
F 1 "GND" H 7150 2800 50  0000 C CNN
F 2 "" H 7300 2850 50  0001 C CNN
F 3 "" H 7300 2850 50  0001 C CNN
	1    7300 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 2850 7450 2850
$Comp
L power:+3.3V #PWR031
U 1 1 5CF62631
P 7450 4900
AR Path="/5CB2800A/5CF62631" Ref="#PWR031"  Part="1" 
AR Path="/5CF6E8CA/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CF62631" Ref="#PWR041"  Part="1" 
AR Path="/5D16695E/5CF62631" Ref="#PWR051"  Part="1" 
AR Path="/5D1DAFAE/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CF62631" Ref="#PWR061"  Part="1" 
F 0 "#PWR031" H 7450 4750 50  0001 C CNN
F 1 "+3.3V" H 7465 5073 50  0000 C CNN
F 2 "" H 7450 4900 50  0001 C CNN
F 3 "" H 7450 4900 50  0001 C CNN
	1    7450 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 3050 7400 2950
Wire Wire Line
	7400 2950 7450 2950
Wire Wire Line
	7400 3050 7000 3050
Wire Wire Line
	7000 3050 7000 2800
$Comp
L power:+3.3V #PWR028
U 1 1 5CF6263B
P 7000 2800
AR Path="/5CB2800A/5CF6263B" Ref="#PWR028"  Part="1" 
AR Path="/5CF6E8CA/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CF6263B" Ref="#PWR038"  Part="1" 
AR Path="/5D16695E/5CF6263B" Ref="#PWR048"  Part="1" 
AR Path="/5D1DAFAE/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CF6263B" Ref="#PWR058"  Part="1" 
F 0 "#PWR028" H 7000 2650 50  0001 C CNN
F 1 "+3.3V" H 7015 2973 50  0000 C CNN
F 2 "" H 7000 2800 50  0001 C CNN
F 3 "" H 7000 2800 50  0001 C CNN
	1    7000 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5CF62641
P 8250 5350
AR Path="/5C9AAC30/5CF62641" Ref="C?"  Part="1" 
AR Path="/5CB83508/5CF62641" Ref="C?"  Part="1" 
AR Path="/5CB24539/5C9AAC30/5CF62641" Ref="C?"  Part="1" 
AR Path="/5CB24539/5CB83508/5CF62641" Ref="C?"  Part="1" 
AR Path="/5CB2800A/5CF62641" Ref="C5"  Part="1" 
AR Path="/5CB2803C/5CF62641" Ref="C?"  Part="1" 
AR Path="/5CF6E8CA/5CF62641" Ref="C?"  Part="1" 
AR Path="/5CF713F4/5CF62641" Ref="C?"  Part="1" 
AR Path="/5CF73EB0/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D0B9ED9/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D0B9EEB/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D0E2164/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D0E2176/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D115D42/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D115D5B/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D1494D0/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D1494E2/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D160D66/5CF62641" Ref="C13"  Part="1" 
AR Path="/5D16695E/5CF62641" Ref="C21"  Part="1" 
AR Path="/5D1DAFAE/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D1EFE44/5CF62641" Ref="C29"  Part="1" 
F 0 "C5" V 8100 5550 50  0000 C CNN
F 1 "C" V 8200 5550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8288 5200 50  0001 C CNN
F 3 "~" H 8250 5350 50  0001 C CNN
	1    8250 5350
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5CF62648
P 8250 5600
AR Path="/5C9AAC30/5CF62648" Ref="C?"  Part="1" 
AR Path="/5CB83508/5CF62648" Ref="C?"  Part="1" 
AR Path="/5CB24539/5C9AAC30/5CF62648" Ref="C?"  Part="1" 
AR Path="/5CB24539/5CB83508/5CF62648" Ref="C?"  Part="1" 
AR Path="/5CB2800A/5CF62648" Ref="C6"  Part="1" 
AR Path="/5CB2803C/5CF62648" Ref="C?"  Part="1" 
AR Path="/5CF6E8CA/5CF62648" Ref="C?"  Part="1" 
AR Path="/5CF713F4/5CF62648" Ref="C?"  Part="1" 
AR Path="/5CF73EB0/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D0B9ED9/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D0B9EEB/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D0E2164/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D0E2176/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D115D42/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D115D5B/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D1494D0/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D1494E2/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D160D66/5CF62648" Ref="C14"  Part="1" 
AR Path="/5D16695E/5CF62648" Ref="C22"  Part="1" 
AR Path="/5D1DAFAE/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D1EFE44/5CF62648" Ref="C30"  Part="1" 
F 0 "C6" V 8100 5800 50  0000 C CNN
F 1 "C" V 8200 5800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8288 5450 50  0001 C CNN
F 3 "~" H 8250 5600 50  0001 C CNN
	1    8250 5600
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5CF6264F
P 8250 5900
AR Path="/5C9AAC30/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5CB83508/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5CB24539/5C9AAC30/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5CB24539/5CB83508/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5CB2800A/5CF6264F" Ref="C7"  Part="1" 
AR Path="/5CB2803C/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5CF6E8CA/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5CF713F4/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5CF73EB0/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D0B9ED9/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D0B9EEB/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D0E2164/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D0E2176/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D115D42/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D115D5B/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D1494D0/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D1494E2/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D160D66/5CF6264F" Ref="C15"  Part="1" 
AR Path="/5D16695E/5CF6264F" Ref="C23"  Part="1" 
AR Path="/5D1DAFAE/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D1EFE44/5CF6264F" Ref="C31"  Part="1" 
F 0 "C7" V 8100 6100 50  0000 C CNN
F 1 "C" V 8200 6100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8288 5750 50  0001 C CNN
F 3 "~" H 8250 5900 50  0001 C CNN
	1    8250 5900
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5CF62656
P 8250 6200
AR Path="/5C9AAC30/5CF62656" Ref="C?"  Part="1" 
AR Path="/5CB83508/5CF62656" Ref="C?"  Part="1" 
AR Path="/5CB24539/5C9AAC30/5CF62656" Ref="C?"  Part="1" 
AR Path="/5CB24539/5CB83508/5CF62656" Ref="C?"  Part="1" 
AR Path="/5CB2800A/5CF62656" Ref="C8"  Part="1" 
AR Path="/5CB2803C/5CF62656" Ref="C?"  Part="1" 
AR Path="/5CF6E8CA/5CF62656" Ref="C?"  Part="1" 
AR Path="/5CF713F4/5CF62656" Ref="C?"  Part="1" 
AR Path="/5CF73EB0/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D0B9ED9/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D0B9EEB/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D0E2164/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D0E2176/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D115D42/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D115D5B/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D1494D0/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D1494E2/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D160D66/5CF62656" Ref="C16"  Part="1" 
AR Path="/5D16695E/5CF62656" Ref="C24"  Part="1" 
AR Path="/5D1DAFAE/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D1EFE44/5CF62656" Ref="C32"  Part="1" 
F 0 "C8" V 8100 6400 50  0000 C CNN
F 1 "C" V 8200 6400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8288 6050 50  0001 C CNN
F 3 "~" H 8250 6200 50  0001 C CNN
	1    8250 6200
	0    1    1    0   
$EndComp
Wire Wire Line
	8400 5350 9250 5350
Wire Wire Line
	9250 5350 9250 3900
Wire Wire Line
	9350 5600 8400 5600
Wire Wire Line
	8400 5900 9450 5900
Wire Wire Line
	9550 6200 8400 6200
Wire Wire Line
	6800 5350 8100 5350
Wire Wire Line
	8100 5600 6700 5600
Wire Wire Line
	6600 5900 8100 5900
Wire Wire Line
	8100 6200 6500 6200
Text HLabel 10250 1450 2    50   Output ~ 0
O6
Text HLabel 10250 1950 2    50   Output ~ 0
O8
Text HLabel 5950 2150 0    50   Input ~ 0
N6
Text HLabel 5950 2350 0    50   Input ~ 0
N8
Wire Wire Line
	6700 2150 6700 3950
Connection ~ 6700 3950
Wire Wire Line
	6700 3950 6700 5600
Wire Wire Line
	6800 2050 6800 3950
Wire Wire Line
	7450 4100 6800 4100
Connection ~ 6800 4100
Wire Wire Line
	6800 4100 6800 5350
Wire Wire Line
	6500 2350 6500 4300
Wire Wire Line
	7450 4500 6600 4500
Connection ~ 6600 4500
Wire Wire Line
	6600 4500 6600 5900
Connection ~ 6500 4300
Wire Wire Line
	6500 4300 6500 6200
Wire Wire Line
	6500 4300 6600 4300
Wire Wire Line
	9350 1750 9350 3800
Wire Wire Line
	9250 3900 9050 3900
Connection ~ 9250 3900
Wire Wire Line
	9250 1650 9250 3900
Wire Wire Line
	9050 3800 9350 3800
Connection ~ 9350 3800
Wire Wire Line
	9350 3800 9350 5600
Wire Wire Line
	9450 1850 9450 4100
Wire Wire Line
	9550 1950 9550 4000
Wire Wire Line
	9050 4100 9450 4100
Connection ~ 9450 4100
Wire Wire Line
	9450 4100 9450 5900
Wire Wire Line
	9050 4000 9550 4000
Connection ~ 9550 4000
Wire Wire Line
	9550 4000 9550 6200
$Comp
L Connector:TestPoint TP29
U 1 1 5CDC3CFD
P 10100 1200
AR Path="/5D1EFE44/5CDC3CFD" Ref="TP29"  Part="1" 
AR Path="/5CB2800A/5CDC3CFD" Ref="TP5"  Part="1" 
AR Path="/5D160D66/5CDC3CFD" Ref="TP13"  Part="1" 
AR Path="/5D16695E/5CDC3CFD" Ref="TP21"  Part="1" 
F 0 "TP5" H 9900 1250 50  0001 L CNN
F 1 "TestPoint" H 9700 1350 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 10300 1200 50  0001 C CNN
F 3 "~" H 10300 1200 50  0001 C CNN
	1    10100 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 1200 10100 1200
Connection ~ 10100 1200
$Comp
L Connector:TestPoint TP30
U 1 1 5CDDB343
P 10100 1450
AR Path="/5D1EFE44/5CDDB343" Ref="TP30"  Part="1" 
AR Path="/5CB2800A/5CDDB343" Ref="TP6"  Part="1" 
AR Path="/5D160D66/5CDDB343" Ref="TP14"  Part="1" 
AR Path="/5D16695E/5CDDB343" Ref="TP22"  Part="1" 
F 0 "TP6" H 9900 1500 50  0001 L CNN
F 1 "TestPoint" H 9700 1600 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 10300 1450 50  0001 C CNN
F 3 "~" H 10300 1450 50  0001 C CNN
	1    10100 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 1450 10100 1450
Connection ~ 10100 1450
$Comp
L Connector:TestPoint TP31
U 1 1 5CDE3E80
P 10100 1700
AR Path="/5D1EFE44/5CDE3E80" Ref="TP31"  Part="1" 
AR Path="/5CB2800A/5CDE3E80" Ref="TP7"  Part="1" 
AR Path="/5D160D66/5CDE3E80" Ref="TP15"  Part="1" 
AR Path="/5D16695E/5CDE3E80" Ref="TP23"  Part="1" 
F 0 "TP7" H 9900 1750 50  0001 L CNN
F 1 "TestPoint" H 9700 1850 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 10300 1700 50  0001 C CNN
F 3 "~" H 10300 1700 50  0001 C CNN
	1    10100 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 1700 10100 1700
Connection ~ 10100 1700
$Comp
L Connector:TestPoint TP32
U 1 1 5CDE3E8A
P 10100 1950
AR Path="/5D1EFE44/5CDE3E8A" Ref="TP32"  Part="1" 
AR Path="/5CB2800A/5CDE3E8A" Ref="TP8"  Part="1" 
AR Path="/5D160D66/5CDE3E8A" Ref="TP16"  Part="1" 
AR Path="/5D16695E/5CDE3E8A" Ref="TP24"  Part="1" 
F 0 "TP8" H 9900 2000 50  0001 L CNN
F 1 "TestPoint" H 9700 2100 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 10300 1950 50  0001 C CNN
F 3 "~" H 10300 1950 50  0001 C CNN
	1    10100 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 1950 10100 1950
Connection ~ 10100 1950
Wire Wire Line
	9050 1750 9350 1750
Wire Wire Line
	9550 1950 10100 1950
Connection ~ 9550 1950
Wire Wire Line
	9450 1700 9450 1850
Wire Wire Line
	9450 1700 10100 1700
Connection ~ 9450 1850
Wire Wire Line
	9350 1750 9350 1450
Wire Wire Line
	9350 1450 10100 1450
Connection ~ 9350 1750
Wire Wire Line
	9250 1650 9250 1200
Wire Wire Line
	9250 1200 10100 1200
Connection ~ 9250 1650
Wire Wire Line
	6250 2050 6800 2050
Connection ~ 6800 2050
Wire Wire Line
	6250 2150 6700 2150
Connection ~ 6700 2150
Wire Wire Line
	6250 2250 6600 2250
Connection ~ 6600 2250
Wire Wire Line
	6250 2350 6500 2350
Connection ~ 6500 2350
Text HLabel 5150 1250 2    50   Output ~ 0
O1
Text HLabel 5150 1750 2    50   Output ~ 0
O3
Text HLabel 850  2100 0    50   Input ~ 0
N1
Text HLabel 850  2300 0    50   Input ~ 0
N3
$Comp
L 2019-04-26_20-18-08:ADG794BRQZ-REEL7 U9
U 1 1 5CE3FCFB
P 2350 3750
AR Path="/5CB2800A/5CE3FCFB" Ref="U9"  Part="1" 
AR Path="/5CF6E8CA/5CE3FCFB" Ref="U?"  Part="1" 
AR Path="/5CF713F4/5CE3FCFB" Ref="U?"  Part="1" 
AR Path="/5CF73EB0/5CE3FCFB" Ref="U?"  Part="1" 
AR Path="/5D0B9ED9/5CE3FCFB" Ref="U?"  Part="1" 
AR Path="/5D0B9EEB/5CE3FCFB" Ref="U?"  Part="1" 
AR Path="/5D0E2164/5CE3FCFB" Ref="U?"  Part="1" 
AR Path="/5D0E2176/5CE3FCFB" Ref="U?"  Part="1" 
AR Path="/5D115D42/5CE3FCFB" Ref="U?"  Part="1" 
AR Path="/5D115D5B/5CE3FCFB" Ref="U?"  Part="1" 
AR Path="/5D1494D0/5CE3FCFB" Ref="U?"  Part="1" 
AR Path="/5D1494E2/5CE3FCFB" Ref="U?"  Part="1" 
AR Path="/5D160D66/5CE3FCFB" Ref="U13"  Part="1" 
AR Path="/5D16695E/5CE3FCFB" Ref="U17"  Part="1" 
AR Path="/5D1DAFAE/5CE3FCFB" Ref="U?"  Part="1" 
AR Path="/5D1EFE44/5CE3FCFB" Ref="U21"  Part="1" 
F 0 "U9" H 3150 4237 60  0000 C CNN
F 1 "ADG794" H 3150 4131 60  0000 C CNN
F 2 "footprints:ADG794BRQZ-REEL7" H 3150 4090 60  0001 C CNN
F 3 "" H 2350 3750 60  0000 C CNN
	1    2350 3750
	1    0    0    -1  
$EndComp
$Comp
L 2019-04-26_19-51-24:AD8608ARUZ-REEL U8
U 1 1 5CE3FD02
P 2350 1700
AR Path="/5CB2800A/5CE3FD02" Ref="U8"  Part="1" 
AR Path="/5CF6E8CA/5CE3FD02" Ref="U?"  Part="1" 
AR Path="/5CF713F4/5CE3FD02" Ref="U?"  Part="1" 
AR Path="/5CF73EB0/5CE3FD02" Ref="U?"  Part="1" 
AR Path="/5D0B9ED9/5CE3FD02" Ref="U?"  Part="1" 
AR Path="/5D0B9EEB/5CE3FD02" Ref="U?"  Part="1" 
AR Path="/5D0E2164/5CE3FD02" Ref="U?"  Part="1" 
AR Path="/5D0E2176/5CE3FD02" Ref="U?"  Part="1" 
AR Path="/5D115D42/5CE3FD02" Ref="U?"  Part="1" 
AR Path="/5D115D5B/5CE3FD02" Ref="U?"  Part="1" 
AR Path="/5D1494D0/5CE3FD02" Ref="U?"  Part="1" 
AR Path="/5D1494E2/5CE3FD02" Ref="U?"  Part="1" 
AR Path="/5D160D66/5CE3FD02" Ref="U12"  Part="1" 
AR Path="/5D16695E/5CE3FD02" Ref="U16"  Part="1" 
AR Path="/5D1DAFAE/5CE3FD02" Ref="U?"  Part="1" 
AR Path="/5D1EFE44/5CE3FD02" Ref="U20"  Part="1" 
F 0 "U8" H 3150 2087 60  0000 C CNN
F 1 "AD8608" H 3150 1981 60  0000 C CNN
F 2 "footprints:AD8608ARUZ-REEL" H 3150 1940 60  0001 C CNN
F 3 "" H 2350 1700 60  0000 C CNN
	1    2350 1700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR024
U 1 1 5CE3FD09
P 2050 3850
AR Path="/5CB2800A/5CE3FD09" Ref="#PWR024"  Part="1" 
AR Path="/5CF6E8CA/5CE3FD09" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CE3FD09" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CE3FD09" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CE3FD09" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CE3FD09" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CE3FD09" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CE3FD09" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CE3FD09" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CE3FD09" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CE3FD09" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CE3FD09" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CE3FD09" Ref="#PWR034"  Part="1" 
AR Path="/5D16695E/5CE3FD09" Ref="#PWR044"  Part="1" 
AR Path="/5D1DAFAE/5CE3FD09" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CE3FD09" Ref="#PWR054"  Part="1" 
F 0 "#PWR024" H 2050 3600 50  0001 C CNN
F 1 "GND" H 1900 3800 50  0000 C CNN
F 2 "" H 2050 3850 50  0001 C CNN
F 3 "" H 2050 3850 50  0001 C CNN
	1    2050 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 3850 2350 3850
NoConn ~ 2350 4050
NoConn ~ 2350 4250
NoConn ~ 2350 4450
NoConn ~ 2350 4650
$Comp
L power:GND #PWR027
U 1 1 5CE3FD14
P 3950 4450
AR Path="/5CB2800A/5CE3FD14" Ref="#PWR027"  Part="1" 
AR Path="/5CF6E8CA/5CE3FD14" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CE3FD14" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CE3FD14" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CE3FD14" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CE3FD14" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CE3FD14" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CE3FD14" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CE3FD14" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CE3FD14" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CE3FD14" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CE3FD14" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CE3FD14" Ref="#PWR037"  Part="1" 
AR Path="/5D16695E/5CE3FD14" Ref="#PWR047"  Part="1" 
AR Path="/5D1DAFAE/5CE3FD14" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CE3FD14" Ref="#PWR057"  Part="1" 
F 0 "#PWR027" H 3950 4200 50  0001 C CNN
F 1 "GND" H 3955 4277 50  0000 C CNN
F 2 "" H 3950 4450 50  0001 C CNN
F 3 "" H 3950 4450 50  0001 C CNN
	1    3950 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 1700 2350 1800
Connection ~ 2350 1800
Wire Wire Line
	2350 1800 2350 1900
Connection ~ 2350 1900
Wire Wire Line
	2350 1900 2350 2000
Wire Wire Line
	2350 1700 2100 1700
Connection ~ 2350 1700
Text GLabel 2100 1700 0    50   Input ~ 0
bias_op_amps
Text GLabel 2350 3750 0    50   Input ~ 0
reset_op_amps
Wire Wire Line
	1700 2100 2350 2100
Wire Wire Line
	1600 2200 2350 2200
Wire Wire Line
	2350 2300 1500 2300
Wire Wire Line
	1400 2400 2350 2400
Wire Wire Line
	4150 1700 3950 1700
Wire Wire Line
	4350 1900 3950 1900
Wire Wire Line
	3950 2000 4450 2000
Wire Wire Line
	2350 3950 2200 3950
Wire Wire Line
	2200 3950 2200 4000
$Comp
L power:GND #PWR025
U 1 1 5CE3FD2C
P 2200 2900
AR Path="/5CB2800A/5CE3FD2C" Ref="#PWR025"  Part="1" 
AR Path="/5CF6E8CA/5CE3FD2C" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CE3FD2C" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CE3FD2C" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CE3FD2C" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CE3FD2C" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CE3FD2C" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CE3FD2C" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CE3FD2C" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CE3FD2C" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CE3FD2C" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CE3FD2C" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CE3FD2C" Ref="#PWR035"  Part="1" 
AR Path="/5D16695E/5CE3FD2C" Ref="#PWR045"  Part="1" 
AR Path="/5D1DAFAE/5CE3FD2C" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CE3FD2C" Ref="#PWR055"  Part="1" 
F 0 "#PWR025" H 2200 2650 50  0001 C CNN
F 1 "GND" H 2050 2850 50  0000 C CNN
F 2 "" H 2200 2900 50  0001 C CNN
F 3 "" H 2200 2900 50  0001 C CNN
	1    2200 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 2900 2350 2900
$Comp
L power:+3.3V #PWR026
U 1 1 5CE3FD33
P 2350 4950
AR Path="/5CB2800A/5CE3FD33" Ref="#PWR026"  Part="1" 
AR Path="/5CF6E8CA/5CE3FD33" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CE3FD33" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CE3FD33" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CE3FD33" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CE3FD33" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CE3FD33" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CE3FD33" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CE3FD33" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CE3FD33" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CE3FD33" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CE3FD33" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CE3FD33" Ref="#PWR036"  Part="1" 
AR Path="/5D16695E/5CE3FD33" Ref="#PWR046"  Part="1" 
AR Path="/5D1DAFAE/5CE3FD33" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CE3FD33" Ref="#PWR056"  Part="1" 
F 0 "#PWR026" H 2350 4800 50  0001 C CNN
F 1 "+3.3V" H 2365 5123 50  0000 C CNN
F 2 "" H 2350 4950 50  0001 C CNN
F 3 "" H 2350 4950 50  0001 C CNN
	1    2350 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 3100 2300 3000
Wire Wire Line
	2300 3000 2350 3000
Wire Wire Line
	2300 3100 1900 3100
Wire Wire Line
	1900 3100 1900 2850
$Comp
L power:+3.3V #PWR023
U 1 1 5CE3FD3D
P 1900 2850
AR Path="/5CB2800A/5CE3FD3D" Ref="#PWR023"  Part="1" 
AR Path="/5CF6E8CA/5CE3FD3D" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CE3FD3D" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CE3FD3D" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CE3FD3D" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CE3FD3D" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CE3FD3D" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CE3FD3D" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CE3FD3D" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CE3FD3D" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CE3FD3D" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CE3FD3D" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CE3FD3D" Ref="#PWR033"  Part="1" 
AR Path="/5D16695E/5CE3FD3D" Ref="#PWR043"  Part="1" 
AR Path="/5D1DAFAE/5CE3FD3D" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CE3FD3D" Ref="#PWR053"  Part="1" 
F 0 "#PWR023" H 1900 2700 50  0001 C CNN
F 1 "+3.3V" H 1915 3023 50  0000 C CNN
F 2 "" H 1900 2850 50  0001 C CNN
F 3 "" H 1900 2850 50  0001 C CNN
	1    1900 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5CE3FD43
P 3150 5400
AR Path="/5C9AAC30/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5CB83508/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5CB24539/5C9AAC30/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5CB24539/5CB83508/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5CB2800A/5CE3FD43" Ref="C1"  Part="1" 
AR Path="/5CB2803C/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5CF6E8CA/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5CF713F4/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5CF73EB0/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5D0B9ED9/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5D0B9EEB/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5D0E2164/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5D0E2176/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5D115D42/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5D115D5B/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5D1494D0/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5D1494E2/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5D160D66/5CE3FD43" Ref="C9"  Part="1" 
AR Path="/5D16695E/5CE3FD43" Ref="C17"  Part="1" 
AR Path="/5D1DAFAE/5CE3FD43" Ref="C?"  Part="1" 
AR Path="/5D1EFE44/5CE3FD43" Ref="C25"  Part="1" 
F 0 "C1" V 3000 5600 50  0000 C CNN
F 1 "C" V 3100 5600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3188 5250 50  0001 C CNN
F 3 "~" H 3150 5400 50  0001 C CNN
	1    3150 5400
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5CE3FD4A
P 3150 5650
AR Path="/5C9AAC30/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5CB83508/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5CB24539/5C9AAC30/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5CB24539/5CB83508/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5CB2800A/5CE3FD4A" Ref="C2"  Part="1" 
AR Path="/5CB2803C/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5CF6E8CA/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5CF713F4/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5CF73EB0/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5D0B9ED9/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5D0B9EEB/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5D0E2164/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5D0E2176/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5D115D42/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5D115D5B/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5D1494D0/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5D1494E2/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5D160D66/5CE3FD4A" Ref="C10"  Part="1" 
AR Path="/5D16695E/5CE3FD4A" Ref="C18"  Part="1" 
AR Path="/5D1DAFAE/5CE3FD4A" Ref="C?"  Part="1" 
AR Path="/5D1EFE44/5CE3FD4A" Ref="C26"  Part="1" 
F 0 "C2" V 3000 5850 50  0000 C CNN
F 1 "C" V 3100 5850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3188 5500 50  0001 C CNN
F 3 "~" H 3150 5650 50  0001 C CNN
	1    3150 5650
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5CE3FD51
P 3150 5950
AR Path="/5C9AAC30/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5CB83508/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5CB24539/5C9AAC30/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5CB24539/5CB83508/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5CB2800A/5CE3FD51" Ref="C3"  Part="1" 
AR Path="/5CB2803C/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5CF6E8CA/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5CF713F4/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5CF73EB0/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5D0B9ED9/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5D0B9EEB/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5D0E2164/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5D0E2176/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5D115D42/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5D115D5B/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5D1494D0/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5D1494E2/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5D160D66/5CE3FD51" Ref="C11"  Part="1" 
AR Path="/5D16695E/5CE3FD51" Ref="C19"  Part="1" 
AR Path="/5D1DAFAE/5CE3FD51" Ref="C?"  Part="1" 
AR Path="/5D1EFE44/5CE3FD51" Ref="C27"  Part="1" 
F 0 "C3" V 3000 6150 50  0000 C CNN
F 1 "C" V 3100 6150 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3188 5800 50  0001 C CNN
F 3 "~" H 3150 5950 50  0001 C CNN
	1    3150 5950
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5CE3FD58
P 3150 6250
AR Path="/5C9AAC30/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5CB83508/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5CB24539/5C9AAC30/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5CB24539/5CB83508/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5CB2800A/5CE3FD58" Ref="C4"  Part="1" 
AR Path="/5CB2803C/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5CF6E8CA/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5CF713F4/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5CF73EB0/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5D0B9ED9/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5D0B9EEB/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5D0E2164/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5D0E2176/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5D115D42/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5D115D5B/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5D1494D0/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5D1494E2/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5D160D66/5CE3FD58" Ref="C12"  Part="1" 
AR Path="/5D16695E/5CE3FD58" Ref="C20"  Part="1" 
AR Path="/5D1DAFAE/5CE3FD58" Ref="C?"  Part="1" 
AR Path="/5D1EFE44/5CE3FD58" Ref="C28"  Part="1" 
F 0 "C4" V 3000 6450 50  0000 C CNN
F 1 "C" V 3100 6450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3188 6100 50  0001 C CNN
F 3 "~" H 3150 6250 50  0001 C CNN
	1    3150 6250
	0    1    1    0   
$EndComp
Wire Wire Line
	3300 5400 4150 5400
Wire Wire Line
	4150 5400 4150 3950
Wire Wire Line
	4250 5650 3300 5650
Wire Wire Line
	3300 5950 4350 5950
Wire Wire Line
	4450 6250 3300 6250
Wire Wire Line
	1700 5400 3000 5400
Wire Wire Line
	3000 5650 1600 5650
Wire Wire Line
	1500 5950 3000 5950
Wire Wire Line
	3000 6250 1400 6250
Text HLabel 10250 1200 2    50   Output ~ 0
O5
Text HLabel 10250 1700 2    50   Output ~ 0
O7
Text HLabel 5950 2050 0    50   Input ~ 0
N5
Text HLabel 5950 2250 0    50   Input ~ 0
N7
Wire Wire Line
	1600 2200 1600 4000
Connection ~ 1600 4000
Wire Wire Line
	1600 4000 1600 5650
Wire Wire Line
	2350 4150 1700 4150
Wire Wire Line
	1700 4150 1700 5400
Wire Wire Line
	1400 2400 1400 4350
Wire Wire Line
	2350 4550 1500 4550
Connection ~ 1500 4550
Wire Wire Line
	1500 4550 1500 5950
Connection ~ 1400 4350
Wire Wire Line
	1400 4350 1400 6250
Wire Wire Line
	1400 4350 1500 4350
Wire Wire Line
	4250 1800 4250 3850
Wire Wire Line
	4150 3950 3950 3950
Connection ~ 4150 3950
Wire Wire Line
	4150 1700 4150 3950
Wire Wire Line
	3950 3850 4250 3850
Connection ~ 4250 3850
Wire Wire Line
	4250 3850 4250 5650
Wire Wire Line
	4350 1900 4350 4150
Wire Wire Line
	4450 2000 4450 4050
Wire Wire Line
	3950 4150 4350 4150
Connection ~ 4350 4150
Wire Wire Line
	4350 4150 4350 5950
Wire Wire Line
	3950 4050 4450 4050
Connection ~ 4450 4050
Wire Wire Line
	4450 4050 4450 6250
$Comp
L Connector:TestPoint TP25
U 1 1 5CE3FD8D
P 5000 1250
AR Path="/5D1EFE44/5CE3FD8D" Ref="TP25"  Part="1" 
AR Path="/5CB2800A/5CE3FD8D" Ref="TP1"  Part="1" 
AR Path="/5D160D66/5CE3FD8D" Ref="TP9"  Part="1" 
AR Path="/5D16695E/5CE3FD8D" Ref="TP17"  Part="1" 
F 0 "TP1" H 4800 1300 50  0001 L CNN
F 1 "TestPoint" H 4600 1400 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 5200 1250 50  0001 C CNN
F 3 "~" H 5200 1250 50  0001 C CNN
	1    5000 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1250 5000 1250
Connection ~ 5000 1250
$Comp
L Connector:TestPoint TP26
U 1 1 5CE3FD96
P 5000 1500
AR Path="/5D1EFE44/5CE3FD96" Ref="TP26"  Part="1" 
AR Path="/5CB2800A/5CE3FD96" Ref="TP2"  Part="1" 
AR Path="/5D160D66/5CE3FD96" Ref="TP10"  Part="1" 
AR Path="/5D16695E/5CE3FD96" Ref="TP18"  Part="1" 
F 0 "TP2" H 4800 1550 50  0001 L CNN
F 1 "TestPoint" H 4600 1650 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 5200 1500 50  0001 C CNN
F 3 "~" H 5200 1500 50  0001 C CNN
	1    5000 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1500 5000 1500
Connection ~ 5000 1500
$Comp
L Connector:TestPoint TP27
U 1 1 5CE3FD9F
P 5000 1750
AR Path="/5D1EFE44/5CE3FD9F" Ref="TP27"  Part="1" 
AR Path="/5CB2800A/5CE3FD9F" Ref="TP3"  Part="1" 
AR Path="/5D160D66/5CE3FD9F" Ref="TP11"  Part="1" 
AR Path="/5D16695E/5CE3FD9F" Ref="TP19"  Part="1" 
F 0 "TP3" H 4800 1800 50  0001 L CNN
F 1 "TestPoint" H 4600 1900 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 5200 1750 50  0001 C CNN
F 3 "~" H 5200 1750 50  0001 C CNN
	1    5000 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1750 5000 1750
Connection ~ 5000 1750
$Comp
L Connector:TestPoint TP28
U 1 1 5CE3FDA8
P 5000 2000
AR Path="/5D1EFE44/5CE3FDA8" Ref="TP28"  Part="1" 
AR Path="/5CB2800A/5CE3FDA8" Ref="TP4"  Part="1" 
AR Path="/5D160D66/5CE3FDA8" Ref="TP12"  Part="1" 
AR Path="/5D16695E/5CE3FDA8" Ref="TP20"  Part="1" 
F 0 "TP4" H 4800 2050 50  0001 L CNN
F 1 "TestPoint" H 4600 2150 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 5200 2000 50  0001 C CNN
F 3 "~" H 5200 2000 50  0001 C CNN
	1    5000 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 2000 5000 2000
Connection ~ 5000 2000
Wire Wire Line
	3950 1800 4250 1800
Wire Wire Line
	4450 2000 5000 2000
Connection ~ 4450 2000
Wire Wire Line
	4350 1750 4350 1900
Wire Wire Line
	4350 1750 5000 1750
Connection ~ 4350 1900
Wire Wire Line
	4250 1800 4250 1500
Wire Wire Line
	4250 1500 5000 1500
Connection ~ 4250 1800
Wire Wire Line
	4150 1700 4150 1250
Wire Wire Line
	4150 1250 5000 1250
Connection ~ 4150 1700
Wire Wire Line
	1150 2100 1700 2100
Connection ~ 1700 2100
Wire Wire Line
	1150 2200 1600 2200
Connection ~ 1600 2200
Wire Wire Line
	1150 2300 1500 2300
Connection ~ 1500 2300
Wire Wire Line
	1150 2400 1400 2400
Connection ~ 1400 2400
Wire Wire Line
	6600 4300 7450 4300
Wire Wire Line
	6800 3950 6800 4100
Wire Wire Line
	1500 4350 2350 4350
Wire Wire Line
	6600 2250 6600 4500
Wire Wire Line
	6700 3950 7300 3950
Wire Wire Line
	1500 2300 1500 4550
Wire Wire Line
	1700 2100 1700 4000
Connection ~ 1700 4150
Wire Wire Line
	1700 4000 1700 4150
Wire Wire Line
	1600 4000 2200 4000
$Comp
L Jumper:SolderJumper_2_Bridged JP1
U 1 1 5CF0DDA9
P 1000 2100
AR Path="/5CB2800A/5CF0DDA9" Ref="JP1"  Part="1" 
AR Path="/5D160D66/5CF0DDA9" Ref="JP9"  Part="1" 
AR Path="/5D16695E/5CF0DDA9" Ref="JP17"  Part="1" 
AR Path="/5D1EFE44/5CF0DDA9" Ref="JP25"  Part="1" 
F 0 "JP1" H 1200 2150 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 1000 2214 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 1000 2100 50  0001 C CNN
F 3 "~" H 1000 2100 50  0001 C CNN
	1    1000 2100
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP2
U 1 1 5CF1AA01
P 1000 2200
AR Path="/5CB2800A/5CF1AA01" Ref="JP2"  Part="1" 
AR Path="/5D160D66/5CF1AA01" Ref="JP10"  Part="1" 
AR Path="/5D16695E/5CF1AA01" Ref="JP18"  Part="1" 
AR Path="/5D1EFE44/5CF1AA01" Ref="JP26"  Part="1" 
F 0 "JP2" H 1200 2250 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 1000 2314 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 1000 2200 50  0001 C CNN
F 3 "~" H 1000 2200 50  0001 C CNN
	1    1000 2200
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP3
U 1 1 5CF20DD3
P 1000 2300
AR Path="/5CB2800A/5CF20DD3" Ref="JP3"  Part="1" 
AR Path="/5D160D66/5CF20DD3" Ref="JP11"  Part="1" 
AR Path="/5D16695E/5CF20DD3" Ref="JP19"  Part="1" 
AR Path="/5D1EFE44/5CF20DD3" Ref="JP27"  Part="1" 
F 0 "JP3" H 1200 2350 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 1000 2414 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 1000 2300 50  0001 C CNN
F 3 "~" H 1000 2300 50  0001 C CNN
	1    1000 2300
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP4
U 1 1 5CF27175
P 1000 2400
AR Path="/5CB2800A/5CF27175" Ref="JP4"  Part="1" 
AR Path="/5D160D66/5CF27175" Ref="JP12"  Part="1" 
AR Path="/5D16695E/5CF27175" Ref="JP20"  Part="1" 
AR Path="/5D1EFE44/5CF27175" Ref="JP28"  Part="1" 
F 0 "JP4" H 1200 2450 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 1000 2514 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 1000 2400 50  0001 C CNN
F 3 "~" H 1000 2400 50  0001 C CNN
	1    1000 2400
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP5
U 1 1 5CF33AF6
P 6100 2050
AR Path="/5CB2800A/5CF33AF6" Ref="JP5"  Part="1" 
AR Path="/5D160D66/5CF33AF6" Ref="JP13"  Part="1" 
AR Path="/5D16695E/5CF33AF6" Ref="JP21"  Part="1" 
AR Path="/5D1EFE44/5CF33AF6" Ref="JP29"  Part="1" 
F 0 "JP5" H 6300 2100 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 6100 2164 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 6100 2050 50  0001 C CNN
F 3 "~" H 6100 2050 50  0001 C CNN
	1    6100 2050
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP6
U 1 1 5CF33AFD
P 6100 2150
AR Path="/5CB2800A/5CF33AFD" Ref="JP6"  Part="1" 
AR Path="/5D160D66/5CF33AFD" Ref="JP14"  Part="1" 
AR Path="/5D16695E/5CF33AFD" Ref="JP22"  Part="1" 
AR Path="/5D1EFE44/5CF33AFD" Ref="JP30"  Part="1" 
F 0 "JP6" H 6300 2200 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 6100 2264 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 6100 2150 50  0001 C CNN
F 3 "~" H 6100 2150 50  0001 C CNN
	1    6100 2150
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP7
U 1 1 5CF33B04
P 6100 2250
AR Path="/5CB2800A/5CF33B04" Ref="JP7"  Part="1" 
AR Path="/5D160D66/5CF33B04" Ref="JP15"  Part="1" 
AR Path="/5D16695E/5CF33B04" Ref="JP23"  Part="1" 
AR Path="/5D1EFE44/5CF33B04" Ref="JP31"  Part="1" 
F 0 "JP7" H 6300 2300 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 6100 2364 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 6100 2250 50  0001 C CNN
F 3 "~" H 6100 2250 50  0001 C CNN
	1    6100 2250
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP8
U 1 1 5CF33B0B
P 6100 2350
AR Path="/5CB2800A/5CF33B0B" Ref="JP8"  Part="1" 
AR Path="/5D160D66/5CF33B0B" Ref="JP16"  Part="1" 
AR Path="/5D16695E/5CF33B0B" Ref="JP24"  Part="1" 
AR Path="/5D1EFE44/5CF33B0B" Ref="JP32"  Part="1" 
F 0 "JP8" H 6300 2400 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 6100 2464 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 6100 2350 50  0001 C CNN
F 3 "~" H 6100 2350 50  0001 C CNN
	1    6100 2350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
