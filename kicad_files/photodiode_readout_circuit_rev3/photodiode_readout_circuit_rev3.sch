EESchema Schematic File Version 4
LIBS:photodiode_readout_circuit_rev3-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title ""
Date "lun. 30 mars 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 8900 2500 0    60   ~ 0
A0
Text Label 8900 2600 0    60   ~ 0
A1
Text Label 8150 2650 0    60   ~ 0
7
Text Label 8150 2750 0    60   ~ 0
8
Text Label 8150 2850 0    60   ~ 0
9(**)
Text Label 8150 2950 0    60   ~ 0
10(**/SS)
Text Label 8150 3050 0    60   ~ 0
11(**/MOSI)
Text Label 8150 3750 0    60   ~ 0
12(MISO)
NoConn ~ 9400 1600
Text Notes 8550 750  0    60   ~ 0
Shield for Arduino that uses\nthe same pin disposition\nlike "Uno" board Rev 3.
$Comp
L Connector_Generic:Conn_01x08 P1
U 1 1 56D70129
P 9600 1900
F 0 "P1" H 9600 2350 50  0000 C CNN
F 1 "Power" V 9700 1900 50  0000 C CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" V 9750 1900 20  0000 C CNN
F 3 "" H 9600 1900 50  0000 C CNN
	1    9600 1900
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR04
U 1 1 56D70538
P 9150 1450
F 0 "#PWR04" H 9150 1300 50  0001 C CNN
F 1 "+3.3V" V 9150 1700 50  0000 C CNN
F 2 "" H 9150 1450 50  0000 C CNN
F 3 "" H 9150 1450 50  0000 C CNN
	1    9150 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 56D70CC2
P 9300 3150
F 0 "#PWR05" H 9300 2900 50  0001 C CNN
F 1 "GND" H 9300 3000 50  0000 C CNN
F 2 "" H 9300 3150 50  0000 C CNN
F 3 "" H 9300 3150 50  0000 C CNN
	1    9300 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 56D70CFF
P 10300 3150
F 0 "#PWR06" H 10300 2900 50  0001 C CNN
F 1 "GND" H 10300 3000 50  0000 C CNN
F 2 "" H 10300 3150 50  0000 C CNN
F 3 "" H 10300 3150 50  0000 C CNN
	1    10300 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 P2
U 1 1 56D70DD8
P 9600 2700
F 0 "P2" H 9600 2300 50  0000 C CNN
F 1 "Analog" V 9700 2700 50  0000 C CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x06" V 9750 2750 20  0000 C CNN
F 3 "" H 9600 2700 50  0000 C CNN
	1    9600 2700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 P4
U 1 1 56D7164F
P 10000 2600
F 0 "P4" H 10000 2100 50  0000 C CNN
F 1 "Digital" V 10100 2600 50  0000 C CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" V 10150 2550 20  0000 C CNN
F 3 "" H 10000 2600 50  0000 C CNN
	1    10000 2600
	-1   0    0    -1  
$EndComp
Wire Notes Line
	8525 825  9925 825 
Wire Notes Line
	9925 825  9925 475 
Wire Wire Line
	9400 1900 9150 1900
Wire Wire Line
	9400 2100 9300 2100
Wire Wire Line
	9400 2200 9300 2200
Connection ~ 9300 2200
Wire Wire Line
	9150 1900 9150 1450
Wire Wire Line
	9400 2500 8900 2500
Wire Wire Line
	9400 2600 8900 2600
$Comp
L Connector_Generic:Conn_01x10 P3
U 1 1 56D721E0
P 10000 1600
F 0 "P3" H 10000 2150 50  0000 C CNN
F 1 "Digital" V 10100 1600 50  0000 C CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x10" V 10150 1600 20  0000 C CNN
F 3 "" H 10000 1600 50  0000 C CNN
	1    10000 1600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10200 1500 10300 1500
Wire Wire Line
	10300 1500 10300 3150
Wire Wire Line
	9300 2100 9300 2200
Wire Wire Line
	9300 2200 9300 3150
Wire Notes Line
	8500 500  8500 3450
Wire Notes Line
	8500 3450 11200 3450
Text Notes 9700 1600 0    60   ~ 0
1
Connection ~ 8150 3550
Wire Wire Line
	8150 3650 8150 3550
Wire Wire Line
	8150 3550 8150 3450
$Comp
L power:GND #PWR?
U 1 1 5CB280A2
P 8300 3350
AR Path="/5CB24539/5CB280A2" Ref="#PWR?"  Part="1" 
AR Path="/5CB280A2" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 8300 3100 50  0001 C CNN
F 1 "GND" H 8305 3177 50  0000 C CNN
F 2 "" H 8300 3350 50  0001 C CNN
F 3 "" H 8300 3350 50  0001 C CNN
	1    8300 3350
	0    1    -1   0   
$EndComp
Text Label 8150 2350 0    50   ~ 0
A1
$Comp
L photodiode_readout_circuit_rev3-rescue:photodiode_array-photodiode_readout_circuit U1
U 1 1 5CB4A29D
P 3750 5850
F 0 "U1" H 3700 6000 50  0000 L CNN
F 1 "photodiode_array" H 3650 5900 50  0000 L CNN
F 2 "Package_DFN_QFN2:QFN-64-1EP_9x9mm_P0.5mm_EP7.65x7.65mm" H 4000 5750 50  0001 C CNN
F 3 "" H 4000 5750 50  0001 C CNN
	1    3750 5850
	1    0    0    -1  
$EndComp
Text Label 10400 3800 2    60   ~ 0
0(Rx)
Wire Wire Line
	8400 3350 8400 3550
Wire Wire Line
	8400 3550 8150 3550
Text GLabel 10500 3800 2    50   Input ~ 0
reset_op_amps
Wire Wire Line
	10500 3800 10400 3800
Text GLabel 10550 4100 2    50   Input ~ 0
bias_op_amps
Text Label 10250 4100 2    60   ~ 0
A0
Wire Wire Line
	10250 4100 10550 4100
Text Label 10600 2900 0    60   ~ 0
1(Tx)
Text Label 10600 2700 0    60   ~ 0
3(**)
Text Label 10600 2500 0    60   ~ 0
5(**)
Wire Wire Line
	10200 2900 10600 2900
Wire Wire Line
	10600 2700 10200 2700
Wire Wire Line
	10200 2500 10600 2500
Text Label 10600 2400 0    60   ~ 0
6(**)
Wire Wire Line
	10600 2400 10200 2400
Wire Wire Line
	10200 2600 10600 2600
Wire Wire Line
	10200 2800 10600 2800
Wire Wire Line
	10200 3000 10600 3000
Text Label 10600 2300 0    60   ~ 0
7
Text Label 10600 2100 0    60   ~ 0
8
Text Label 10600 2000 0    60   ~ 0
9(**)
Text Label 10600 1900 0    60   ~ 0
10(**/SS)
Text Label 10600 1800 0    60   ~ 0
11(**/MOSI)
Wire Wire Line
	10600 2100 10200 2100
Wire Wire Line
	10600 2000 10200 2000
Wire Wire Line
	10600 1900 10200 1900
Wire Wire Line
	10600 1800 10200 1800
Wire Wire Line
	10600 1700 10200 1700
Wire Wire Line
	10600 2300 10200 2300
Text Label 10600 1700 0    60   ~ 0
12(MISO)
Text Label 10600 3000 0    60   ~ 0
0(Rx)
Text Label 10600 2800 0    60   ~ 0
2
Text Label 10600 2600 0    60   ~ 0
4
Wire Wire Line
	8300 3350 8400 3350
NoConn ~ 9400 2700
NoConn ~ 9400 2800
NoConn ~ 9400 2900
NoConn ~ 9400 3000
NoConn ~ 9400 2000
NoConn ~ 9400 1800
NoConn ~ 9400 1700
NoConn ~ 10200 1200
NoConn ~ 10200 1300
NoConn ~ 10200 1400
NoConn ~ 10200 1600
NoConn ~ 9600 2050
NoConn ~ 9400 2300
$Sheet
S 850  4000 850  3700
U 5CC442EB
F0 "Row Select" 50
F1 "row_sel.sch" 50
F2 "R8" B R 1700 5000 50 
F3 "R1" B R 1700 4300 50 
F4 "R2" B R 1700 4400 50 
F5 "R3" B R 1700 4500 50 
F6 "R4" B R 1700 4600 50 
F7 "R5" B R 1700 4700 50 
F8 "R6" B R 1700 4800 50 
F9 "R7" B R 1700 4900 50 
F10 "R16" B R 1700 5800 50 
F11 "R9" B R 1700 5100 50 
F12 "R10" B R 1700 5200 50 
F13 "R11" B R 1700 5300 50 
F14 "R12" B R 1700 5400 50 
F15 "R13" B R 1700 5500 50 
F16 "R14" B R 1700 5600 50 
F17 "R15" B R 1700 5700 50 
F18 "R24" B R 1700 6600 50 
F19 "R17" B R 1700 5900 50 
F20 "R18" B R 1700 6000 50 
F21 "R19" B R 1700 6100 50 
F22 "R20" B R 1700 6200 50 
F23 "R21" B R 1700 6300 50 
F24 "R22" B R 1700 6400 50 
F25 "R23" B R 1700 6500 50 
F26 "R32" B R 1700 7400 50 
F27 "R25" B R 1700 6700 50 
F28 "R26" B R 1700 6800 50 
F29 "R27" B R 1700 6900 50 
F30 "R28" B R 1700 7000 50 
F31 "R29" B R 1700 7100 50 
F32 "R30" B R 1700 7200 50 
F33 "R31" B R 1700 7300 50 
F34 "SEL0" I L 850 5150 50 
F35 "SEL1" I L 850 5250 50 
F36 "SEL2" I L 850 5350 50 
F37 "SEL3" I L 850 5450 50 
F38 "SEL4" I L 850 5550 50 
F39 "EN_NOT" I L 850 5800 50 
$EndSheet
Text Label 850  5250 2    60   ~ 0
2
Text Label 850  5150 2    60   ~ 0
1(Tx)
Text Label 850  5350 2    60   ~ 0
3(**)
Text Label 850  5550 2    60   ~ 0
5(**)
Text Label 850  5800 2    60   ~ 0
6(**)
Text Label 850  5450 2    60   ~ 0
4
$Sheet
S 2200 1050 450  850 
U 5CB2800A
F0 "sheet5CB27FDD" 59
F1 "trans_impedance_amp_arr.sch" 20
F2 "N1" I L 2200 1100 50 
F3 "N2" I L 2200 1200 50 
F4 "N3" I L 2200 1300 50 
F5 "N4" I L 2200 1400 50 
F6 "O4" O R 2650 1400 50 
F7 "O3" O R 2650 1300 50 
F8 "O2" O R 2650 1200 50 
F9 "O1" O R 2650 1100 50 
F10 "O5" O R 2650 1500 50 
F11 "O6" O R 2650 1600 50 
F12 "O7" O R 2650 1700 50 
F13 "O8" O R 2650 1800 50 
F14 "N5" I L 2200 1500 50 
F15 "N6" I L 2200 1600 50 
F16 "N7" I L 2200 1700 50 
F17 "N8" I L 2200 1800 50 
$EndSheet
$Sheet
S 3100 1650 450  850 
U 5D160D66
F0 "sheet5D160D54" 20
F1 "trans_impedance_amp_arr.sch" 20
F2 "N1" I L 3100 1700 50 
F3 "N2" I L 3100 1800 50 
F4 "N3" I L 3100 1900 50 
F5 "N4" I L 3100 2000 50 
F6 "O4" O R 3550 2000 50 
F7 "O3" O R 3550 1900 50 
F8 "O2" O R 3550 1800 50 
F9 "O1" O R 3550 1700 50 
F10 "O5" O R 3550 2100 50 
F11 "O6" O R 3550 2200 50 
F12 "O7" O R 3550 2300 50 
F13 "O8" O R 3550 2400 50 
F14 "N5" I L 3100 2100 50 
F15 "N6" I L 3100 2200 50 
F16 "N7" I L 3100 2300 50 
F17 "N8" I L 3100 2400 50 
$EndSheet
$Sheet
S 3900 2150 450  850 
U 5D16695E
F0 "sheet5D16694C" 20
F1 "trans_impedance_amp_arr.sch" 20
F2 "N1" I L 3900 2200 50 
F3 "N2" I L 3900 2300 50 
F4 "N3" I L 3900 2400 50 
F5 "N4" I L 3900 2500 50 
F6 "O4" O R 4350 2500 50 
F7 "O3" O R 4350 2400 50 
F8 "O2" O R 4350 2300 50 
F9 "O1" O R 4350 2200 50 
F10 "O5" O R 4350 2600 50 
F11 "O6" O R 4350 2700 50 
F12 "O7" O R 4350 2800 50 
F13 "O8" O R 4350 2900 50 
F14 "N5" I L 3900 2600 50 
F15 "N6" I L 3900 2700 50 
F16 "N7" I L 3900 2800 50 
F17 "N8" I L 3900 2900 50 
$EndSheet
$Comp
L ADG732BCPZ:ADG732BCPZ U?
U 1 1 5CBB7408
P 8150 4050
AR Path="/5CB24539/5CBB7408" Ref="U?"  Part="1" 
AR Path="/5CBB7408" Ref="U2"  Part="1" 
F 0 "U2" H 8950 4537 60  0000 C CNN
F 1 "ADG732BCPZ" H 8950 4431 60  0000 C CNN
F 2 "footprints:ADG732BCPZ" H 8950 4390 60  0001 C CNN
F 3 "" H 8150 4050 60  0000 C CNN
	1    8150 4050
	-1   0    0    1   
$EndComp
$Sheet
S 4800 2550 450  850 
U 5D1EFE44
F0 "sheet5D1EFE32" 20
F1 "trans_impedance_amp_arr.sch" 20
F2 "N1" I L 4800 2600 50 
F3 "N2" I L 4800 2700 50 
F4 "N3" I L 4800 2800 50 
F5 "N4" I L 4800 2900 50 
F6 "O4" O R 5250 2900 50 
F7 "O3" O R 5250 2800 50 
F8 "O2" O R 5250 2700 50 
F9 "O1" O R 5250 2600 50 
F10 "O5" O R 5250 3000 50 
F11 "O6" O R 5250 3100 50 
F12 "O7" O R 5250 3200 50 
F13 "O8" O R 5250 3300 50 
F14 "N5" I L 4800 3000 50 
F15 "N6" I L 4800 3100 50 
F16 "N7" I L 4800 3200 50 
F17 "N8" I L 4800 3300 50 
$EndSheet
Wire Wire Line
	1700 4300 1950 4300
Wire Wire Line
	1700 4400 1950 4400
Wire Wire Line
	1700 4500 1950 4500
Wire Wire Line
	1700 4600 1950 4600
Wire Wire Line
	1700 4700 1950 4700
Wire Wire Line
	1700 4800 1950 4800
Wire Wire Line
	1700 4900 1950 4900
Wire Wire Line
	1700 5000 1950 5000
Wire Wire Line
	1700 5100 1950 5100
Wire Wire Line
	1700 5200 1950 5200
Wire Wire Line
	1700 5300 1950 5300
Wire Wire Line
	1700 5400 1950 5400
Wire Wire Line
	1700 5500 1950 5500
Wire Wire Line
	1700 5600 1950 5600
Wire Wire Line
	1700 5700 1950 5700
Wire Wire Line
	1700 5800 1950 5800
Wire Wire Line
	1700 5900 1950 5900
Wire Wire Line
	1700 6000 1950 6000
Wire Wire Line
	1700 6100 1950 6100
Wire Wire Line
	1700 6200 1950 6200
Wire Wire Line
	1700 6300 1950 6300
Wire Wire Line
	1700 6400 1950 6400
Wire Wire Line
	1700 6500 1950 6500
Wire Wire Line
	1700 6600 1950 6600
Wire Wire Line
	1700 6700 1950 6700
Wire Wire Line
	1700 6800 1950 6800
Wire Wire Line
	1700 6900 1950 6900
Wire Wire Line
	1700 7000 1950 7000
Wire Wire Line
	1700 7100 1950 7100
Wire Wire Line
	1700 7200 1950 7200
Wire Wire Line
	1700 7300 1950 7300
Wire Wire Line
	1700 7400 1950 7400
$Comp
L power:+3.3V #PWR02
U 1 1 5D305DF1
P 8800 5050
AR Path="/5D305DF1" Ref="#PWR02"  Part="1" 
AR Path="/5CC637E9/5D305DF1" Ref="#PWR?"  Part="1" 
AR Path="/5CB24539/5CC637E9/5D305DF1" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5D305DF1" Ref="#PWR?"  Part="1" 
F 0 "#PWR02" H 8800 4900 50  0001 C CNN
F 1 "+3.3V" H 8815 5223 50  0000 C CNN
F 2 "" H 8800 5050 50  0001 C CNN
F 3 "" H 8800 5050 50  0001 C CNN
	1    8800 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 5100 8800 5100
Wire Wire Line
	8800 5100 8800 5050
$Comp
L power:+3.3V #PWR07
U 1 1 5D305DF9
P 10800 4850
AR Path="/5D305DF9" Ref="#PWR07"  Part="1" 
AR Path="/5CC637E9/5D305DF9" Ref="#PWR?"  Part="1" 
AR Path="/5CB24539/5CC637E9/5D305DF9" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5D305DF9" Ref="#PWR?"  Part="1" 
F 0 "#PWR07" H 10800 4700 50  0001 C CNN
F 1 "+3.3V" H 10815 5023 50  0000 C CNN
F 2 "" H 10800 4850 50  0001 C CNN
F 3 "" H 10800 4850 50  0001 C CNN
	1    10800 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10800 4900 10800 4850
Wire Wire Line
	9200 5900 8900 5900
Wire Wire Line
	8900 5900 8900 6000
$Comp
L power:GND #PWR03
U 1 1 5D305E02
P 8900 6000
AR Path="/5D305E02" Ref="#PWR03"  Part="1" 
AR Path="/5CC637E9/5D305E02" Ref="#PWR?"  Part="1" 
AR Path="/5CB24539/5CC637E9/5D305E02" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5D305E02" Ref="#PWR?"  Part="1" 
F 0 "#PWR03" H 8900 5750 50  0001 C CNN
F 1 "GND" H 8905 5827 50  0000 C CNN
F 2 "" H 8900 6000 50  0001 C CNN
F 3 "" H 8900 6000 50  0001 C CNN
	1    8900 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 6000 8900 6000
Connection ~ 8900 6000
NoConn ~ 9200 6400
NoConn ~ 9200 5300
NoConn ~ 9200 4800
$Comp
L ADG732BCPZ:ADG732BCPZ U2
U 2 1 5D305E0D
P 9200 4700
AR Path="/5D305E0D" Ref="U2"  Part="2" 
AR Path="/5CC637E9/5D305E0D" Ref="U?"  Part="2" 
AR Path="/5CB24539/5CC637E9/5D305E0D" Ref="U?"  Part="2" 
AR Path="/5CB3ED8C/5D305E0D" Ref="U?"  Part="2" 
F 0 "U2" H 10000 5087 60  0000 C CNN
F 1 "ADG732BCPZ" H 10000 4981 60  0000 C CNN
F 2 "footprints:ADG732BCPZ" H 10000 5040 60  0001 C CNN
F 3 "" H 9200 4700 60  0000 C CNN
	2    9200 4700
	1    0    0    -1  
$EndComp
NoConn ~ 3750 7650
NoConn ~ -2450 -2000
Text Label 2650 1100 0    50   ~ 0
O1
Text Label 2650 1200 0    50   ~ 0
O2
Text Label 2650 1300 0    50   ~ 0
O3
Text Label 2650 1400 0    50   ~ 0
O4
Text Label 2650 1500 0    50   ~ 0
O5
Text Label 2650 1600 0    50   ~ 0
O6
Text Label 2650 1700 0    50   ~ 0
O7
Text Label 2650 1800 0    50   ~ 0
O8
Text Label 6550 2250 2    50   ~ 0
O1
Text Label 6550 2150 2    50   ~ 0
O2
Text Label 6550 2050 2    50   ~ 0
O3
Text Label 6550 1950 2    50   ~ 0
O4
Text Label 6550 1850 2    50   ~ 0
O5
Text Label 6550 1750 2    50   ~ 0
O6
Text Label 6550 1650 2    50   ~ 0
O7
Text Label 6550 1550 2    50   ~ 0
O8
Text Label 4350 2200 0    50   ~ 0
O17
Text Label 4350 2300 0    50   ~ 0
O18
Text Label 4350 2400 0    50   ~ 0
O19
Text Label 4350 2500 0    50   ~ 0
O20
Text Label 4350 2600 0    50   ~ 0
O21
Text Label 4350 2700 0    50   ~ 0
O22
Text Label 4350 2800 0    50   ~ 0
O23
Text Label 4350 2900 0    50   ~ 0
O24
Text Label 3550 1700 0    50   ~ 0
O9
Text Label 3550 1800 0    50   ~ 0
O10
Text Label 3550 1900 0    50   ~ 0
O11
Text Label 3550 2000 0    50   ~ 0
O12
Text Label 3550 2100 0    50   ~ 0
O13
Text Label 3550 2200 0    50   ~ 0
O14
Text Label 3550 2300 0    50   ~ 0
O15
Text Label 3550 2400 0    50   ~ 0
O16
Text Label 5250 2600 0    50   ~ 0
O25
Text Label 5250 2700 0    50   ~ 0
O26
Text Label 5250 2800 0    50   ~ 0
O27
Text Label 5250 2900 0    50   ~ 0
O28
Text Label 5250 3000 0    50   ~ 0
O29
Text Label 5250 3100 0    50   ~ 0
O30
Text Label 5250 3200 0    50   ~ 0
O31
Text Label 5250 3300 0    50   ~ 0
O32
Text Label 6550 2350 2    50   ~ 0
O17
Text Label 6550 2450 2    50   ~ 0
O18
Text Label 6550 2550 2    50   ~ 0
O19
Text Label 6550 2650 2    50   ~ 0
O20
Text Label 6550 2750 2    50   ~ 0
O21
Text Label 6550 2850 2    50   ~ 0
O22
Text Label 6550 2950 2    50   ~ 0
O23
Text Label 6550 3050 2    50   ~ 0
O24
Text Label 6550 1450 2    50   ~ 0
O9
Text Label 6550 1350 2    50   ~ 0
O10
Text Label 6550 1250 2    50   ~ 0
O11
Text Label 6550 1150 2    50   ~ 0
O12
Text Label 6550 1050 2    50   ~ 0
O13
Text Label 6550 950  2    50   ~ 0
O14
Text Label 6550 850  2    50   ~ 0
O15
Text Label 6550 750  2    50   ~ 0
O16
Text Label 6550 3150 2    50   ~ 0
O25
Text Label 6550 3250 2    50   ~ 0
O26
Text Label 6550 3350 2    50   ~ 0
O27
Text Label 6550 3450 2    50   ~ 0
O28
Text Label 6550 3550 2    50   ~ 0
O29
Text Label 6550 3650 2    50   ~ 0
O30
Text Label 6550 3750 2    50   ~ 0
O31
Text Label 6550 3850 2    50   ~ 0
O32
Text Notes 6300 3600 1    79   ~ 0
Op Amp Output Maps to ADG732 MUX input\n
$Comp
L power:+3.3V #PWR063
U 1 1 5CF5FE07
P 5900 5850
AR Path="/5CF5FE07" Ref="#PWR063"  Part="1" 
AR Path="/5CC637E9/5CF5FE07" Ref="#PWR?"  Part="1" 
AR Path="/5CB24539/5CC637E9/5CF5FE07" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5CF5FE07" Ref="#PWR?"  Part="1" 
F 0 "#PWR063" H 5900 5700 50  0001 C CNN
F 1 "+3.3V" H 5915 6023 50  0000 C CNN
F 2 "" H 5900 5850 50  0001 C CNN
F 3 "" H 5900 5850 50  0001 C CNN
	1    5900 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR064
U 1 1 5CF5FE0D
P 5900 6150
AR Path="/5CF5FE0D" Ref="#PWR064"  Part="1" 
AR Path="/5CC637E9/5CF5FE0D" Ref="#PWR?"  Part="1" 
AR Path="/5CB24539/5CC637E9/5CF5FE0D" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5CF5FE0D" Ref="#PWR?"  Part="1" 
F 0 "#PWR064" H 5900 5900 50  0001 C CNN
F 1 "GND" H 5905 5977 50  0000 C CNN
F 2 "" H 5900 6150 50  0001 C CNN
F 3 "" H 5900 6150 50  0001 C CNN
	1    5900 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C33
U 1 1 5CF5FE13
P 5900 6000
F 0 "C33" H 6015 6046 50  0000 L CNN
F 1 "C" H 6015 5955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5938 5850 50  0001 C CNN
F 3 "~" H 5900 6000 50  0001 C CNN
	1    5900 6000
	1    0    0    -1  
$EndComp
Text Label 2200 1100 2    50   ~ 0
N1
Text Label 2200 1200 2    50   ~ 0
N2
Text Label 2200 1300 2    50   ~ 0
N3
Text Label 2200 1400 2    50   ~ 0
N4
Text Label 2200 1500 2    50   ~ 0
N5
Text Label 2200 1600 2    50   ~ 0
N6
Text Label 2200 1700 2    50   ~ 0
N7
Text Label 2200 1800 2    50   ~ 0
N8
Text Label 3600 4000 1    50   ~ 0
N9
Text Label 3400 4000 1    50   ~ 0
N10
Text Label 3200 4000 1    50   ~ 0
N11
Text Label 3000 4000 1    50   ~ 0
N12
Text Label 2800 4000 1    50   ~ 0
N13
Text Label 2600 4000 1    50   ~ 0
N14
Text Label 2400 4000 1    50   ~ 0
N15
Text Label 2200 4000 1    50   ~ 0
N16
Text Label 3100 1700 2    50   ~ 0
N9
Text Label 3100 1800 2    50   ~ 0
N10
Text Label 3100 1900 2    50   ~ 0
N11
Text Label 3100 2000 2    50   ~ 0
N12
Text Label 3100 2100 2    50   ~ 0
N13
Text Label 3100 2200 2    50   ~ 0
N14
Text Label 3100 2300 2    50   ~ 0
N15
Text Label 3100 2400 2    50   ~ 0
N16
Text Label 3900 2900 2    50   ~ 0
N24
Text Label 3900 2800 2    50   ~ 0
N23
Text Label 3900 2700 2    50   ~ 0
N22
Text Label 3900 2600 2    50   ~ 0
N21
Text Label 3900 2500 2    50   ~ 0
N20
Text Label 3900 2400 2    50   ~ 0
N19
Text Label 3900 2300 2    50   ~ 0
N18
Text Label 3900 2200 2    50   ~ 0
N17
Text Label 4800 3300 2    50   ~ 0
N32
Text Label 4800 3200 2    50   ~ 0
N31
Text Label 4800 3100 2    50   ~ 0
N30
Text Label 4800 3000 2    50   ~ 0
N29
Text Label 4800 2900 2    50   ~ 0
N28
Text Label 4800 2800 2    50   ~ 0
N27
Text Label 4800 2700 2    50   ~ 0
N26
Text Label 4800 2600 2    50   ~ 0
N25
Text Label 3700 4000 1    50   ~ 0
N24
Text Label 3500 4000 1    50   ~ 0
N23
Text Label 3300 4000 1    50   ~ 0
N22
Text Label 3100 4000 1    50   ~ 0
N21
Text Label 2900 4000 1    50   ~ 0
N20
Text Label 2700 4000 1    50   ~ 0
N19
Text Label 2500 4000 1    50   ~ 0
N18
Text Label 2300 4000 1    50   ~ 0
N17
Text Label 5400 4000 1    50   ~ 0
N32
Text Label 5200 4000 1    50   ~ 0
N31
Text Label 5000 4000 1    50   ~ 0
N30
Text Label 4800 4000 1    50   ~ 0
N29
Text Label 4600 4000 1    50   ~ 0
N28
Text Label 4400 4000 1    50   ~ 0
N27
Text Label 4200 4000 1    50   ~ 0
N26
Text Label 4000 4000 1    50   ~ 0
N25
Text Label 5300 4000 1    50   ~ 0
N1
Text Label 5100 4000 1    50   ~ 0
N2
Text Label 4900 4000 1    50   ~ 0
N3
Text Label 4700 4000 1    50   ~ 0
N4
Text Label 4500 4000 1    50   ~ 0
N5
Text Label 4300 4000 1    50   ~ 0
N6
Text Label 4100 4000 1    50   ~ 0
N7
Text Label 3900 4000 1    50   ~ 0
N8
$Comp
L Device:C C34
U 1 1 5CF5FDC7
P 5900 6850
F 0 "C34" H 6015 6896 50  0000 L CNN
F 1 "C" H 6015 6805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5938 6700 50  0001 C CNN
F 3 "~" H 5900 6850 50  0001 C CNN
	1    5900 6850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR066
U 1 1 5CF5FDC1
P 5900 7000
AR Path="/5CF5FDC1" Ref="#PWR066"  Part="1" 
AR Path="/5CC637E9/5CF5FDC1" Ref="#PWR?"  Part="1" 
AR Path="/5CB24539/5CC637E9/5CF5FDC1" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5CF5FDC1" Ref="#PWR?"  Part="1" 
F 0 "#PWR066" H 5900 6750 50  0001 C CNN
F 1 "GND" H 5905 6827 50  0000 C CNN
F 2 "" H 5900 7000 50  0001 C CNN
F 3 "" H 5900 7000 50  0001 C CNN
	1    5900 7000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR065
U 1 1 5CF5FDBB
P 5900 6700
AR Path="/5CF5FDBB" Ref="#PWR065"  Part="1" 
AR Path="/5CC637E9/5CF5FDBB" Ref="#PWR?"  Part="1" 
AR Path="/5CB24539/5CC637E9/5CF5FDBB" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5CF5FDBB" Ref="#PWR?"  Part="1" 
F 0 "#PWR065" H 5900 6550 50  0001 C CNN
F 1 "+3.3V" H 5915 6873 50  0000 C CNN
F 2 "" H 5900 6700 50  0001 C CNN
F 3 "" H 5900 6700 50  0001 C CNN
	1    5900 6700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
