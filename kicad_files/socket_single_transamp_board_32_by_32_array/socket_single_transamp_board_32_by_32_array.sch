EESchema Schematic File Version 4
LIBS:socket_transamp_board-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "QFN-64 Socket Board for UV 32x32 Photodiode Array with transimpedance amplifier"
Date "2019-06-10"
Rev "Revision 1"
Comp "CoolCAD Electronics LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "Nevo Magnezi"
$EndDescr
$Comp
L SamacSys_Parts:264-5205-01 J5
U 1 1 5CE4410F
P 2250 850
F 0 "J5" H 2700 1115 50  0000 C CNN
F 1 "264-5205-01" H 2700 1024 50  0000 C CNN
F 2 "SamacSys_Parts:264-5205-01_1" H 3000 950 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/264-5205-01.pdf" H 3000 850 50  0001 L CNN
F 4 "3M - 264-5205-01 - IC SOCKET, QFN, 64 POSITION, 0.5MM, THROUGH HOLE" H 3000 750 50  0001 L CNN "Description"
F 5 "" H 3000 650 50  0001 L CNN "Height"
F 6 "517-264-5205-01" H 3000 550 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=517-264-5205-01" H 3000 450 50  0001 L CNN "Mouser Price/Stock"
F 8 "3M" H 3000 350 50  0001 L CNN "Manufacturer_Name"
F 9 "264-5205-01" H 3000 250 50  0001 L CNN "Manufacturer_Part_Number"
	1    2250 850 
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J6
U 1 1 5CE4444F
P 3750 1250
F 0 "J6" H 3777 1226 50  0000 L CNN
F 1 "Conn_01x08_Female" H 3777 1135 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 3750 1250 50  0001 C CNN
F 3 "~" H 3750 1250 50  0001 C CNN
	1    3750 1250
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J7
U 1 1 5CE44504
P 3750 2050
F 0 "J7" H 3777 2026 50  0000 L CNN
F 1 "Conn_01x08_Female" H 3777 1935 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 3750 2050 50  0001 C CNN
F 3 "~" H 3750 2050 50  0001 C CNN
	1    3750 2050
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J8
U 1 1 5CE445CA
P 3750 2850
F 0 "J8" H 3777 2826 50  0000 L CNN
F 1 "Conn_01x08_Female" H 3777 2735 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 3750 2850 50  0001 C CNN
F 3 "~" H 3750 2850 50  0001 C CNN
	1    3750 2850
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J9
U 1 1 5CE445D1
P 3750 3650
F 0 "J9" H 3777 3626 50  0000 L CNN
F 1 "Conn_01x08_Female" H 3777 3535 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 3750 3650 50  0001 C CNN
F 3 "~" H 3750 3650 50  0001 C CNN
	1    3750 3650
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J1
U 1 1 5CE44772
P 1650 1150
F 0 "J1" H 1677 1126 50  0000 L CNN
F 1 "Conn_01x08_Female" H 1677 1035 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 1650 1150 50  0001 C CNN
F 3 "~" H 1650 1150 50  0001 C CNN
	1    1650 1150
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J2
U 1 1 5CE44779
P 1650 1950
F 0 "J2" H 1677 1926 50  0000 L CNN
F 1 "Conn_01x08_Female" H 1677 1835 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 1650 1950 50  0001 C CNN
F 3 "~" H 1650 1950 50  0001 C CNN
	1    1650 1950
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J3
U 1 1 5CE44780
P 1650 2750
F 0 "J3" H 1677 2726 50  0000 L CNN
F 1 "Conn_01x08_Female" H 1677 2635 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 1650 2750 50  0001 C CNN
F 3 "~" H 1650 2750 50  0001 C CNN
	1    1650 2750
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J4
U 1 1 5CE44787
P 1650 3550
F 0 "J4" H 1677 3526 50  0000 L CNN
F 1 "Conn_01x08_Female" H 1677 3435 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 1650 3550 50  0001 C CNN
F 3 "~" H 1650 3550 50  0001 C CNN
	1    1650 3550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2600 4200 2600 4300
Wire Wire Line
	2600 4300 2700 4300
Wire Wire Line
	2800 4300 2800 4200
Wire Wire Line
	2700 4200 2700 4300
Connection ~ 2700 4300
Wire Wire Line
	2700 4300 2800 4300
$Comp
L power:GND #PWR0101
U 1 1 5CED5B07
P 2700 4300
F 0 "#PWR0101" H 2700 4050 50  0001 C CNN
F 1 "GND" H 2705 4127 50  0000 C CNN
F 2 "" H 2700 4300 50  0001 C CNN
F 3 "" H 2700 4300 50  0001 C CNN
	1    2700 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 850  2250 850 
Text Label 2000 850  0    50   ~ 0
N2
Wire Wire Line
	1850 950  2250 950 
Text Label 2000 950  0    50   ~ 0
N6
Wire Wire Line
	1850 1050 2250 1050
Text Label 2000 1050 0    50   ~ 0
N10
Wire Wire Line
	1850 1150 2250 1150
Text Label 2000 1150 0    50   ~ 0
N14
Wire Wire Line
	1850 1250 2250 1250
Text Label 2000 1250 0    50   ~ 0
N18
Wire Wire Line
	1850 1350 2250 1350
Text Label 2000 1350 0    50   ~ 0
N22
Wire Wire Line
	1850 1450 2250 1450
Text Label 2000 1450 0    50   ~ 0
N26
Wire Wire Line
	1850 1550 2250 1550
Text Label 2000 1550 0    50   ~ 0
N30
Wire Wire Line
	1850 1650 2250 1650
Text Label 2000 1650 0    50   ~ 0
P1
Wire Wire Line
	1850 1750 2250 1750
Text Label 2000 1750 0    50   ~ 0
P5
Wire Wire Line
	1850 1850 2250 1850
Text Label 2000 1850 0    50   ~ 0
P9
Wire Wire Line
	1850 1950 2250 1950
Text Label 2000 1950 0    50   ~ 0
P13
Wire Wire Line
	1850 2050 2250 2050
Text Label 2000 2050 0    50   ~ 0
P17
Wire Wire Line
	1850 2150 2250 2150
Text Label 2000 2150 0    50   ~ 0
P21
Wire Wire Line
	1850 2250 2250 2250
Text Label 2000 2250 0    50   ~ 0
P25
Wire Wire Line
	1850 2350 2250 2350
Text Label 2000 2350 0    50   ~ 0
P29
Wire Wire Line
	1850 2450 2250 2450
Text Label 2000 2450 0    50   ~ 0
N31
Wire Wire Line
	1850 2550 2250 2550
Text Label 2000 2550 0    50   ~ 0
N27
Wire Wire Line
	1850 2650 2250 2650
Text Label 2000 2650 0    50   ~ 0
N23
Wire Wire Line
	1850 2750 2250 2750
Text Label 2000 2750 0    50   ~ 0
N19
Wire Wire Line
	1850 2850 2250 2850
Text Label 2000 2850 0    50   ~ 0
N15
Wire Wire Line
	1850 2950 2250 2950
Text Label 2000 2950 0    50   ~ 0
N11
Wire Wire Line
	1850 3050 2250 3050
Text Label 2000 3050 0    50   ~ 0
N7
Wire Wire Line
	1850 3150 2250 3150
Text Label 2000 3150 0    50   ~ 0
N3
Wire Wire Line
	1850 3250 2250 3250
Text Label 2000 3250 0    50   ~ 0
P30
Wire Wire Line
	1850 3350 2250 3350
Text Label 2000 3350 0    50   ~ 0
P26
Wire Wire Line
	1850 3450 2250 3450
Text Label 2000 3450 0    50   ~ 0
P22
Wire Wire Line
	1850 3550 2250 3550
Text Label 2000 3550 0    50   ~ 0
P18
Wire Wire Line
	1850 3650 2250 3650
Text Label 2000 3650 0    50   ~ 0
P14
Wire Wire Line
	1850 3750 2250 3750
Text Label 2000 3750 0    50   ~ 0
P10
Wire Wire Line
	1850 3850 2250 3850
Text Label 2000 3850 0    50   ~ 0
P6
Wire Wire Line
	1850 3950 2250 3950
Text Label 2000 3950 0    50   ~ 0
P2
Wire Wire Line
	3150 850  3550 850 
Text Label 3300 850  0    50   ~ 0
N4
Wire Wire Line
	3150 950  3550 950 
Text Label 3300 950  0    50   ~ 0
N8
Wire Wire Line
	3150 1050 3550 1050
Text Label 3300 1050 0    50   ~ 0
N12
Wire Wire Line
	3150 1150 3550 1150
Text Label 3300 1150 0    50   ~ 0
N16
Wire Wire Line
	3150 1250 3550 1250
Text Label 3300 1250 0    50   ~ 0
N20
Wire Wire Line
	3150 1350 3550 1350
Text Label 3300 1350 0    50   ~ 0
N24
Wire Wire Line
	3150 1450 3550 1450
Text Label 3300 1450 0    50   ~ 0
N28
Wire Wire Line
	3150 1550 3550 1550
Text Label 3300 1550 0    50   ~ 0
N32
Wire Wire Line
	3150 1650 3550 1650
Text Label 3300 1650 0    50   ~ 0
P3
Wire Wire Line
	3150 1750 3550 1750
Text Label 3300 1750 0    50   ~ 0
P7
Wire Wire Line
	3150 1850 3550 1850
Text Label 3300 1850 0    50   ~ 0
P11
Wire Wire Line
	3150 1950 3550 1950
Text Label 3300 1950 0    50   ~ 0
P15
Wire Wire Line
	3150 2050 3550 2050
Text Label 3300 2050 0    50   ~ 0
P19
Wire Wire Line
	3150 2150 3550 2150
Text Label 3300 2150 0    50   ~ 0
P23
Wire Wire Line
	3150 2250 3550 2250
Text Label 3300 2250 0    50   ~ 0
P27
Wire Wire Line
	3150 2350 3550 2350
Text Label 3300 2350 0    50   ~ 0
P31
Wire Wire Line
	3150 2450 3550 2450
Text Label 3300 2450 0    50   ~ 0
N29
Wire Wire Line
	3150 2550 3550 2550
Text Label 3300 2550 0    50   ~ 0
N25
Wire Wire Line
	3150 2650 3550 2650
Text Label 3300 2650 0    50   ~ 0
N21
Wire Wire Line
	3150 2750 3550 2750
Text Label 3300 2750 0    50   ~ 0
N17
Wire Wire Line
	3150 2850 3550 2850
Text Label 3300 2850 0    50   ~ 0
N13
Wire Wire Line
	3150 2950 3550 2950
Text Label 3300 2950 0    50   ~ 0
N9
Wire Wire Line
	3150 3050 3550 3050
Text Label 3300 3050 0    50   ~ 0
N5
Wire Wire Line
	3150 3150 3550 3150
Text Label 3300 3150 0    50   ~ 0
N1
Wire Wire Line
	3150 3250 3550 3250
Text Label 3300 3250 0    50   ~ 0
P32
Wire Wire Line
	3150 3350 3550 3350
Text Label 3300 3350 0    50   ~ 0
P28
Wire Wire Line
	3150 3450 3550 3450
Text Label 3300 3450 0    50   ~ 0
P24
Wire Wire Line
	3150 3550 3550 3550
Text Label 3300 3550 0    50   ~ 0
P20
Wire Wire Line
	3150 3650 3550 3650
Text Label 3300 3650 0    50   ~ 0
P16
Wire Wire Line
	3150 3750 3550 3750
Text Label 3300 3750 0    50   ~ 0
P12
Wire Wire Line
	3150 3850 3550 3850
Text Label 3300 3850 0    50   ~ 0
P8
Wire Wire Line
	3150 3950 3550 3950
Text Label 3300 3950 0    50   ~ 0
P4
$Comp
L 2019-06-10_17-42-28:1-1571983-1 SW1
U 1 1 5CFF1D8A
P 6550 1000
F 0 "SW1" H 7350 1387 60  0000 C CNN
F 1 "1-1571983-1" H 7350 1281 60  0000 C CNN
F 2 "footprints:1-1571983-1" H 7350 1240 60  0001 C CNN
F 3 "" H 6550 1000 60  0000 C CNN
	1    6550 1000
	1    0    0    -1  
$EndComp
$Comp
L 2019-06-10_17-42-28:1-1571983-1 SW2
U 1 1 5CFF1F3B
P 8150 2400
F 0 "SW2" H 8950 2787 60  0000 C CNN
F 1 "1-1571983-1" H 8950 2681 60  0000 C CNN
F 2 "footprints:1-1571983-1" H 8950 2640 60  0001 C CNN
F 3 "" H 8150 2400 60  0000 C CNN
	1    8150 2400
	-1   0    0    -1  
$EndComp
$Comp
L 2019-06-10_17-42-28:1-1571983-1 SW3
U 1 1 5CFF3AB4
P 6550 3800
F 0 "SW3" H 7350 4187 60  0000 C CNN
F 1 "1-1571983-1" H 7350 4081 60  0000 C CNN
F 2 "footprints:1-1571983-1" H 7350 4040 60  0001 C CNN
F 3 "" H 6550 3800 60  0000 C CNN
	1    6550 3800
	1    0    0    -1  
$EndComp
$Comp
L 2019-06-10_17-42-28:1-1571983-1 SW4
U 1 1 5CFF3ABB
P 8150 5200
F 0 "SW4" H 8950 5587 60  0000 C CNN
F 1 "1-1571983-1" H 8950 5481 60  0000 C CNN
F 2 "footprints:1-1571983-1" H 8950 5440 60  0001 C CNN
F 3 "" H 8150 5200 60  0000 C CNN
	1    8150 5200
	-1   0    0    -1  
$EndComp
$Comp
L 2019-06-10_17-42-28:1-1571983-1 SW5
U 1 1 5CFF5650
P 10300 1050
F 0 "SW5" H 11100 1437 60  0000 C CNN
F 1 "1-1571983-1" H 11100 1331 60  0000 C CNN
F 2 "footprints:1-1571983-1" H 11100 1290 60  0001 C CNN
F 3 "" H 10300 1050 60  0000 C CNN
	1    10300 1050
	-1   0    0    -1  
$EndComp
$Comp
L 2019-06-10_17-42-28:1-1571983-1 SW6
U 1 1 5CFF5657
P 8700 2450
F 0 "SW6" H 9500 2837 60  0000 C CNN
F 1 "1-1571983-1" H 9500 2731 60  0000 C CNN
F 2 "footprints:1-1571983-1" H 9500 2690 60  0001 C CNN
F 3 "" H 8700 2450 60  0000 C CNN
	1    8700 2450
	1    0    0    -1  
$EndComp
$Comp
L 2019-06-10_17-42-28:1-1571983-1 SW7
U 1 1 5CFF565E
P 10300 3850
F 0 "SW7" H 11100 4237 60  0000 C CNN
F 1 "1-1571983-1" H 11100 4131 60  0000 C CNN
F 2 "footprints:1-1571983-1" H 11100 4090 60  0001 C CNN
F 3 "" H 10300 3850 60  0000 C CNN
	1    10300 3850
	-1   0    0    -1  
$EndComp
$Comp
L 2019-06-10_17-42-28:1-1571983-1 SW8
U 1 1 5CFF5665
P 8700 5250
F 0 "SW8" H 9500 5637 60  0000 C CNN
F 1 "1-1571983-1" H 9500 5531 60  0000 C CNN
F 2 "footprints:1-1571983-1" H 9500 5490 60  0001 C CNN
F 3 "" H 8700 5250 60  0000 C CNN
	1    8700 5250
	1    0    0    -1  
$EndComp
Connection ~ 8150 1100
Wire Wire Line
	8150 1100 8150 1000
Connection ~ 8150 1200
Wire Wire Line
	8150 1200 8150 1100
Connection ~ 8150 1300
Wire Wire Line
	8150 1300 8150 1200
Connection ~ 8150 1400
Wire Wire Line
	8150 1400 8150 1300
Connection ~ 8150 1500
Wire Wire Line
	8150 1500 8150 1400
Connection ~ 8150 1600
Wire Wire Line
	8150 1600 8150 1500
Connection ~ 8150 1700
Wire Wire Line
	8150 1700 8150 1600
Connection ~ 8150 3800
Wire Wire Line
	8150 3800 8150 3550
Connection ~ 8150 3900
Wire Wire Line
	8150 3900 8150 3800
Connection ~ 8150 4000
Wire Wire Line
	8150 4000 8150 3900
Connection ~ 8150 4100
Wire Wire Line
	8150 4100 8150 4000
Connection ~ 8150 4200
Wire Wire Line
	8150 4200 8150 4100
Connection ~ 8150 4300
Wire Wire Line
	8150 4300 8150 4200
Connection ~ 8150 4400
Wire Wire Line
	8150 4400 8150 4300
Connection ~ 8150 4500
Wire Wire Line
	8150 4500 8150 4400
Connection ~ 10300 2450
Wire Wire Line
	10300 2450 10300 2550
Connection ~ 10300 2550
Wire Wire Line
	10300 2550 10300 2650
Connection ~ 10300 2650
Wire Wire Line
	10300 2650 10300 2750
Connection ~ 10300 2750
Wire Wire Line
	10300 2750 10300 2850
Connection ~ 10300 2850
Wire Wire Line
	10300 2850 10300 2950
Connection ~ 10300 2950
Wire Wire Line
	10300 2950 10300 3050
Connection ~ 10300 3050
Wire Wire Line
	10300 3050 10300 3150
Connection ~ 10300 3150
Connection ~ 10300 5250
Wire Wire Line
	10300 5250 10300 5350
Connection ~ 10300 5350
Wire Wire Line
	10300 5350 10300 5450
Connection ~ 10300 5450
Wire Wire Line
	10300 5450 10300 5550
Connection ~ 10300 5550
Wire Wire Line
	10300 5550 10300 5650
Connection ~ 10300 5650
Wire Wire Line
	10300 5650 10300 5750
Connection ~ 10300 5750
Wire Wire Line
	10300 5750 10300 5850
Connection ~ 10300 5850
Wire Wire Line
	10300 5850 10300 5950
Text Label 6550 5200 2    50   ~ 0
N4
Text Label 6550 5300 2    50   ~ 0
N8
Text Label 6550 5400 2    50   ~ 0
N12
Text Label 6550 5500 2    50   ~ 0
N16
Text Label 6550 5600 2    50   ~ 0
N20
Text Label 6550 5700 2    50   ~ 0
N24
Text Label 6550 5800 2    50   ~ 0
N28
Text Label 6550 5900 2    50   ~ 0
N32
Text Label 8700 3850 2    50   ~ 0
P3
Text Label 8700 3950 2    50   ~ 0
P7
Text Label 8700 4050 2    50   ~ 0
P11
Text Label 8700 4150 2    50   ~ 0
P15
Text Label 8700 4250 2    50   ~ 0
P19
Text Label 8700 4350 2    50   ~ 0
P23
Text Label 8700 4450 2    50   ~ 0
P27
Text Label 8700 4550 2    50   ~ 0
P31
Text Label 6550 1700 2    50   ~ 0
N29
Text Label 6550 1600 2    50   ~ 0
N25
Text Label 6550 1500 2    50   ~ 0
N21
Text Label 6550 1400 2    50   ~ 0
N17
Text Label 6550 1300 2    50   ~ 0
N13
Text Label 6550 1200 2    50   ~ 0
N9
Text Label 6550 1100 2    50   ~ 0
N5
Text Label 6550 1000 2    50   ~ 0
N1
Text Label 8700 5950 2    50   ~ 0
P32
Text Label 8700 5850 2    50   ~ 0
P28
Text Label 8700 5750 2    50   ~ 0
P24
Text Label 8700 5650 2    50   ~ 0
P20
Text Label 8700 5550 2    50   ~ 0
P16
Text Label 8700 5450 2    50   ~ 0
P12
Text Label 8700 5350 2    50   ~ 0
P8
Text Label 8700 5250 2    50   ~ 0
P4
Text Label 6550 2400 2    50   ~ 0
N2
Text Label 6550 2500 2    50   ~ 0
N6
Text Label 6550 2600 2    50   ~ 0
N10
Text Label 6550 2700 2    50   ~ 0
N14
Text Label 6550 2800 2    50   ~ 0
N18
Text Label 6550 2900 2    50   ~ 0
N22
Text Label 6550 3000 2    50   ~ 0
N26
Text Label 6550 3100 2    50   ~ 0
N30
Text Label 8700 1050 2    50   ~ 0
P1
Text Label 8700 1150 2    50   ~ 0
P5
Text Label 8700 1250 2    50   ~ 0
P9
Text Label 8700 1350 2    50   ~ 0
P13
Text Label 8700 1450 2    50   ~ 0
P17
Text Label 8700 1550 2    50   ~ 0
P21
Text Label 8700 1650 2    50   ~ 0
P25
Text Label 8700 1750 2    50   ~ 0
P29
Text Label 6550 4500 2    50   ~ 0
N31
Text Label 6550 4400 2    50   ~ 0
N27
Text Label 6550 4300 2    50   ~ 0
N23
Text Label 6550 4200 2    50   ~ 0
N19
Text Label 6550 4100 2    50   ~ 0
N15
Text Label 6550 4000 2    50   ~ 0
N11
Text Label 6550 3900 2    50   ~ 0
N7
Text Label 6550 3800 2    50   ~ 0
N3
Text Label 8700 3150 2    50   ~ 0
P30
Text Label 8700 3050 2    50   ~ 0
P26
Text Label 8700 2950 2    50   ~ 0
P22
Text Label 8700 2850 2    50   ~ 0
P18
Text Label 8700 2750 2    50   ~ 0
P14
Text Label 8700 2650 2    50   ~ 0
P10
Text Label 8700 2550 2    50   ~ 0
P6
Text Label 8700 2450 2    50   ~ 0
P2
Text Label 8350 3550 0    79   ~ 0
ARRAY_N_SIDE
Wire Wire Line
	8350 3550 8150 3550
Connection ~ 8150 3550
Wire Wire Line
	10500 3550 10300 3550
Connection ~ 10300 3550
$Comp
L power:GND #PWR0102
U 1 1 5D09901C
P 2000 6800
F 0 "#PWR0102" H 2000 6550 50  0001 C CNN
F 1 "GND" H 2005 6627 50  0000 C CNN
F 2 "" H 2000 6800 50  0001 C CNN
F 3 "" H 2000 6800 50  0001 C CNN
	1    2000 6800
	1    0    0    -1  
$EndComp
$Comp
L socket_transamp_board-rescue:+3.3V-photodiode_readout_circuit_rev3-cache #PWR0103
U 1 1 5D0990C5
P 3450 7400
F 0 "#PWR0103" H 3450 7250 50  0001 C CNN
F 1 "+3.3V" H 3550 7550 50  0000 C CNN
F 2 "" H 3450 7400 50  0001 C CNN
F 3 "" H 3450 7400 50  0001 C CNN
	1    3450 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 6500 3000 6900
$Comp
L Device:R R1
U 1 1 5D09CFF8
P 3000 7050
F 0 "R1" V 3070 7096 50  0000 L CNN
F 1 "R" H 3070 7005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2930 7050 50  0001 C CNN
F 3 "~" H 3000 7050 50  0001 C CNN
	1    3000 7050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5D09D0A5
P 2650 7050
F 0 "C1" H 2765 7096 50  0000 L CNN
F 1 "C" H 2765 7005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2688 6900 50  0001 C CNN
F 3 "~" H 2650 7050 50  0001 C CNN
	1    2650 7050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J10
U 1 1 5D09D2AE
P 1450 6650
F 0 "J10" H 1477 6626 50  0000 L CNN
F 1 "Conn_01x08_Female" H 1477 6535 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 1450 6650 50  0001 C CNN
F 3 "~" H 1450 6650 50  0001 C CNN
	1    1450 6650
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5D0D22F3
P 3800 5550
F 0 "#PWR0104" H 3800 5300 50  0001 C CNN
F 1 "GND" H 4000 5450 50  0000 C CNN
F 2 "" H 3800 5550 50  0001 C CNN
F 3 "" H 3800 5550 50  0001 C CNN
	1    3800 5550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5D0DAA9A
P 4350 5250
F 0 "R8" V 4143 5250 50  0000 C CNN
F 1 "R" V 4234 5250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4280 5250 50  0001 C CNN
F 3 "~" H 4350 5250 50  0001 C CNN
	1    4350 5250
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5D0DAAA2
P 3900 5400
F 0 "R6" H 3830 5354 50  0000 R CNN
F 1 "R" H 3830 5445 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3830 5400 50  0001 C CNN
F 3 "~" H 3900 5400 50  0001 C CNN
	1    3900 5400
	-1   0    0    1   
$EndComp
Connection ~ 3900 5250
$Comp
L Device:C C2
U 1 1 5D0F043E
P 3400 4600
F 0 "C2" V 3148 4600 50  0000 C CNN
F 1 "C" V 3239 4600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3438 4450 50  0001 C CNN
F 3 "~" H 3400 4600 50  0001 C CNN
	1    3400 4600
	0    1    1    0   
$EndComp
Connection ~ 2950 5250
Wire Wire Line
	2650 7200 2650 7400
Wire Wire Line
	2650 7400 3000 7400
Wire Wire Line
	3000 7400 3000 7200
Wire Wire Line
	2650 6900 2650 6500
Wire Wire Line
	2650 6500 3000 6500
Connection ~ 3000 6500
Wire Wire Line
	2650 7400 2250 7400
Wire Wire Line
	2250 7400 2250 7050
Wire Wire Line
	2250 7050 1650 7050
Connection ~ 2650 7400
Wire Wire Line
	2950 5250 2200 5250
Text Label 2200 5250 2    79   ~ 0
ARRAY_N_SIDE
$Comp
L power:GND #PWR0105
U 1 1 5D14019C
P 10500 3550
F 0 "#PWR0105" H 10500 3300 50  0001 C CNN
F 1 "GND" H 10505 3377 50  0000 C CNN
F 2 "" H 10500 3550 50  0001 C CNN
F 3 "" H 10500 3550 50  0001 C CNN
	1    10500 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 6850 1800 6850
Wire Wire Line
	1800 6850 1800 6800
Wire Wire Line
	1800 6800 2000 6800
$Comp
L socket_transamp_board-rescue:+3.3V-photodiode_readout_circuit_rev3-cache #PWR0106
U 1 1 5D14B2E4
P 2000 6650
F 0 "#PWR0106" H 2000 6500 50  0001 C CNN
F 1 "+3.3V" H 2150 6700 50  0000 C CNN
F 2 "" H 2000 6650 50  0001 C CNN
F 3 "" H 2000 6650 50  0001 C CNN
	1    2000 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 6650 1650 6650
Wire Wire Line
	1650 7050 1650 6950
Connection ~ 1650 7050
Wire Wire Line
	1650 6850 1650 6750
Connection ~ 1650 6850
Wire Wire Line
	1650 6550 1650 6650
Connection ~ 1650 6650
Text Label 1650 6350 0    79   ~ 0
ARRAY_N_SIDE
$Comp
L Device:R R2
U 1 1 5D18D538
P 3250 7050
F 0 "R2" H 3320 7096 50  0000 L CNN
F 1 "R" H 3320 7005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3180 7050 50  0001 C CNN
F 3 "~" H 3250 7050 50  0001 C CNN
	1    3250 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 6900 3250 6500
Wire Wire Line
	3250 6500 3000 6500
Wire Wire Line
	3250 7200 3250 7400
Wire Wire Line
	3250 7400 3000 7400
Connection ~ 3000 7400
$Comp
L Device:R R7
U 1 1 5D1A6EE0
P 4350 4950
F 0 "R7" V 4143 4950 50  0000 C CNN
F 1 "R" V 4234 4950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4280 4950 50  0001 C CNN
F 3 "~" H 4350 4950 50  0001 C CNN
	1    4350 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	4500 4950 4750 4950
Wire Wire Line
	4750 4950 4750 5250
Wire Wire Line
	4500 5250 4750 5250
Wire Wire Line
	4200 5250 4100 5250
$Comp
L Device:R R4
U 1 1 5D1F0C20
P 3400 5250
F 0 "R4" V 3193 5250 50  0000 C CNN
F 1 "R" V 3284 5250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3330 5250 50  0001 C CNN
F 3 "~" H 3400 5250 50  0001 C CNN
	1    3400 5250
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5D1F0C27
P 3400 4950
F 0 "R3" V 3193 4950 50  0000 C CNN
F 1 "R" V 3284 4950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3330 4950 50  0001 C CNN
F 3 "~" H 3400 4950 50  0001 C CNN
	1    3400 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 5250 2950 5250
Wire Wire Line
	3250 4950 2950 4950
Wire Wire Line
	3550 5250 3600 5250
Wire Wire Line
	2950 4600 3250 4600
Wire Wire Line
	3550 4600 3600 4600
$Comp
L Device:R R5
U 1 1 5D22F7CF
P 3700 5400
F 0 "R5" H 3630 5354 50  0000 R CNN
F 1 "R" H 3630 5445 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3630 5400 50  0001 C CNN
F 3 "~" H 3700 5400 50  0001 C CNN
	1    3700 5400
	-1   0    0    1   
$EndComp
Connection ~ 3700 5250
Wire Wire Line
	3700 5250 3900 5250
Wire Wire Line
	3700 5550 3800 5550
Connection ~ 3800 5550
Wire Wire Line
	3800 5550 3900 5550
Wire Wire Line
	3600 5250 3600 4950
Connection ~ 3600 5250
Wire Wire Line
	3600 5250 3700 5250
Wire Wire Line
	4200 4950 4100 4950
Wire Wire Line
	4100 4950 4100 5250
Connection ~ 4100 5250
Wire Wire Line
	4100 5250 3900 5250
Wire Wire Line
	3600 4950 3550 4950
Connection ~ 3600 4950
Wire Wire Line
	3600 4950 3600 4600
Text Label 1650 6450 0    47   ~ 0
V_OUT
Text Label 5450 6350 0    47   ~ 0
V_OUT
$Comp
L power:GND #PWR0107
U 1 1 5D280C27
P 3650 7350
F 0 "#PWR0107" H 3650 7100 50  0001 C CNN
F 1 "GND" H 3850 7250 50  0000 C CNN
F 2 "" H 3650 7350 50  0001 C CNN
F 3 "" H 3650 7350 50  0001 C CNN
	1    3650 7350
	1    0    0    -1  
$EndComp
NoConn ~ 2900 7750
NoConn ~ 3250 8400
NoConn ~ 2450 8500
Text Label 2250 6950 0    59   ~ 0
V_BIAS
Wire Wire Line
	2250 7050 2250 6950
Connection ~ 2250 7050
Wire Wire Line
	2950 4600 2950 4950
Connection ~ 2950 4950
Wire Wire Line
	2950 4950 2950 5250
Wire Wire Line
	8150 1700 8150 2400
Connection ~ 8150 2400
Wire Wire Line
	8150 2400 8150 2500
Connection ~ 8150 2500
Wire Wire Line
	8150 2500 8150 2600
Connection ~ 8150 2600
Wire Wire Line
	8150 2600 8150 2700
Connection ~ 8150 2700
Wire Wire Line
	8150 2700 8150 2800
Connection ~ 8150 2800
Wire Wire Line
	8150 2800 8150 2900
Connection ~ 8150 2900
Wire Wire Line
	8150 2900 8150 3000
Connection ~ 8150 3000
Wire Wire Line
	8150 3000 8150 3100
Connection ~ 8150 3100
Wire Wire Line
	8150 3100 8150 3550
Wire Wire Line
	10300 1750 10300 2450
Wire Wire Line
	10300 1750 10300 1650
Connection ~ 10300 1750
Connection ~ 10300 1150
Wire Wire Line
	10300 1150 10300 1050
Connection ~ 10300 1250
Wire Wire Line
	10300 1250 10300 1150
Connection ~ 10300 1350
Wire Wire Line
	10300 1350 10300 1250
Connection ~ 10300 1450
Wire Wire Line
	10300 1450 10300 1350
Connection ~ 10300 1550
Wire Wire Line
	10300 1550 10300 1450
Connection ~ 10300 1650
Wire Wire Line
	10300 1650 10300 1550
Wire Wire Line
	10300 3550 10300 3850
Wire Wire Line
	10300 4550 10300 5250
Wire Wire Line
	10300 4550 10300 4450
Connection ~ 10300 4550
Connection ~ 10300 3850
Connection ~ 10300 3950
Wire Wire Line
	10300 3950 10300 3850
Connection ~ 10300 4050
Wire Wire Line
	10300 4050 10300 3950
Connection ~ 10300 4150
Wire Wire Line
	10300 4150 10300 4050
Connection ~ 10300 4250
Wire Wire Line
	10300 4250 10300 4150
Connection ~ 10300 4350
Wire Wire Line
	10300 4350 10300 4250
Connection ~ 10300 4450
Wire Wire Line
	10300 4450 10300 4350
Wire Wire Line
	8150 5800 8150 5900
Wire Wire Line
	8150 5800 8150 5700
Connection ~ 8150 5800
Connection ~ 8150 5400
Connection ~ 8150 5500
Wire Wire Line
	8150 5500 8150 5400
Connection ~ 8150 5600
Wire Wire Line
	8150 5600 8150 5500
Connection ~ 8150 5700
Wire Wire Line
	8150 5700 8150 5600
Wire Wire Line
	8150 4500 8150 5200
Connection ~ 8150 5200
Wire Wire Line
	8150 5200 8150 5300
Connection ~ 8150 5300
Wire Wire Line
	8150 5300 8150 5400
Wire Wire Line
	10300 3200 10300 3550
Wire Wire Line
	10300 3150 10300 3550
$Comp
L 2019-07-19_19-42-07:LMP2232AMM_NOPB U1
U 1 1 5D3A610E
P 3850 6450
F 0 "U1" H 4650 6937 60  0000 C CNN
F 1 "LMP2232AMM_NOPB" H 4650 6831 60  0000 C CNN
F 2 "MUA08A" H 4650 6790 60  0001 C CNN
F 3 "" H 3850 6450 60  0000 C CNN
	1    3850 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 7400 3450 7550
Wire Wire Line
	3450 7550 3800 7550
Wire Wire Line
	3800 7550 3800 7450
Wire Wire Line
	3800 7450 3850 7450
Wire Wire Line
	3650 7350 3850 7350
Wire Wire Line
	3850 6550 3500 6550
Wire Wire Line
	3500 6550 3500 6500
Wire Wire Line
	3500 6500 3250 6500
Connection ~ 3250 6500
Wire Wire Line
	2950 5250 2950 6200
Wire Wire Line
	2950 6200 3800 6200
Wire Wire Line
	3800 6200 3800 6450
Wire Wire Line
	3800 6450 3850 6450
Wire Wire Line
	3850 6850 3500 6850
Wire Wire Line
	3500 6850 3500 6550
Connection ~ 3500 6550
Wire Wire Line
	3850 6750 3650 6750
Wire Wire Line
	3650 6750 3650 5850
Wire Wire Line
	3650 5850 5900 5850
Wire Wire Line
	5900 5850 5900 6450
Wire Wire Line
	5900 6450 5450 6450
Wire Wire Line
	5450 6350 5450 5250
Wire Wire Line
	5450 5250 4750 5250
Connection ~ 4750 5250
$EndSCHEMATC
