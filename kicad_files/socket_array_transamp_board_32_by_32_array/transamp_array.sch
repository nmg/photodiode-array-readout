EESchema Schematic File Version 4
LIBS:socket_transamp_board_32_by_32_array-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 18
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1650 1300 1500 400 
U 5D1701D6
F0 "sheet5D1701D0" 50
F1 "dual_trans_amp.sch" 50
F2 "transamp_in_a" I L 1650 1400 50 
F3 "transamp_in_b" I L 1650 1600 50 
F4 "transamp_out_a" O R 3150 1400 50 
F5 "transamp_out_b" O R 3150 1600 50 
$EndSheet
$Sheet
S 1650 2050 1500 400 
U 5D1701E2
F0 "sheet5D1701DC" 50
F1 "dual_trans_amp.sch" 50
F2 "transamp_in_a" I L 1650 2150 50 
F3 "transamp_in_b" I L 1650 2350 50 
F4 "transamp_out_a" O R 3150 2150 50 
F5 "transamp_out_b" O R 3150 2350 50 
$EndSheet
$Sheet
S 1650 2850 1500 400 
U 5D1701FA
F0 "sheet5D1701EE" 50
F1 "dual_trans_amp.sch" 50
F2 "transamp_in_a" I L 1650 2950 50 
F3 "transamp_in_b" I L 1650 3150 50 
F4 "transamp_out_a" O R 3150 2950 50 
F5 "transamp_out_b" O R 3150 3150 50 
$EndSheet
$Sheet
S 1650 3600 1500 400 
U 5D170218
F0 "sheet5D17020F" 50
F1 "dual_trans_amp.sch" 50
F2 "transamp_in_a" I L 1650 3700 50 
F3 "transamp_in_b" I L 1650 3900 50 
F4 "transamp_out_a" O R 3150 3700 50 
F5 "transamp_out_b" O R 3150 3900 50 
$EndSheet
$Sheet
S 1650 4350 1500 400 
U 5D17021E
F0 "sheet5D170210" 50
F1 "dual_trans_amp.sch" 50
F2 "transamp_in_a" I L 1650 4450 50 
F3 "transamp_in_b" I L 1650 4650 50 
F4 "transamp_out_a" O R 3150 4450 50 
F5 "transamp_out_b" O R 3150 4650 50 
$EndSheet
$Sheet
S 1650 5100 1500 400 
U 5D170224
F0 "sheet5D170211" 50
F1 "dual_trans_amp.sch" 50
F2 "transamp_in_a" I L 1650 5200 50 
F3 "transamp_in_b" I L 1650 5400 50 
F4 "transamp_out_a" O R 3150 5200 50 
F5 "transamp_out_b" O R 3150 5400 50 
$EndSheet
$Sheet
S 1650 5900 1500 400 
U 5D17022A
F0 "sheet5D170212" 50
F1 "dual_trans_amp.sch" 50
F2 "transamp_in_a" I L 1650 6000 50 
F3 "transamp_in_b" I L 1650 6200 50 
F4 "transamp_out_a" O R 3150 6000 50 
F5 "transamp_out_b" O R 3150 6200 50 
$EndSheet
$Sheet
S 4250 1350 1500 400 
U 5D170260
F0 "sheet5D170253" 50
F1 "dual_trans_amp.sch" 50
F2 "transamp_in_a" I L 4250 1450 50 
F3 "transamp_in_b" I L 4250 1650 50 
F4 "transamp_out_a" O R 5750 1450 50 
F5 "transamp_out_b" O R 5750 1650 50 
$EndSheet
$Sheet
S 4250 2150 1500 400 
U 5D170266
F0 "sheet5D170254" 50
F1 "dual_trans_amp.sch" 50
F2 "transamp_in_a" I L 4250 2250 50 
F3 "transamp_in_b" I L 4250 2450 50 
F4 "transamp_out_a" O R 5750 2250 50 
F5 "transamp_out_b" O R 5750 2450 50 
$EndSheet
$Sheet
S 4250 2900 1500 400 
U 5D17026C
F0 "sheet5D170255" 50
F1 "dual_trans_amp.sch" 50
F2 "transamp_in_a" I L 4250 3000 50 
F3 "transamp_in_b" I L 4250 3200 50 
F4 "transamp_out_a" O R 5750 3000 50 
F5 "transamp_out_b" O R 5750 3200 50 
$EndSheet
$Sheet
S 4250 3650 1500 400 
U 5D170272
F0 "sheet5D170256" 50
F1 "dual_trans_amp.sch" 50
F2 "transamp_in_a" I L 4250 3750 50 
F3 "transamp_in_b" I L 4250 3950 50 
F4 "transamp_out_a" O R 5750 3750 50 
F5 "transamp_out_b" O R 5750 3950 50 
$EndSheet
$Sheet
S 4250 4400 1500 400 
U 5D170278
F0 "sheet5D170257" 50
F1 "dual_trans_amp.sch" 50
F2 "transamp_in_a" I L 4250 4500 50 
F3 "transamp_in_b" I L 4250 4700 50 
F4 "transamp_out_a" O R 5750 4500 50 
F5 "transamp_out_b" O R 5750 4700 50 
$EndSheet
$Sheet
S 4250 5150 1500 400 
U 5D17027E
F0 "sheet5D170258" 50
F1 "dual_trans_amp.sch" 50
F2 "transamp_in_a" I L 4250 5250 50 
F3 "transamp_in_b" I L 4250 5450 50 
F4 "transamp_out_a" O R 5750 5250 50 
F5 "transamp_out_b" O R 5750 5450 50 
$EndSheet
$Sheet
S 4250 5950 1500 400 
U 5D170284
F0 "sheet5D170259" 50
F1 "dual_trans_amp.sch" 50
F2 "transamp_in_a" I L 4250 6050 50 
F3 "transamp_in_b" I L 4250 6250 50 
F4 "transamp_out_a" O R 5750 6050 50 
F5 "transamp_out_b" O R 5750 6250 50 
$EndSheet
$Sheet
S 4250 6700 1500 400 
U 5D17028A
F0 "sheet5D17025A" 50
F1 "dual_trans_amp.sch" 50
F2 "transamp_in_a" I L 4250 6800 50 
F3 "transamp_in_b" I L 4250 7000 50 
F4 "transamp_out_a" O R 5750 6800 50 
F5 "transamp_out_b" O R 5750 7000 50 
$EndSheet
Text HLabel 1650 1400 0    50   Input ~ 0
N1
Text HLabel 1650 1600 0    50   Input ~ 0
N5
Text HLabel 1650 2150 0    50   Input ~ 0
N9
Text HLabel 1650 2350 0    50   Input ~ 0
N13
Text HLabel 1650 2950 0    50   Input ~ 0
N17
Text HLabel 1650 3150 0    50   Input ~ 0
N21
Text HLabel 1650 3700 0    50   Input ~ 0
N25
Text HLabel 1650 3900 0    50   Input ~ 0
N29
Text HLabel 1650 4450 0    50   Input ~ 0
N2
Text HLabel 1650 4650 0    50   Input ~ 0
N6
Text HLabel 1650 5200 0    50   Input ~ 0
N10
Text HLabel 1650 5400 0    50   Input ~ 0
N14
Text HLabel 1650 6000 0    50   Input ~ 0
N18
Text HLabel 1650 6200 0    50   Input ~ 0
N22
Text HLabel 4250 4500 0    50   Input ~ 0
N4
Text HLabel 4250 4700 0    50   Input ~ 0
N8
Text HLabel 4250 5250 0    50   Input ~ 0
N12
Text HLabel 4250 5450 0    50   Input ~ 0
N16
Text HLabel 4250 6050 0    50   Input ~ 0
N20
Text HLabel 4250 6250 0    50   Input ~ 0
N24
Text HLabel 4250 6800 0    50   Input ~ 0
N28
Text HLabel 4250 7000 0    50   Input ~ 0
N32
Text HLabel 4250 1450 0    50   Input ~ 0
N3
Text HLabel 4250 1650 0    50   Input ~ 0
N7
Text HLabel 4250 2250 0    50   Input ~ 0
N11
Text HLabel 4250 2450 0    50   Input ~ 0
N15
Text HLabel 4250 3000 0    50   Input ~ 0
N19
Text HLabel 4250 3200 0    50   Input ~ 0
N23
Text HLabel 4250 3750 0    50   Input ~ 0
N27
Text HLabel 4250 3950 0    50   Input ~ 0
N31
$Sheet
S 1650 6650 1500 400 
U 5D1BCC6F
F0 "sheet5D1BCC69" 50
F1 "dual_trans_amp.sch" 50
F2 "transamp_in_a" I L 1650 6750 50 
F3 "transamp_in_b" I L 1650 6950 50 
F4 "transamp_out_a" O R 3150 6750 50 
F5 "transamp_out_b" O R 3150 6950 50 
$EndSheet
Text HLabel 1650 6750 0    50   Input ~ 0
N26
Text HLabel 1650 6950 0    50   Input ~ 0
N30
Text HLabel 3150 1400 2    50   Output ~ 0
O1
Text HLabel 3150 1600 2    50   Output ~ 0
O5
Text HLabel 3150 2150 2    50   Output ~ 0
O9
Text HLabel 3150 2350 2    50   Output ~ 0
O13
Text HLabel 3150 2950 2    50   Output ~ 0
O17
Text HLabel 3150 3150 2    50   Output ~ 0
O21
Text HLabel 3150 3700 2    50   Output ~ 0
O25
Text HLabel 3150 3900 2    50   Output ~ 0
O29
Text HLabel 3150 4450 2    50   Output ~ 0
O2
Text HLabel 3150 4650 2    50   Output ~ 0
O6
Text HLabel 3150 5200 2    50   Output ~ 0
O10
Text HLabel 3150 5400 2    50   Output ~ 0
O14
Text HLabel 3150 6000 2    50   Output ~ 0
O18
Text HLabel 3150 6200 2    50   Output ~ 0
O22
Text HLabel 3150 6750 2    50   Output ~ 0
O26
Text HLabel 3150 6950 2    50   Output ~ 0
O30
Text HLabel 5750 1450 2    50   Output ~ 0
O3
Text HLabel 5750 1650 2    50   Output ~ 0
O7
Text HLabel 5750 2250 2    50   Output ~ 0
O11
Text HLabel 5750 2450 2    50   Output ~ 0
O15
Text HLabel 5750 3000 2    50   Output ~ 0
O19
Text HLabel 5750 3200 2    50   Output ~ 0
O23
Text HLabel 5750 3750 2    50   Output ~ 0
O27
Text HLabel 5750 3950 2    50   Output ~ 0
O31
Text HLabel 5750 4500 2    50   Output ~ 0
O4
Text HLabel 5750 4700 2    50   Output ~ 0
O8
Text HLabel 5750 5250 2    50   Output ~ 0
O12
Text HLabel 5750 5450 2    50   Output ~ 0
O16
Text HLabel 5750 6050 2    50   Output ~ 0
O20
Text HLabel 5750 6250 2    50   Output ~ 0
O24
Text HLabel 5750 6800 2    50   Output ~ 0
O28
Text HLabel 5750 7000 2    50   Output ~ 0
O32
$EndSCHEMATC
