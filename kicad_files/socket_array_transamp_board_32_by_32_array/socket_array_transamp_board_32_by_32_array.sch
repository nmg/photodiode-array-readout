EESchema Schematic File Version 4
LIBS:socket_transamp_board_32_by_32_array-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 18
Title "QFN-64 Socket Board for UV 32x32 Photodiode Array with transimpedance amplifier"
Date "2019-06-10"
Rev "Revision 1"
Comp "CoolCAD Electronics LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "Nevo Magnezi"
$EndDescr
$Comp
L SamacSys_Parts:264-5205-01 J5
U 1 1 5CE4410F
P 2000 950
F 0 "J5" H 2450 1215 50  0000 C CNN
F 1 "264-5205-01" H 2450 1124 50  0000 C CNN
F 2 "SamacSys_Parts:264-5205-01_1" H 2750 1050 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/264-5205-01.pdf" H 2750 950 50  0001 L CNN
F 4 "3M - 264-5205-01 - IC SOCKET, QFN, 64 POSITION, 0.5MM, THROUGH HOLE" H 2750 850 50  0001 L CNN "Description"
F 5 "" H 2750 750 50  0001 L CNN "Height"
F 6 "517-264-5205-01" H 2750 650 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=517-264-5205-01" H 2750 550 50  0001 L CNN "Mouser Price/Stock"
F 8 "3M" H 2750 450 50  0001 L CNN "Manufacturer_Name"
F 9 "264-5205-01" H 2750 350 50  0001 L CNN "Manufacturer_Part_Number"
	1    2000 950 
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J6
U 1 1 5CE4444F
P 3500 1350
F 0 "J6" H 3527 1326 50  0000 L CNN
F 1 "Conn_01x08_Female" H 3527 1235 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 3500 1350 50  0001 C CNN
F 3 "~" H 3500 1350 50  0001 C CNN
	1    3500 1350
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J7
U 1 1 5CE44504
P 3500 2150
F 0 "J7" H 3527 2126 50  0000 L CNN
F 1 "Conn_01x08_Female" H 3527 2035 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 3500 2150 50  0001 C CNN
F 3 "~" H 3500 2150 50  0001 C CNN
	1    3500 2150
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J8
U 1 1 5CE445CA
P 3500 2950
F 0 "J8" H 3527 2926 50  0000 L CNN
F 1 "Conn_01x08_Female" H 3527 2835 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 3500 2950 50  0001 C CNN
F 3 "~" H 3500 2950 50  0001 C CNN
	1    3500 2950
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J9
U 1 1 5CE445D1
P 3500 3750
F 0 "J9" H 3527 3726 50  0000 L CNN
F 1 "Conn_01x08_Female" H 3527 3635 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 3500 3750 50  0001 C CNN
F 3 "~" H 3500 3750 50  0001 C CNN
	1    3500 3750
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J1
U 1 1 5CE44772
P 1400 1250
F 0 "J1" H 1427 1226 50  0000 L CNN
F 1 "Conn_01x08_Female" H 1427 1135 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 1400 1250 50  0001 C CNN
F 3 "~" H 1400 1250 50  0001 C CNN
	1    1400 1250
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J2
U 1 1 5CE44779
P 1400 2050
F 0 "J2" H 1427 2026 50  0000 L CNN
F 1 "Conn_01x08_Female" H 1427 1935 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 1400 2050 50  0001 C CNN
F 3 "~" H 1400 2050 50  0001 C CNN
	1    1400 2050
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J3
U 1 1 5CE44780
P 1400 2850
F 0 "J3" H 1427 2826 50  0000 L CNN
F 1 "Conn_01x08_Female" H 1427 2735 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 1400 2850 50  0001 C CNN
F 3 "~" H 1400 2850 50  0001 C CNN
	1    1400 2850
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J4
U 1 1 5CE44787
P 1400 3650
F 0 "J4" H 1427 3626 50  0000 L CNN
F 1 "Conn_01x08_Female" H 1427 3535 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 1400 3650 50  0001 C CNN
F 3 "~" H 1400 3650 50  0001 C CNN
	1    1400 3650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2350 4300 2350 4400
Wire Wire Line
	2350 4400 2450 4400
Wire Wire Line
	2550 4400 2550 4300
Wire Wire Line
	2450 4300 2450 4400
Connection ~ 2450 4400
Wire Wire Line
	2450 4400 2550 4400
$Comp
L power:GND #PWR0101
U 1 1 5CED5B07
P 2450 4400
F 0 "#PWR0101" H 2450 4150 50  0001 C CNN
F 1 "GND" H 2455 4227 50  0000 C CNN
F 2 "" H 2450 4400 50  0001 C CNN
F 3 "" H 2450 4400 50  0001 C CNN
	1    2450 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 950  2000 950 
Text Label 1750 950  0    50   ~ 0
N2
Wire Wire Line
	1600 1050 2000 1050
Text Label 1750 1050 0    50   ~ 0
N6
Wire Wire Line
	1600 1150 2000 1150
Text Label 1750 1150 0    50   ~ 0
N10
Wire Wire Line
	1600 1250 2000 1250
Text Label 1750 1250 0    50   ~ 0
N14
Wire Wire Line
	1600 1350 2000 1350
Text Label 1750 1350 0    50   ~ 0
N18
Wire Wire Line
	1600 1450 2000 1450
Text Label 1750 1450 0    50   ~ 0
N22
Wire Wire Line
	1600 1550 2000 1550
Text Label 1750 1550 0    50   ~ 0
N26
Wire Wire Line
	1600 1650 2000 1650
Text Label 1750 1650 0    50   ~ 0
N30
Wire Wire Line
	1600 1750 2000 1750
Text Label 1750 1750 0    50   ~ 0
P1
Wire Wire Line
	1600 1850 2000 1850
Text Label 1750 1850 0    50   ~ 0
P5
Wire Wire Line
	1600 1950 2000 1950
Text Label 1750 1950 0    50   ~ 0
P9
Wire Wire Line
	1600 2050 2000 2050
Text Label 1750 2050 0    50   ~ 0
P13
Wire Wire Line
	1600 2150 2000 2150
Text Label 1750 2150 0    50   ~ 0
P17
Wire Wire Line
	1600 2250 2000 2250
Text Label 1750 2250 0    50   ~ 0
P21
Wire Wire Line
	1600 2350 2000 2350
Text Label 1750 2350 0    50   ~ 0
P25
Wire Wire Line
	1600 2450 2000 2450
Text Label 1750 2450 0    50   ~ 0
P29
Wire Wire Line
	1600 2550 2000 2550
Text Label 1750 2550 0    50   ~ 0
N31
Wire Wire Line
	1600 2650 2000 2650
Text Label 1750 2650 0    50   ~ 0
N27
Wire Wire Line
	1600 2750 2000 2750
Text Label 1750 2750 0    50   ~ 0
N23
Wire Wire Line
	1600 2850 2000 2850
Text Label 1750 2850 0    50   ~ 0
N19
Wire Wire Line
	1600 2950 2000 2950
Text Label 1750 2950 0    50   ~ 0
N15
Wire Wire Line
	1600 3050 2000 3050
Text Label 1750 3050 0    50   ~ 0
N11
Wire Wire Line
	1600 3150 2000 3150
Text Label 1750 3150 0    50   ~ 0
N7
Wire Wire Line
	1600 3250 2000 3250
Text Label 1750 3250 0    50   ~ 0
N3
Wire Wire Line
	1600 3350 2000 3350
Text Label 1750 3350 0    50   ~ 0
P30
Wire Wire Line
	1600 3450 2000 3450
Text Label 1750 3450 0    50   ~ 0
P26
Wire Wire Line
	1600 3550 2000 3550
Text Label 1750 3550 0    50   ~ 0
P22
Wire Wire Line
	1600 3650 2000 3650
Text Label 1750 3650 0    50   ~ 0
P18
Wire Wire Line
	1600 3750 2000 3750
Text Label 1750 3750 0    50   ~ 0
P14
Wire Wire Line
	1600 3850 2000 3850
Text Label 1750 3850 0    50   ~ 0
P10
Wire Wire Line
	1600 3950 2000 3950
Text Label 1750 3950 0    50   ~ 0
P6
Wire Wire Line
	1600 4050 2000 4050
Text Label 1750 4050 0    50   ~ 0
P2
Wire Wire Line
	2900 950  3300 950 
Text Label 3050 950  0    50   ~ 0
N4
Wire Wire Line
	2900 1050 3300 1050
Text Label 3050 1050 0    50   ~ 0
N8
Wire Wire Line
	2900 1150 3300 1150
Text Label 3050 1150 0    50   ~ 0
N12
Wire Wire Line
	2900 1250 3300 1250
Text Label 3050 1250 0    50   ~ 0
N16
Wire Wire Line
	2900 1350 3300 1350
Text Label 3050 1350 0    50   ~ 0
N20
Wire Wire Line
	2900 1450 3300 1450
Text Label 3050 1450 0    50   ~ 0
N24
Wire Wire Line
	2900 1550 3300 1550
Text Label 3050 1550 0    50   ~ 0
N28
Wire Wire Line
	2900 1650 3300 1650
Text Label 3050 1650 0    50   ~ 0
N32
Wire Wire Line
	2900 1750 3300 1750
Text Label 3050 1750 0    50   ~ 0
P3
Wire Wire Line
	2900 1850 3300 1850
Text Label 3050 1850 0    50   ~ 0
P7
Wire Wire Line
	2900 1950 3300 1950
Text Label 3050 1950 0    50   ~ 0
P11
Wire Wire Line
	2900 2050 3300 2050
Text Label 3050 2050 0    50   ~ 0
P15
Wire Wire Line
	2900 2150 3300 2150
Text Label 3050 2150 0    50   ~ 0
P19
Wire Wire Line
	2900 2250 3300 2250
Text Label 3050 2250 0    50   ~ 0
P23
Wire Wire Line
	2900 2350 3300 2350
Text Label 3050 2350 0    50   ~ 0
P27
Wire Wire Line
	2900 2450 3300 2450
Text Label 3050 2450 0    50   ~ 0
P31
Wire Wire Line
	2900 2550 3300 2550
Text Label 3050 2550 0    50   ~ 0
N29
Wire Wire Line
	2900 2650 3300 2650
Text Label 3050 2650 0    50   ~ 0
N25
Wire Wire Line
	2900 2750 3300 2750
Text Label 3050 2750 0    50   ~ 0
N21
Wire Wire Line
	2900 2850 3300 2850
Text Label 3050 2850 0    50   ~ 0
N17
Wire Wire Line
	2900 2950 3300 2950
Text Label 3050 2950 0    50   ~ 0
N13
Wire Wire Line
	2900 3050 3300 3050
Text Label 3050 3050 0    50   ~ 0
N9
Wire Wire Line
	2900 3150 3300 3150
Text Label 3050 3150 0    50   ~ 0
N5
Wire Wire Line
	2900 3250 3300 3250
Text Label 3050 3250 0    50   ~ 0
N1
Wire Wire Line
	2900 3350 3300 3350
Text Label 3050 3350 0    50   ~ 0
P32
Wire Wire Line
	2900 3450 3300 3450
Text Label 3050 3450 0    50   ~ 0
P28
Wire Wire Line
	2900 3550 3300 3550
Text Label 3050 3550 0    50   ~ 0
P24
Wire Wire Line
	2900 3650 3300 3650
Text Label 3050 3650 0    50   ~ 0
P20
Wire Wire Line
	2900 3750 3300 3750
Text Label 3050 3750 0    50   ~ 0
P16
Wire Wire Line
	2900 3850 3300 3850
Text Label 3050 3850 0    50   ~ 0
P12
Wire Wire Line
	2900 3950 3300 3950
Text Label 3050 3950 0    50   ~ 0
P8
Wire Wire Line
	2900 4050 3300 4050
Text Label 3050 4050 0    50   ~ 0
P4
$Comp
L 2019-06-10_17-42-28:1-1571983-1 SW1
U 1 1 5CFF1D8A
P 5100 950
F 0 "SW1" H 5900 1337 60  0000 C CNN
F 1 "1-1571983-1" H 5900 1231 60  0000 C CNN
F 2 "footprints:1-1571983-1" H 5900 1190 60  0001 C CNN
F 3 "" H 5100 950 60  0000 C CNN
	1    5100 950 
	1    0    0    -1  
$EndComp
$Comp
L 2019-06-10_17-42-28:1-1571983-1 SW2
U 1 1 5CFF1F3B
P 6700 2350
F 0 "SW2" H 7500 2737 60  0000 C CNN
F 1 "1-1571983-1" H 7500 2631 60  0000 C CNN
F 2 "footprints:1-1571983-1" H 7500 2590 60  0001 C CNN
F 3 "" H 6700 2350 60  0000 C CNN
	1    6700 2350
	-1   0    0    -1  
$EndComp
$Comp
L 2019-06-10_17-42-28:1-1571983-1 SW3
U 1 1 5CFF3AB4
P 5100 3750
F 0 "SW3" H 5900 4137 60  0000 C CNN
F 1 "1-1571983-1" H 5900 4031 60  0000 C CNN
F 2 "footprints:1-1571983-1" H 5900 3990 60  0001 C CNN
F 3 "" H 5100 3750 60  0000 C CNN
	1    5100 3750
	1    0    0    -1  
$EndComp
$Comp
L 2019-06-10_17-42-28:1-1571983-1 SW4
U 1 1 5CFF3ABB
P 6700 5150
F 0 "SW4" H 7500 5537 60  0000 C CNN
F 1 "1-1571983-1" H 7500 5431 60  0000 C CNN
F 2 "footprints:1-1571983-1" H 7500 5390 60  0001 C CNN
F 3 "" H 6700 5150 60  0000 C CNN
	1    6700 5150
	-1   0    0    -1  
$EndComp
$Comp
L 2019-06-10_17-42-28:1-1571983-1 SW5
U 1 1 5CFF5650
P 8850 1000
F 0 "SW5" H 9650 1387 60  0000 C CNN
F 1 "1-1571983-1" H 9650 1281 60  0000 C CNN
F 2 "footprints:1-1571983-1" H 9650 1240 60  0001 C CNN
F 3 "" H 8850 1000 60  0000 C CNN
	1    8850 1000
	-1   0    0    -1  
$EndComp
$Comp
L 2019-06-10_17-42-28:1-1571983-1 SW6
U 1 1 5CFF5657
P 7250 2400
F 0 "SW6" H 8050 2787 60  0000 C CNN
F 1 "1-1571983-1" H 8050 2681 60  0000 C CNN
F 2 "footprints:1-1571983-1" H 8050 2640 60  0001 C CNN
F 3 "" H 7250 2400 60  0000 C CNN
	1    7250 2400
	1    0    0    -1  
$EndComp
$Comp
L 2019-06-10_17-42-28:1-1571983-1 SW7
U 1 1 5CFF565E
P 8850 3800
F 0 "SW7" H 9650 4187 60  0000 C CNN
F 1 "1-1571983-1" H 9650 4081 60  0000 C CNN
F 2 "footprints:1-1571983-1" H 9650 4040 60  0001 C CNN
F 3 "" H 8850 3800 60  0000 C CNN
	1    8850 3800
	-1   0    0    -1  
$EndComp
$Comp
L 2019-06-10_17-42-28:1-1571983-1 SW8
U 1 1 5CFF5665
P 7250 5200
F 0 "SW8" H 8050 5587 60  0000 C CNN
F 1 "1-1571983-1" H 8050 5481 60  0000 C CNN
F 2 "footprints:1-1571983-1" H 8050 5440 60  0001 C CNN
F 3 "" H 7250 5200 60  0000 C CNN
	1    7250 5200
	1    0    0    -1  
$EndComp
Text Label 5100 5150 2    50   ~ 0
N4
Text Label 5100 5250 2    50   ~ 0
N8
Text Label 5100 5350 2    50   ~ 0
N12
Text Label 5100 5450 2    50   ~ 0
N16
Text Label 5100 5550 2    50   ~ 0
N20
Text Label 5100 5650 2    50   ~ 0
N24
Text Label 5100 5750 2    50   ~ 0
N28
Text Label 5100 5850 2    50   ~ 0
N32
Text Label 7250 3800 2    50   ~ 0
P3
Text Label 7250 3900 2    50   ~ 0
P7
Text Label 7250 4000 2    50   ~ 0
P11
Text Label 7250 4100 2    50   ~ 0
P15
Text Label 7250 4200 2    50   ~ 0
P19
Text Label 7250 4300 2    50   ~ 0
P23
Text Label 7250 4400 2    50   ~ 0
P27
Text Label 7250 4500 2    50   ~ 0
P31
Text Label 5100 1650 2    50   ~ 0
N29
Text Label 5100 1550 2    50   ~ 0
N25
Text Label 5100 1450 2    50   ~ 0
N21
Text Label 5100 1350 2    50   ~ 0
N17
Text Label 5100 1250 2    50   ~ 0
N13
Text Label 5100 1150 2    50   ~ 0
N9
Text Label 5100 1050 2    50   ~ 0
N5
Text Label 5100 950  2    50   ~ 0
N1
Text Label 7250 5900 2    50   ~ 0
P32
Text Label 7250 5800 2    50   ~ 0
P28
Text Label 7250 5700 2    50   ~ 0
P24
Text Label 7250 5600 2    50   ~ 0
P20
Text Label 7250 5500 2    50   ~ 0
P16
Text Label 7250 5400 2    50   ~ 0
P12
Text Label 7250 5300 2    50   ~ 0
P8
Text Label 7250 5200 2    50   ~ 0
P4
Text Label 5100 2350 2    50   ~ 0
N2
Text Label 5100 2450 2    50   ~ 0
N6
Text Label 5100 2550 2    50   ~ 0
N10
Text Label 5100 2650 2    50   ~ 0
N14
Text Label 5100 2750 2    50   ~ 0
N18
Text Label 5100 2850 2    50   ~ 0
N22
Text Label 5100 2950 2    50   ~ 0
N26
Text Label 5100 3050 2    50   ~ 0
N30
Text Label 7250 1000 2    50   ~ 0
P1
Text Label 7250 1100 2    50   ~ 0
P5
Text Label 7250 1200 2    50   ~ 0
P9
Text Label 7250 1300 2    50   ~ 0
P13
Text Label 7250 1400 2    50   ~ 0
P17
Text Label 7250 1500 2    50   ~ 0
P21
Text Label 7250 1600 2    50   ~ 0
P25
Text Label 7250 1700 2    50   ~ 0
P29
Text Label 5100 4450 2    50   ~ 0
N31
Text Label 5100 4350 2    50   ~ 0
N27
Text Label 5100 4250 2    50   ~ 0
N23
Text Label 5100 4150 2    50   ~ 0
N19
Text Label 5100 4050 2    50   ~ 0
N15
Text Label 5100 3950 2    50   ~ 0
N11
Text Label 5100 3850 2    50   ~ 0
N7
Text Label 5100 3750 2    50   ~ 0
N3
Text Label 7250 3100 2    50   ~ 0
P30
Text Label 7250 3000 2    50   ~ 0
P26
Text Label 7250 2900 2    50   ~ 0
P22
Text Label 7250 2800 2    50   ~ 0
P18
Text Label 7250 2700 2    50   ~ 0
P14
Text Label 7250 2600 2    50   ~ 0
P10
Text Label 7250 2500 2    50   ~ 0
P6
Text Label 7250 2400 2    50   ~ 0
P2
Text Label 6900 3500 0    79   ~ 0
ARRAY_N_SIDE
Wire Wire Line
	6900 3500 6700 3500
Connection ~ 6700 3500
Wire Wire Line
	9050 3500 8850 3500
Connection ~ 8850 3500
$Comp
L 2019-06-10_18-49-03:LMP7721MA_NOPB U1
U 1 1 5D08B50D
P 3600 6750
F 0 "U1" H 3600 7050 60  0000 C CNN
F 1 "LMP7721MA_NOPB" H 3700 7150 60  0000 C CNN
F 2 "footprints:LMP7721MA&slash_NOPB" H 3550 6540 60  0001 C CNN
F 3 "" H 3600 6750 60  0000 C CNN
	1    3600 6750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5D09901C
P 1800 7350
F 0 "#PWR0102" H 1800 7100 50  0001 C CNN
F 1 "GND" H 1805 7177 50  0000 C CNN
F 2 "" H 1800 7350 50  0001 C CNN
F 3 "" H 1800 7350 50  0001 C CNN
	1    1800 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 7050 3050 7050
Wire Wire Line
	2800 7050 2800 7450
$Comp
L Device:R R1
U 1 1 5D09CFF8
P 2800 7600
F 0 "R1" V 2870 7646 50  0000 L CNN
F 1 "R" H 2870 7555 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2730 7600 50  0001 C CNN
F 3 "~" H 2800 7600 50  0001 C CNN
	1    2800 7600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5D09D0A5
P 2450 7600
F 0 "C1" H 2565 7646 50  0000 L CNN
F 1 "C" H 2565 7555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2488 7450 50  0001 C CNN
F 3 "~" H 2450 7600 50  0001 C CNN
	1    2450 7600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J10
U 1 1 5D09D2AE
P 1250 7200
F 0 "J10" H 1277 7176 50  0000 L CNN
F 1 "Conn_01x08_Female" H 1277 7085 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 1250 7200 50  0001 C CNN
F 3 "~" H 1250 7200 50  0001 C CNN
	1    1250 7200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4200 6950 4550 6950
Wire Wire Line
	3400 6850 2750 6850
$Comp
L power:GND #PWR0104
U 1 1 5D0D22F3
P 3600 6100
F 0 "#PWR0104" H 3600 5850 50  0001 C CNN
F 1 "GND" H 3800 6000 50  0000 C CNN
F 2 "" H 3600 6100 50  0001 C CNN
F 3 "" H 3600 6100 50  0001 C CNN
	1    3600 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5D0DAA9A
P 4150 5800
F 0 "R8" V 3943 5800 50  0000 C CNN
F 1 "R" V 4034 5800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4080 5800 50  0001 C CNN
F 3 "~" H 4150 5800 50  0001 C CNN
	1    4150 5800
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5D0DAAA2
P 3700 5950
F 0 "R6" H 3630 5904 50  0000 R CNN
F 1 "R" H 3630 5995 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3630 5950 50  0001 C CNN
F 3 "~" H 3700 5950 50  0001 C CNN
	1    3700 5950
	-1   0    0    1   
$EndComp
Connection ~ 3700 5800
$Comp
L Device:C C2
U 1 1 5D0F043E
P 3200 5150
F 0 "C2" V 2948 5150 50  0000 C CNN
F 1 "C" V 3039 5150 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3238 5000 50  0001 C CNN
F 3 "~" H 3200 5150 50  0001 C CNN
	1    3200 5150
	0    1    1    0   
$EndComp
Connection ~ 2750 5800
Wire Wire Line
	2750 5800 2750 6850
Wire Wire Line
	2450 7750 2450 7950
Wire Wire Line
	2450 7950 2800 7950
Wire Wire Line
	2800 7950 2800 7750
Wire Wire Line
	2450 7450 2450 7050
Wire Wire Line
	2450 7050 2800 7050
Connection ~ 2800 7050
Wire Wire Line
	2450 7950 2050 7950
Wire Wire Line
	2050 7950 2050 7600
Wire Wire Line
	2050 7600 1450 7600
Connection ~ 2450 7950
Wire Wire Line
	2750 5800 2000 5800
Text Label 2000 5800 2    79   ~ 0
ARRAY_N_SIDE
$Comp
L power:GND #PWR0105
U 1 1 5D14019C
P 9050 3500
F 0 "#PWR0105" H 9050 3250 50  0001 C CNN
F 1 "GND" H 9055 3327 50  0000 C CNN
F 2 "" H 9050 3500 50  0001 C CNN
F 3 "" H 9050 3500 50  0001 C CNN
	1    9050 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 7400 1600 7400
Wire Wire Line
	1600 7400 1600 7350
Wire Wire Line
	1600 7350 1800 7350
Wire Wire Line
	1800 7200 1450 7200
Wire Wire Line
	1450 7600 1450 7500
Connection ~ 1450 7600
Wire Wire Line
	1450 7400 1450 7300
Connection ~ 1450 7400
Wire Wire Line
	1450 7100 1450 7200
Connection ~ 1450 7200
Text Label 1450 6900 0    79   ~ 0
ARRAY_N_SIDE
$Comp
L Device:R R2
U 1 1 5D18D538
P 3050 7600
F 0 "R2" H 3120 7646 50  0000 L CNN
F 1 "R" H 3120 7555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2980 7600 50  0001 C CNN
F 3 "~" H 3050 7600 50  0001 C CNN
	1    3050 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 7450 3050 7050
Connection ~ 3050 7050
Wire Wire Line
	3050 7050 2800 7050
Wire Wire Line
	3050 7750 3050 7950
Wire Wire Line
	3050 7950 2800 7950
Connection ~ 2800 7950
$Comp
L Device:R R7
U 1 1 5D1A6EE0
P 4150 5500
F 0 "R7" V 3943 5500 50  0000 C CNN
F 1 "R" V 4034 5500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4080 5500 50  0001 C CNN
F 3 "~" H 4150 5500 50  0001 C CNN
	1    4150 5500
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 5500 4550 5500
Wire Wire Line
	4550 5500 4550 5800
Wire Wire Line
	4300 5800 4550 5800
Connection ~ 4550 5800
Wire Wire Line
	4550 5800 4550 6950
Wire Wire Line
	4000 5800 3900 5800
$Comp
L Device:R R4
U 1 1 5D1F0C20
P 3200 5800
F 0 "R4" V 2993 5800 50  0000 C CNN
F 1 "R" V 3084 5800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3130 5800 50  0001 C CNN
F 3 "~" H 3200 5800 50  0001 C CNN
	1    3200 5800
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5D1F0C27
P 3200 5500
F 0 "R3" V 2993 5500 50  0000 C CNN
F 1 "R" V 3084 5500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3130 5500 50  0001 C CNN
F 3 "~" H 3200 5500 50  0001 C CNN
	1    3200 5500
	0    1    1    0   
$EndComp
Wire Wire Line
	3050 5800 2750 5800
Wire Wire Line
	3050 5500 2750 5500
Wire Wire Line
	3350 5800 3400 5800
Wire Wire Line
	2750 5150 3050 5150
Wire Wire Line
	3350 5150 3400 5150
$Comp
L Device:R R5
U 1 1 5D22F7CF
P 3500 5950
F 0 "R5" H 3430 5904 50  0000 R CNN
F 1 "R" H 3430 5995 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3430 5950 50  0001 C CNN
F 3 "~" H 3500 5950 50  0001 C CNN
	1    3500 5950
	-1   0    0    1   
$EndComp
Connection ~ 3500 5800
Wire Wire Line
	3500 5800 3700 5800
Wire Wire Line
	3500 6100 3600 6100
Connection ~ 3600 6100
Wire Wire Line
	3600 6100 3700 6100
Wire Wire Line
	3400 5800 3400 5500
Connection ~ 3400 5800
Wire Wire Line
	3400 5800 3500 5800
Wire Wire Line
	4000 5500 3900 5500
Wire Wire Line
	3900 5500 3900 5800
Connection ~ 3900 5800
Wire Wire Line
	3900 5800 3700 5800
Wire Wire Line
	3400 5500 3350 5500
Connection ~ 3400 5500
Wire Wire Line
	3400 5500 3400 5150
Text Label 1450 7000 0    47   ~ 0
V_OUT
Text Label 4550 6050 0    47   ~ 0
V_OUT
$Comp
L power:GND #PWR0107
U 1 1 5D280C27
P 3800 7350
F 0 "#PWR0107" H 3800 7100 50  0001 C CNN
F 1 "GND" H 4000 7250 50  0000 C CNN
F 2 "" H 3800 7350 50  0001 C CNN
F 3 "" H 3800 7350 50  0001 C CNN
	1    3800 7350
	1    0    0    -1  
$EndComp
NoConn ~ 3400 6650
NoConn ~ 4200 7150
NoConn ~ 3400 7250
Text Label 2050 7500 0    59   ~ 0
V_BIAS
Wire Wire Line
	2050 7600 2050 7500
Connection ~ 2050 7600
Wire Wire Line
	2750 5150 2750 5500
Connection ~ 2750 5500
Wire Wire Line
	2750 5500 2750 5800
Wire Wire Line
	8850 3500 8850 3800
Wire Wire Line
	6700 3500 6700 3750
Wire Wire Line
	6700 950  6700 1050
Wire Wire Line
	8850 1000 8850 1100
$Comp
L power:+3.3V #PWR0103
U 1 1 5D13770C
P 3800 6550
F 0 "#PWR0103" H 3800 6400 50  0001 C CNN
F 1 "+3.3V" H 3815 6723 50  0000 C CNN
F 2 "" H 3800 6550 50  0001 C CNN
F 3 "" H 3800 6550 50  0001 C CNN
	1    3800 6550
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0106
U 1 1 5D13BD92
P 1800 7200
F 0 "#PWR0106" H 1800 7050 50  0001 C CNN
F 1 "+3.3V" H 1815 7373 50  0000 C CNN
F 2 "" H 1800 7200 50  0001 C CNN
F 3 "" H 1800 7200 50  0001 C CNN
	1    1800 7200
	1    0    0    -1  
$EndComp
$Sheet
S 9550 800  550  3200
U 5D16BC1F
F0 "Transimpedance Amplifier Array" 50
F1 "transamp_array.sch" 50
F2 "N1" I L 9550 850 50 
F3 "N2" I L 9550 1650 50 
F4 "N3" I L 9550 2450 50 
F5 "N4" I L 9550 3250 50 
F6 "N5" I L 9550 950 50 
F7 "N6" I L 9550 1750 50 
F8 "N7" I L 9550 2550 50 
F9 "N8" I L 9550 3350 50 
F10 "N9" I L 9550 1050 50 
F11 "N10" I L 9550 1850 50 
F12 "N11" I L 9550 2650 50 
F13 "N12" I L 9550 3450 50 
F14 "N13" I L 9550 1150 50 
F15 "N14" I L 9550 1950 50 
F16 "N25" I L 9550 1450 50 
F17 "N26" I L 9550 2250 50 
F18 "N27" I L 9550 3050 50 
F19 "N28" I L 9550 3850 50 
F20 "N29" I L 9550 1550 50 
F21 "N30" I L 9550 2350 50 
F22 "N31" I L 9550 3150 50 
F23 "N32" I L 9550 3950 50 
F24 "N17" I L 9550 1250 50 
F25 "N18" I L 9550 2050 50 
F26 "N19" I L 9550 2850 50 
F27 "N20" I L 9550 3650 50 
F28 "N21" I L 9550 1350 50 
F29 "N22" I L 9550 2150 50 
F30 "N23" I L 9550 2950 50 
F31 "N24" I L 9550 3750 50 
F32 "N15" I L 9550 2750 50 
F33 "N16" I L 9550 3550 50 
F34 "O1" O R 10100 850 50 
F35 "O2" O R 10100 950 50 
F36 "O3" O R 10100 1050 50 
F37 "O4" O R 10100 1150 50 
F38 "O5" O R 10100 1250 50 
F39 "O6" O R 10100 1350 50 
F40 "O7" O R 10100 1450 50 
F41 "O8" O R 10100 1550 50 
F42 "O9" O R 10100 1650 50 
F43 "O10" O R 10100 1750 50 
F44 "O11" O R 10100 1850 50 
F45 "O12" O R 10100 1950 50 
F46 "O13" O R 10100 2050 50 
F47 "O14" O R 10100 2150 50 
F48 "O15" O R 10100 2250 50 
F49 "O16" O R 10100 2350 50 
F50 "O17" O R 10100 2450 50 
F51 "O18" O R 10100 2550 50 
F52 "O19" O R 10100 2650 50 
F53 "O20" O R 10100 2750 50 
F54 "O21" O R 10100 2850 50 
F55 "O22" O R 10100 2950 50 
F56 "O23" O R 10100 3050 50 
F57 "O24" O R 10100 3150 50 
F58 "O25" O R 10100 3250 50 
F59 "O26" O R 10100 3350 50 
F60 "O27" O R 10100 3450 50 
F61 "O28" O R 10100 3550 50 
F62 "O29" O R 10100 3650 50 
F63 "O30" O R 10100 3750 50 
F64 "O31" O R 10100 3850 50 
F65 "O32" O R 10100 3950 50 
$EndSheet
Text Label 9550 1650 2    50   ~ 0
N2
Text Label 9550 1750 2    50   ~ 0
N6
Text Label 9550 1850 2    50   ~ 0
N10
Text Label 9550 1950 2    50   ~ 0
N14
Text Label 9550 2050 2    50   ~ 0
N18
Text Label 9550 2150 2    50   ~ 0
N22
Text Label 9550 2250 2    50   ~ 0
N26
Text Label 9550 2350 2    50   ~ 0
N30
Text Label 9550 1550 2    50   ~ 0
N29
Text Label 9550 1450 2    50   ~ 0
N25
Text Label 9550 1350 2    50   ~ 0
N21
Text Label 9550 1250 2    50   ~ 0
N17
Text Label 9550 1150 2    50   ~ 0
N13
Text Label 9550 1050 2    50   ~ 0
N9
Text Label 9550 950  2    50   ~ 0
N5
Text Label 9550 850  2    50   ~ 0
N1
Text Label 9550 3150 2    50   ~ 0
N31
Text Label 9550 3050 2    50   ~ 0
N27
Text Label 9550 2950 2    50   ~ 0
N23
Text Label 9550 2850 2    50   ~ 0
N19
Text Label 9550 2750 2    50   ~ 0
N15
Text Label 9550 2650 2    50   ~ 0
N11
Text Label 9550 2550 2    50   ~ 0
N7
Text Label 9550 2450 2    50   ~ 0
N3
Text Label 9550 3250 2    50   ~ 0
N4
Text Label 9550 3350 2    50   ~ 0
N8
Text Label 9550 3450 2    50   ~ 0
N12
Text Label 9550 3550 2    50   ~ 0
N16
Text Label 9550 3650 2    50   ~ 0
N20
Text Label 9550 3750 2    50   ~ 0
N24
Text Label 9550 3850 2    50   ~ 0
N28
Text Label 9550 3950 2    50   ~ 0
N32
$Comp
L Connector:Conn_01x08_Female J11
U 1 1 5D1F8322
P 10300 1250
F 0 "J11" H 10327 1226 50  0000 L CNN
F 1 "Conn_01x08_Female" H 10327 1135 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 10300 1250 50  0001 C CNN
F 3 "~" H 10300 1250 50  0001 C CNN
	1    10300 1250
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J12
U 1 1 5D1F8329
P 10300 2050
F 0 "J12" H 10327 2026 50  0000 L CNN
F 1 "Conn_01x08_Female" H 10327 1935 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 10300 2050 50  0001 C CNN
F 3 "~" H 10300 2050 50  0001 C CNN
	1    10300 2050
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J13
U 1 1 5D1F8330
P 10300 2850
F 0 "J13" H 10327 2826 50  0000 L CNN
F 1 "Conn_01x08_Female" H 10327 2735 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 10300 2850 50  0001 C CNN
F 3 "~" H 10300 2850 50  0001 C CNN
	1    10300 2850
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J14
U 1 1 5D1F8337
P 10300 3650
F 0 "J14" H 10327 3626 50  0000 L CNN
F 1 "Conn_01x08_Female" H 10327 3535 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 10300 3650 50  0001 C CNN
F 3 "~" H 10300 3650 50  0001 C CNN
	1    10300 3650
	1    0    0    1   
$EndComp
Connection ~ 6700 1050
Wire Wire Line
	6700 1050 6700 1150
Connection ~ 6700 1150
Wire Wire Line
	6700 1150 6700 1250
Connection ~ 6700 1250
Wire Wire Line
	6700 1250 6700 1350
Connection ~ 6700 1350
Wire Wire Line
	6700 1350 6700 1450
Connection ~ 6700 1450
Wire Wire Line
	6700 1450 6700 1550
Connection ~ 6700 1550
Wire Wire Line
	6700 1550 6700 1650
Connection ~ 6700 1650
Wire Wire Line
	6700 1650 6700 2350
Connection ~ 6700 2350
Wire Wire Line
	6700 2350 6700 2450
Connection ~ 6700 2450
Wire Wire Line
	6700 2450 6700 2550
Connection ~ 6700 2550
Wire Wire Line
	6700 2550 6700 2650
Connection ~ 6700 2650
Wire Wire Line
	6700 2650 6700 2750
Connection ~ 6700 2750
Wire Wire Line
	6700 2750 6700 2850
Connection ~ 6700 2850
Wire Wire Line
	6700 2850 6700 2950
Connection ~ 6700 2950
Wire Wire Line
	6700 2950 6700 3050
Connection ~ 6700 3050
Wire Wire Line
	6700 3050 6700 3500
Connection ~ 6700 3750
Wire Wire Line
	6700 3750 6700 3850
Connection ~ 6700 3850
Wire Wire Line
	6700 3850 6700 3950
Connection ~ 6700 3950
Wire Wire Line
	6700 3950 6700 4050
Connection ~ 6700 4050
Wire Wire Line
	6700 4050 6700 4150
Connection ~ 6700 4150
Wire Wire Line
	6700 4150 6700 4250
Connection ~ 6700 4250
Wire Wire Line
	6700 4250 6700 4350
Connection ~ 6700 4350
Wire Wire Line
	6700 4350 6700 4450
Connection ~ 6700 4450
Wire Wire Line
	6700 4450 6700 5150
Connection ~ 6700 5150
Wire Wire Line
	6700 5150 6700 5250
Connection ~ 6700 5250
Wire Wire Line
	6700 5250 6700 5350
Connection ~ 6700 5350
Wire Wire Line
	6700 5350 6700 5450
Connection ~ 6700 5450
Wire Wire Line
	6700 5450 6700 5550
Connection ~ 6700 5550
Wire Wire Line
	6700 5550 6700 5650
Connection ~ 6700 5650
Wire Wire Line
	6700 5650 6700 5750
Connection ~ 6700 5750
Wire Wire Line
	6700 5750 6700 5850
Connection ~ 8850 1100
Wire Wire Line
	8850 1100 8850 1200
Connection ~ 8850 1200
Wire Wire Line
	8850 1200 8850 1300
Connection ~ 8850 1300
Wire Wire Line
	8850 1300 8850 1400
Connection ~ 8850 1400
Wire Wire Line
	8850 1400 8850 1500
Connection ~ 8850 1500
Wire Wire Line
	8850 1500 8850 1600
Connection ~ 8850 1600
Wire Wire Line
	8850 1600 8850 1700
Connection ~ 8850 1700
Wire Wire Line
	8850 1700 8850 2400
Connection ~ 8850 2400
Wire Wire Line
	8850 2400 8850 2500
Connection ~ 8850 2500
Wire Wire Line
	8850 2500 8850 2600
Connection ~ 8850 2600
Wire Wire Line
	8850 2600 8850 2700
Connection ~ 8850 2700
Wire Wire Line
	8850 2700 8850 2800
Connection ~ 8850 2800
Wire Wire Line
	8850 2800 8850 2900
Connection ~ 8850 2900
Wire Wire Line
	8850 2900 8850 3000
Connection ~ 8850 3000
Wire Wire Line
	8850 3000 8850 3100
Connection ~ 8850 3100
Connection ~ 8850 3800
Wire Wire Line
	8850 3800 8850 3900
Connection ~ 8850 3900
Wire Wire Line
	8850 3900 8850 4000
Connection ~ 8850 4000
Wire Wire Line
	8850 4000 8850 4100
Connection ~ 8850 4100
Wire Wire Line
	8850 4100 8850 4200
Connection ~ 8850 4200
Wire Wire Line
	8850 4200 8850 4300
Connection ~ 8850 4300
Wire Wire Line
	8850 4300 8850 4400
Connection ~ 8850 4400
Wire Wire Line
	8850 4400 8850 4500
Connection ~ 8850 4500
Wire Wire Line
	8850 4500 8850 5200
Connection ~ 8850 5200
Wire Wire Line
	8850 5200 8850 5300
Connection ~ 8850 5300
Wire Wire Line
	8850 5300 8850 5400
Connection ~ 8850 5400
Wire Wire Line
	8850 5400 8850 5500
Connection ~ 8850 5500
Wire Wire Line
	8850 5500 8850 5600
Connection ~ 8850 5600
Wire Wire Line
	8850 5600 8850 5700
Connection ~ 8850 5700
Wire Wire Line
	8850 5700 8850 5800
Connection ~ 8850 5800
Wire Wire Line
	8850 5800 8850 5900
Wire Wire Line
	8850 3150 8850 3500
Wire Wire Line
	8850 3100 8850 3500
$EndSCHEMATC
