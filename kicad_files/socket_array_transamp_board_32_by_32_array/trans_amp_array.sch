EESchema Schematic File Version 4
LIBS:socket_transamp_board_32_by_32_array-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 2019-06-26_20-49-32:LMP2234BMTX_NOPB U?
U 1 1 5D13E356
P 7650 2650
F 0 "U?" H 8450 3137 60  0000 C CNN
F 1 "LMP2234BMTX_NOPB" H 8450 3031 60  0000 C CNN
F 2 "PW14" H 8450 2990 60  0001 C CNN
F 3 "" H 7650 2650 60  0000 C CNN
	1    7650 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 2950 7500 2950
Wire Wire Line
	7500 2950 7500 3050
Wire Wire Line
	7500 3050 7650 3050
Wire Wire Line
	7650 2650 7650 2050
Wire Wire Line
	7650 2050 9400 2050
Wire Wire Line
	9400 2550 9250 2550
Wire Wire Line
	7650 3300 7500 3300
Wire Wire Line
	7500 3300 7500 3400
Wire Wire Line
	7500 3400 7650 3400
Wire Wire Line
	7650 2750 7450 2750
Wire Wire Line
	7450 2750 7450 1550
$Comp
L power:GND #PWR?
U 1 1 5D140EC2
P 8650 1850
F 0 "#PWR?" H 8650 1600 50  0001 C CNN
F 1 "GND" H 8850 1750 50  0000 C CNN
F 2 "" H 8650 1850 50  0001 C CNN
F 3 "" H 8650 1850 50  0001 C CNN
	1    8650 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D140EC8
P 9300 1550
F 0 "R?" V 9093 1550 50  0000 C CNN
F 1 "R" V 9184 1550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9230 1550 50  0001 C CNN
F 3 "~" H 9300 1550 50  0001 C CNN
	1    9300 1550
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5D140ECF
P 8200 1200
F 0 "C?" V 7948 1200 50  0000 C CNN
F 1 "C" V 8039 1200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8238 1050 50  0001 C CNN
F 3 "~" H 8200 1200 50  0001 C CNN
	1    8200 1200
	0    1    1    0   
$EndComp
Wire Wire Line
	9450 1550 9600 1550
$Comp
L Device:R R?
U 1 1 5D140ED9
P 8350 1550
F 0 "R?" V 8143 1550 50  0000 C CNN
F 1 "R" V 8234 1550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 8280 1550 50  0001 C CNN
F 3 "~" H 8350 1550 50  0001 C CNN
	1    8350 1550
	0    1    1    0   
$EndComp
Wire Wire Line
	8200 1550 7900 1550
$Comp
L Device:R R?
U 1 1 5D140EE1
P 8650 1700
F 0 "R?" H 8580 1654 50  0000 R CNN
F 1 "R" H 8580 1745 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8580 1700 50  0001 C CNN
F 3 "~" H 8650 1700 50  0001 C CNN
	1    8650 1700
	-1   0    0    1   
$EndComp
Connection ~ 8650 1550
Wire Wire Line
	8500 1550 8650 1550
Wire Wire Line
	7900 1550 7900 1200
Wire Wire Line
	7900 1200 8050 1200
Wire Wire Line
	8350 1200 8650 1200
Wire Wire Line
	8650 1200 8650 1550
Wire Wire Line
	8650 1550 9150 1550
Wire Wire Line
	9400 2050 9400 2550
Wire Wire Line
	7900 1550 7450 1550
Connection ~ 7900 1550
Wire Wire Line
	9250 2650 9600 2650
Wire Wire Line
	9600 2650 9600 1550
$Comp
L Device:R R?
U 1 1 5D14586D
P 6950 3100
F 0 "R?" V 7020 3146 50  0000 L CNN
F 1 "R" H 7020 3055 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6880 3100 50  0001 C CNN
F 3 "~" H 6950 3100 50  0001 C CNN
	1    6950 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D145874
P 6550 3100
F 0 "C?" H 6665 3146 50  0000 L CNN
F 1 "C" H 6665 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6588 2950 50  0001 C CNN
F 3 "~" H 6550 3100 50  0001 C CNN
	1    6550 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 2950 6950 2950
Connection ~ 6950 2950
Wire Wire Line
	6950 2950 7500 2950
$Comp
L Device:R R?
U 1 1 5D149BA1
P 6950 3550
F 0 "R?" V 7020 3596 50  0000 L CNN
F 1 "R" H 7020 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6880 3550 50  0001 C CNN
F 3 "~" H 6950 3550 50  0001 C CNN
	1    6950 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5D149BA8
P 6550 3550
F 0 "C?" H 6665 3596 50  0000 L CNN
F 1 "C" H 6665 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6588 3400 50  0001 C CNN
F 3 "~" H 6550 3550 50  0001 C CNN
	1    6550 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 3250 6550 3250
Connection ~ 6550 3250
Wire Wire Line
	6550 3250 6950 3250
Wire Wire Line
	6950 3850 6950 3700
Wire Wire Line
	6550 3700 6550 3850
Wire Wire Line
	6550 3850 6950 3850
Wire Wire Line
	6300 3250 6300 3850
Wire Wire Line
	6300 3850 6550 3850
Connection ~ 6550 3850
Wire Wire Line
	6300 3850 6150 3850
Connection ~ 6300 3850
Wire Wire Line
	6550 3400 6950 3400
Connection ~ 7500 3400
Connection ~ 6950 3400
Wire Wire Line
	6950 3400 7500 3400
$Comp
L power:GND #PWR?
U 1 1 5D14ECFB
P 7650 4050
F 0 "#PWR?" H 7650 3800 50  0001 C CNN
F 1 "GND" H 7850 3950 50  0000 C CNN
F 2 "" H 7650 4050 50  0001 C CNN
F 3 "" H 7650 4050 50  0001 C CNN
	1    7650 4050
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5D152018
P 7650 3950
F 0 "#PWR?" H 7650 3800 50  0001 C CNN
F 1 "+3.3V" H 7665 4123 50  0000 C CNN
F 2 "" H 7650 3950 50  0001 C CNN
F 3 "" H 7650 3950 50  0001 C CNN
	1    7650 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 3700 7450 3700
Wire Wire Line
	7450 3700 7450 4550
Wire Wire Line
	7450 4550 9350 4550
Wire Wire Line
	9350 4550 9350 2850
Wire Wire Line
	9350 2850 9250 2850
Wire Wire Line
	9250 2750 9600 2750
$Comp
L power:GND #PWR?
U 1 1 5D155818
P 8100 5400
F 0 "#PWR?" H 8100 5150 50  0001 C CNN
F 1 "GND" H 8300 5300 50  0000 C CNN
F 2 "" H 8100 5400 50  0001 C CNN
F 3 "" H 8100 5400 50  0001 C CNN
	1    8100 5400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5D15581E
P 8750 5100
F 0 "R?" V 8543 5100 50  0000 C CNN
F 1 "R" V 8634 5100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 8680 5100 50  0001 C CNN
F 3 "~" H 8750 5100 50  0001 C CNN
	1    8750 5100
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5D155825
P 7650 4750
F 0 "C?" V 7550 5050 50  0000 C CNN
F 1 "C" V 7550 4950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7688 4600 50  0001 C CNN
F 3 "~" H 7650 4750 50  0001 C CNN
	1    7650 4750
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5D15582D
P 7800 5100
F 0 "R?" V 7593 5100 50  0000 C CNN
F 1 "R" V 7684 5100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7730 5100 50  0001 C CNN
F 3 "~" H 7800 5100 50  0001 C CNN
	1    7800 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	7650 5100 7350 5100
$Comp
L Device:R R?
U 1 1 5D155835
P 8100 5250
F 0 "R?" H 8030 5204 50  0000 R CNN
F 1 "R" H 8030 5295 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8030 5250 50  0001 C CNN
F 3 "~" H 8100 5250 50  0001 C CNN
	1    8100 5250
	-1   0    0    1   
$EndComp
Connection ~ 8100 5100
Wire Wire Line
	7950 5100 8100 5100
Wire Wire Line
	7350 4750 7500 4750
Wire Wire Line
	7800 4750 8100 4750
Wire Wire Line
	8100 4750 8100 5100
Wire Wire Line
	8100 5100 8600 5100
Wire Wire Line
	9600 2750 9600 5100
Wire Wire Line
	8900 5100 9600 5100
Wire Wire Line
	7650 3600 7350 3600
Wire Wire Line
	7350 3600 7350 4750
Connection ~ 7350 4750
Wire Wire Line
	7350 4750 7350 5100
Text GLabel 6150 3850 0    50   Input ~ 0
V_BIAS
Text HLabel 7450 2750 0    50   Input ~ 0
transamp_in_a
Text HLabel 7350 4750 0    50   Input ~ 0
transamp_in_b
Text HLabel 9600 2650 2    50   Output ~ 0
transamp_out_a
Text HLabel 9600 2850 2    50   Output ~ 0
transamp_out_b
$EndSCHEMATC
