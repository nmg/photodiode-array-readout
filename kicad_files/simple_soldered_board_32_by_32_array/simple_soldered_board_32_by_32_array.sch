EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "QFN-64 Socket Board for UV 32x32 Photodiode Array"
Date "2019-05-21"
Rev "Revision 0"
Comp "CoolCAD Electronics LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "Nevo Magnezi"
$EndDescr
$Comp
L Connector:Conn_01x08_Female J6
U 1 1 5CE4444F
P 3550 5350
F 0 "J6" H 3577 5326 50  0000 L CNN
F 1 "Conn_01x08_Female" H 3577 5235 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 3550 5350 50  0001 C CNN
F 3 "~" H 3550 5350 50  0001 C CNN
	1    3550 5350
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J7
U 1 1 5CE44504
P 3550 4550
F 0 "J7" H 3577 4526 50  0000 L CNN
F 1 "Conn_01x08_Female" H 3577 4435 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 3550 4550 50  0001 C CNN
F 3 "~" H 3550 4550 50  0001 C CNN
	1    3550 4550
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J8
U 1 1 5CE445CA
P 3550 3750
F 0 "J8" H 3577 3726 50  0000 L CNN
F 1 "Conn_01x08_Female" H 3577 3635 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 3550 3750 50  0001 C CNN
F 3 "~" H 3550 3750 50  0001 C CNN
	1    3550 3750
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J9
U 1 1 5CE445D1
P 3550 2950
F 0 "J9" H 3577 2926 50  0000 L CNN
F 1 "Conn_01x08_Female" H 3577 2835 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 3550 2950 50  0001 C CNN
F 3 "~" H 3550 2950 50  0001 C CNN
	1    3550 2950
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J2
U 1 1 5CE44779
P 2150 3750
F 0 "J2" H 2177 3726 50  0000 L CNN
F 1 "Conn_01x08_Female" H 2177 3635 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 2150 3750 50  0001 C CNN
F 3 "~" H 2150 3750 50  0001 C CNN
	1    2150 3750
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J3
U 1 1 5CE44780
P 2150 4550
F 0 "J3" H 2177 4526 50  0000 L CNN
F 1 "Conn_01x08_Female" H 2177 4435 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 2150 4550 50  0001 C CNN
F 3 "~" H 2150 4550 50  0001 C CNN
	1    2150 4550
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J4
U 1 1 5CE44787
P 2150 5350
F 0 "J4" H 2177 5326 50  0000 L CNN
F 1 "Conn_01x08_Female" H 2177 5235 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 2150 5350 50  0001 C CNN
F 3 "~" H 2150 5350 50  0001 C CNN
	1    2150 5350
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J1
U 1 1 5CE44772
P 2150 2950
F 0 "J1" H 2177 2926 50  0000 L CNN
F 1 "Conn_01x08_Female" H 2177 2835 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 2150 2950 50  0001 C CNN
F 3 "~" H 2150 2950 50  0001 C CNN
	1    2150 2950
	-1   0    0    -1  
$EndComp
Text Label 2350 2650 0    50   ~ 0
N2
Text Label 2350 2750 0    50   ~ 0
N6
Text Label 2350 2850 0    50   ~ 0
N10
Text Label 2350 2950 0    50   ~ 0
N14
Text Label 2350 3050 0    50   ~ 0
N18
Text Label 2350 3150 0    50   ~ 0
N22
Text Label 2350 3250 0    50   ~ 0
N26
Text Label 2350 3350 0    50   ~ 0
N30
Text Label 2350 3450 0    50   ~ 0
P1
Text Label 2350 3550 0    50   ~ 0
P5
Text Label 2350 3650 0    50   ~ 0
P9
Text Label 2350 3750 0    50   ~ 0
P13
Text Label 2350 3850 0    50   ~ 0
P17
Text Label 2350 3950 0    50   ~ 0
P21
Text Label 2350 4050 0    50   ~ 0
P25
Text Label 2350 4150 0    50   ~ 0
P29
Text Label 2350 4250 0    50   ~ 0
N31
Text Label 2350 4350 0    50   ~ 0
N27
Text Label 2350 4450 0    50   ~ 0
N23
Text Label 2350 4550 0    50   ~ 0
N19
Text Label 2350 4650 0    50   ~ 0
N15
Text Label 2350 4750 0    50   ~ 0
N11
Text Label 2350 4850 0    50   ~ 0
N7
Text Label 2350 4950 0    50   ~ 0
N3
Text Label 2350 5050 0    50   ~ 0
P30
Text Label 2350 5150 0    50   ~ 0
P26
Text Label 2350 5250 0    50   ~ 0
P22
Text Label 2350 5350 0    50   ~ 0
P18
Text Label 2350 5450 0    50   ~ 0
P14
Text Label 2350 5550 0    50   ~ 0
P10
Text Label 2350 5650 0    50   ~ 0
P6
Text Label 2350 5750 0    50   ~ 0
P2
Text Label 3750 5050 0    50   ~ 0
N32
Text Label 3750 5150 0    50   ~ 0
N28
Text Label 3750 5250 0    50   ~ 0
N24
Text Label 3750 5350 0    50   ~ 0
N20
Text Label 3750 5450 0    50   ~ 0
N16
Text Label 3750 5550 0    50   ~ 0
N12
Text Label 3750 5650 0    50   ~ 0
N8
Text Label 3750 5750 0    50   ~ 0
N4
Text Label 3750 2650 0    50   ~ 0
P4
Text Label 3750 2750 0    50   ~ 0
P8
Text Label 3750 2850 0    50   ~ 0
P12
Text Label 3750 2950 0    50   ~ 0
P16
Text Label 3750 3050 0    50   ~ 0
P20
Text Label 3750 3150 0    50   ~ 0
P24
Text Label 3750 3250 0    50   ~ 0
P28
Text Label 3750 3350 0    50   ~ 0
P32
Text Label 3750 3450 0    50   ~ 0
N1
Text Label 3750 3550 0    50   ~ 0
N5
Text Label 3750 3650 0    50   ~ 0
N9
Text Label 3750 3750 0    50   ~ 0
N13
Text Label 3750 3850 0    50   ~ 0
N17
Text Label 3750 3950 0    50   ~ 0
N21
Text Label 3750 4050 0    50   ~ 0
N25
Text Label 3750 4150 0    50   ~ 0
N29
Text Label 3750 4250 0    50   ~ 0
P31
Text Label 3750 4350 0    50   ~ 0
P27
Text Label 3750 4450 0    50   ~ 0
P23
Text Label 3750 4550 0    50   ~ 0
P19
Text Label 3750 4650 0    50   ~ 0
P15
Text Label 3750 4750 0    50   ~ 0
P11
Text Label 3750 4850 0    50   ~ 0
P7
Text Label 3750 4950 0    50   ~ 0
P3
Text Label 8400 2250 1    50   ~ 0
N24
Text Label 8300 2250 1    50   ~ 0
N23
Text Label 8200 2250 1    50   ~ 0
N22
Text Label 8100 2250 1    50   ~ 0
N21
Text Label 8000 2250 1    50   ~ 0
N20
Text Label 7900 2250 1    50   ~ 0
N19
Text Label 7800 2250 1    50   ~ 0
N18
Text Label 7700 2250 1    50   ~ 0
N17
Text Label 6800 2250 1    50   ~ 0
N9
Text Label 6900 2250 1    50   ~ 0
N10
Text Label 7000 2250 1    50   ~ 0
N11
Text Label 7100 2250 1    50   ~ 0
N12
Text Label 7200 2250 1    50   ~ 0
N13
Text Label 7300 2250 1    50   ~ 0
N14
Text Label 7400 2250 1    50   ~ 0
N15
Text Label 7500 2250 1    50   ~ 0
N16
Text Label 9200 2250 1    50   ~ 0
N32
Text Label 9100 2250 1    50   ~ 0
N31
Text Label 9000 2250 1    50   ~ 0
N30
Text Label 8900 2250 1    50   ~ 0
N29
Text Label 8800 2250 1    50   ~ 0
N28
Text Label 8700 2250 1    50   ~ 0
N27
Text Label 8600 2250 1    50   ~ 0
N26
Text Label 8500 2250 1    50   ~ 0
N25
Text Label 6000 2250 1    50   ~ 0
N1
Text Label 6100 2250 1    50   ~ 0
N2
Text Label 6200 2250 1    50   ~ 0
N3
Text Label 6300 2250 1    50   ~ 0
N4
Text Label 6400 2250 1    50   ~ 0
N5
Text Label 6500 2250 1    50   ~ 0
N6
Text Label 6600 2250 1    50   ~ 0
N7
Text Label 6700 2250 1    50   ~ 0
N8
Text Label 5750 3350 2    50   ~ 0
P9
Text Label 5750 3450 2    50   ~ 0
P10
Text Label 5750 3550 2    50   ~ 0
P11
Text Label 5750 3650 2    50   ~ 0
P12
Text Label 5750 3750 2    50   ~ 0
P13
Text Label 5750 3850 2    50   ~ 0
P14
Text Label 5750 3950 2    50   ~ 0
P15
Text Label 5750 4050 2    50   ~ 0
P16
Text Label 5750 4850 2    50   ~ 0
P24
Text Label 5750 4750 2    50   ~ 0
P23
Text Label 5750 4650 2    50   ~ 0
P22
Text Label 5750 4550 2    50   ~ 0
P21
Text Label 5750 4450 2    50   ~ 0
P20
Text Label 5750 4350 2    50   ~ 0
P19
Text Label 5750 4250 2    50   ~ 0
P18
Text Label 5750 4150 2    50   ~ 0
P17
Text Label 5750 2550 2    50   ~ 0
P1
Text Label 5750 2650 2    50   ~ 0
P2
Text Label 5750 2750 2    50   ~ 0
P3
Text Label 5750 2850 2    50   ~ 0
P4
Text Label 5750 2950 2    50   ~ 0
P5
Text Label 5750 3050 2    50   ~ 0
P6
Text Label 5750 3150 2    50   ~ 0
P7
Text Label 5750 3250 2    50   ~ 0
P8
Text Label 5750 5650 2    50   ~ 0
P32
Text Label 5750 5550 2    50   ~ 0
P31
Text Label 5750 5450 2    50   ~ 0
P30
Text Label 5750 5350 2    50   ~ 0
P29
Text Label 5750 5250 2    50   ~ 0
P28
Text Label 5750 5150 2    50   ~ 0
P27
Text Label 5750 5050 2    50   ~ 0
P26
Text Label 5750 4950 2    50   ~ 0
P25
NoConn ~ 7550 5900
$Comp
L photodiode_readout_circuit:photodiode_array U1
U 1 1 5D111CA7
P 7550 4100
F 0 "U1" H 9277 4171 50  0000 L CNN
F 1 "photodiode_array" H 9277 4080 50  0000 L CNN
F 2 "" H 7800 4000 50  0001 C CNN
F 3 "" H 7800 4000 50  0001 C CNN
	1    7550 4100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
