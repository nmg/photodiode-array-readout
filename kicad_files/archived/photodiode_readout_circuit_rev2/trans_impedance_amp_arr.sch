EESchema Schematic File Version 4
LIBS:arduino_shield_template-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 5 6
Title "Trans-impedance array"
Date "2019-03-28"
Rev "1"
Comp "CoolCAD Electronics, LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 2019-04-26_20-18-08:ADG794BRQZ-REEL7 U4
U 1 1 5CF617CA
P 1750 3700
AR Path="/5CB2800A/5CF617CA" Ref="U4"  Part="1" 
AR Path="/5CF6E8CA/5CF617CA" Ref="U?"  Part="1" 
AR Path="/5CF713F4/5CF617CA" Ref="U?"  Part="1" 
AR Path="/5CF73EB0/5CF617CA" Ref="U?"  Part="1" 
AR Path="/5D0B9ED9/5CF617CA" Ref="U?"  Part="1" 
AR Path="/5D0B9EEB/5CF617CA" Ref="U?"  Part="1" 
AR Path="/5D0E2164/5CF617CA" Ref="U?"  Part="1" 
AR Path="/5D0E2176/5CF617CA" Ref="U?"  Part="1" 
AR Path="/5D115D42/5CF617CA" Ref="U?"  Part="1" 
AR Path="/5D115D5B/5CF617CA" Ref="U?"  Part="1" 
AR Path="/5D1494D0/5CF617CA" Ref="U?"  Part="1" 
AR Path="/5D1494E2/5CF617CA" Ref="U?"  Part="1" 
AR Path="/5D160D66/5CF617CA" Ref="U10"  Part="1" 
AR Path="/5D16695E/5CF617CA" Ref="U16"  Part="1" 
AR Path="/5D1DAFAE/5CF617CA" Ref="U?"  Part="1" 
AR Path="/5D1EFE44/5CF617CA" Ref="U22"  Part="1" 
F 0 "U22" H 2550 4187 60  0000 C CNN
F 1 "ADG794BRQZ-REEL7" H 2550 4081 60  0000 C CNN
F 2 "footprints:ADG794BRQZ-REEL7" H 2550 4040 60  0001 C CNN
F 3 "" H 1750 3700 60  0000 C CNN
	1    1750 3700
	1    0    0    -1  
$EndComp
$Comp
L 2019-04-26_19-51-24:AD8608ARUZ-REEL U3
U 1 1 5CF617D1
P 1750 1650
AR Path="/5CB2800A/5CF617D1" Ref="U3"  Part="1" 
AR Path="/5CF6E8CA/5CF617D1" Ref="U?"  Part="1" 
AR Path="/5CF713F4/5CF617D1" Ref="U?"  Part="1" 
AR Path="/5CF73EB0/5CF617D1" Ref="U?"  Part="1" 
AR Path="/5D0B9ED9/5CF617D1" Ref="U?"  Part="1" 
AR Path="/5D0B9EEB/5CF617D1" Ref="U?"  Part="1" 
AR Path="/5D0E2164/5CF617D1" Ref="U?"  Part="1" 
AR Path="/5D0E2176/5CF617D1" Ref="U?"  Part="1" 
AR Path="/5D115D42/5CF617D1" Ref="U?"  Part="1" 
AR Path="/5D115D5B/5CF617D1" Ref="U?"  Part="1" 
AR Path="/5D1494D0/5CF617D1" Ref="U?"  Part="1" 
AR Path="/5D1494E2/5CF617D1" Ref="U?"  Part="1" 
AR Path="/5D160D66/5CF617D1" Ref="U9"  Part="1" 
AR Path="/5D16695E/5CF617D1" Ref="U15"  Part="1" 
AR Path="/5D1DAFAE/5CF617D1" Ref="U?"  Part="1" 
AR Path="/5D1EFE44/5CF617D1" Ref="U21"  Part="1" 
F 0 "U21" H 2550 2037 60  0000 C CNN
F 1 "AD8608ARUZ-REEL" H 2550 1931 60  0000 C CNN
F 2 "footprints:AD8608ARUZ-REEL" H 2550 1890 60  0001 C CNN
F 3 "" H 1750 1650 60  0000 C CNN
	1    1750 1650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0127
U 1 1 5CF617D8
P 1450 3800
AR Path="/5CB2800A/5CF617D8" Ref="#PWR0127"  Part="1" 
AR Path="/5CF6E8CA/5CF617D8" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CF617D8" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CF617D8" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CF617D8" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CF617D8" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CF617D8" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CF617D8" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CF617D8" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CF617D8" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CF617D8" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CF617D8" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CF617D8" Ref="#PWR0137"  Part="1" 
AR Path="/5D16695E/5CF617D8" Ref="#PWR0147"  Part="1" 
AR Path="/5D1DAFAE/5CF617D8" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CF617D8" Ref="#PWR0157"  Part="1" 
F 0 "#PWR0157" H 1450 3550 50  0001 C CNN
F 1 "GND" H 1300 3750 50  0000 C CNN
F 2 "" H 1450 3800 50  0001 C CNN
F 3 "" H 1450 3800 50  0001 C CNN
	1    1450 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 3800 1750 3800
NoConn ~ 1750 4000
NoConn ~ 1750 4200
NoConn ~ 1750 4400
NoConn ~ 1750 4600
$Comp
L power:GND #PWR0128
U 1 1 5CF617E3
P 3350 4400
AR Path="/5CB2800A/5CF617E3" Ref="#PWR0128"  Part="1" 
AR Path="/5CF6E8CA/5CF617E3" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CF617E3" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CF617E3" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CF617E3" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CF617E3" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CF617E3" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CF617E3" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CF617E3" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CF617E3" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CF617E3" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CF617E3" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CF617E3" Ref="#PWR0138"  Part="1" 
AR Path="/5D16695E/5CF617E3" Ref="#PWR0148"  Part="1" 
AR Path="/5D1DAFAE/5CF617E3" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CF617E3" Ref="#PWR0158"  Part="1" 
F 0 "#PWR0158" H 3350 4150 50  0001 C CNN
F 1 "GND" H 3355 4227 50  0000 C CNN
F 2 "" H 3350 4400 50  0001 C CNN
F 3 "" H 3350 4400 50  0001 C CNN
	1    3350 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 1650 1750 1750
Connection ~ 1750 1750
Wire Wire Line
	1750 1750 1750 1850
Connection ~ 1750 1850
Wire Wire Line
	1750 1850 1750 1950
Wire Wire Line
	1750 1650 1500 1650
Connection ~ 1750 1650
Text GLabel 1500 1650 0    50   Input ~ 0
bias_op_amps
Text GLabel 1750 3700 0    50   Input ~ 0
reset_op_amps
Wire Wire Line
	1100 2050 1750 2050
Wire Wire Line
	1000 4100 1000 2150
Wire Wire Line
	1000 2150 1750 2150
Wire Wire Line
	1750 2250 900  2250
Wire Wire Line
	900  2250 900  4300
Wire Wire Line
	800  4500 800  2350
Wire Wire Line
	800  2350 1750 2350
Wire Wire Line
	3350 3800 3550 3800
Wire Wire Line
	3550 3800 3550 1650
Wire Wire Line
	3550 1650 3350 1650
Wire Wire Line
	3350 1750 3650 1750
Wire Wire Line
	3650 1750 3650 3900
Wire Wire Line
	3650 3900 3350 3900
Wire Wire Line
	3350 4000 3750 4000
Wire Wire Line
	3750 4000 3750 1850
Wire Wire Line
	3750 1850 3350 1850
Wire Wire Line
	3350 1950 3850 1950
Wire Wire Line
	3850 1950 3850 4100
Wire Wire Line
	3850 4100 3350 4100
Wire Wire Line
	1750 3900 1600 3900
Wire Wire Line
	1600 3900 1600 3950
Wire Wire Line
	1600 3950 1100 3950
Wire Wire Line
	1100 2050 1100 3950
$Comp
L power:GND #PWR0129
U 1 1 5CF61809
P 1600 2850
AR Path="/5CB2800A/5CF61809" Ref="#PWR0129"  Part="1" 
AR Path="/5CF6E8CA/5CF61809" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CF61809" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CF61809" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CF61809" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CF61809" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CF61809" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CF61809" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CF61809" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CF61809" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CF61809" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CF61809" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CF61809" Ref="#PWR0139"  Part="1" 
AR Path="/5D16695E/5CF61809" Ref="#PWR0149"  Part="1" 
AR Path="/5D1DAFAE/5CF61809" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CF61809" Ref="#PWR0159"  Part="1" 
F 0 "#PWR0159" H 1600 2600 50  0001 C CNN
F 1 "GND" H 1450 2800 50  0000 C CNN
F 2 "" H 1600 2850 50  0001 C CNN
F 3 "" H 1600 2850 50  0001 C CNN
	1    1600 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 2850 1750 2850
$Comp
L power:+3.3V #PWR0130
U 1 1 5CF61810
P 1750 4900
AR Path="/5CB2800A/5CF61810" Ref="#PWR0130"  Part="1" 
AR Path="/5CF6E8CA/5CF61810" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CF61810" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CF61810" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CF61810" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CF61810" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CF61810" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CF61810" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CF61810" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CF61810" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CF61810" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CF61810" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CF61810" Ref="#PWR0140"  Part="1" 
AR Path="/5D16695E/5CF61810" Ref="#PWR0150"  Part="1" 
AR Path="/5D1DAFAE/5CF61810" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CF61810" Ref="#PWR0160"  Part="1" 
F 0 "#PWR0160" H 1750 4750 50  0001 C CNN
F 1 "+3.3V" H 1765 5073 50  0000 C CNN
F 2 "" H 1750 4900 50  0001 C CNN
F 3 "" H 1750 4900 50  0001 C CNN
	1    1750 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 3050 1700 2950
Wire Wire Line
	1700 2950 1750 2950
Wire Wire Line
	1700 3050 1300 3050
Wire Wire Line
	1300 3050 1300 2800
$Comp
L power:+3.3V #PWR0131
U 1 1 5CF6181A
P 1300 2800
AR Path="/5CB2800A/5CF6181A" Ref="#PWR0131"  Part="1" 
AR Path="/5CF6E8CA/5CF6181A" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CF6181A" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CF6181A" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CF6181A" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CF6181A" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CF6181A" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CF6181A" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CF6181A" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CF6181A" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CF6181A" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CF6181A" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CF6181A" Ref="#PWR0141"  Part="1" 
AR Path="/5D16695E/5CF6181A" Ref="#PWR0151"  Part="1" 
AR Path="/5D1DAFAE/5CF6181A" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CF6181A" Ref="#PWR0161"  Part="1" 
F 0 "#PWR0161" H 1300 2650 50  0001 C CNN
F 1 "+3.3V" H 1315 2973 50  0000 C CNN
F 2 "" H 1300 2800 50  0001 C CNN
F 3 "" H 1300 2800 50  0001 C CNN
	1    1300 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5CF61820
P 2550 5350
AR Path="/5C9AAC30/5CF61820" Ref="C?"  Part="1" 
AR Path="/5CB83508/5CF61820" Ref="C?"  Part="1" 
AR Path="/5CB24539/5C9AAC30/5CF61820" Ref="C?"  Part="1" 
AR Path="/5CB24539/5CB83508/5CF61820" Ref="C?"  Part="1" 
AR Path="/5CB2800A/5CF61820" Ref="C1"  Part="1" 
AR Path="/5CB2803C/5CF61820" Ref="C?"  Part="1" 
AR Path="/5CF6E8CA/5CF61820" Ref="C?"  Part="1" 
AR Path="/5CF713F4/5CF61820" Ref="C?"  Part="1" 
AR Path="/5CF73EB0/5CF61820" Ref="C?"  Part="1" 
AR Path="/5D0B9ED9/5CF61820" Ref="C?"  Part="1" 
AR Path="/5D0B9EEB/5CF61820" Ref="C?"  Part="1" 
AR Path="/5D0E2164/5CF61820" Ref="C?"  Part="1" 
AR Path="/5D0E2176/5CF61820" Ref="C?"  Part="1" 
AR Path="/5D115D42/5CF61820" Ref="C?"  Part="1" 
AR Path="/5D115D5B/5CF61820" Ref="C?"  Part="1" 
AR Path="/5D1494D0/5CF61820" Ref="C?"  Part="1" 
AR Path="/5D1494E2/5CF61820" Ref="C?"  Part="1" 
AR Path="/5D160D66/5CF61820" Ref="C9"  Part="1" 
AR Path="/5D16695E/5CF61820" Ref="C17"  Part="1" 
AR Path="/5D1DAFAE/5CF61820" Ref="C?"  Part="1" 
AR Path="/5D1EFE44/5CF61820" Ref="C25"  Part="1" 
F 0 "C25" V 2400 5550 50  0000 C CNN
F 1 "C" V 2500 5550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2588 5200 50  0001 C CNN
F 3 "~" H 2550 5350 50  0001 C CNN
	1    2550 5350
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5CF61827
P 2550 5600
AR Path="/5C9AAC30/5CF61827" Ref="C?"  Part="1" 
AR Path="/5CB83508/5CF61827" Ref="C?"  Part="1" 
AR Path="/5CB24539/5C9AAC30/5CF61827" Ref="C?"  Part="1" 
AR Path="/5CB24539/5CB83508/5CF61827" Ref="C?"  Part="1" 
AR Path="/5CB2800A/5CF61827" Ref="C2"  Part="1" 
AR Path="/5CB2803C/5CF61827" Ref="C?"  Part="1" 
AR Path="/5CF6E8CA/5CF61827" Ref="C?"  Part="1" 
AR Path="/5CF713F4/5CF61827" Ref="C?"  Part="1" 
AR Path="/5CF73EB0/5CF61827" Ref="C?"  Part="1" 
AR Path="/5D0B9ED9/5CF61827" Ref="C?"  Part="1" 
AR Path="/5D0B9EEB/5CF61827" Ref="C?"  Part="1" 
AR Path="/5D0E2164/5CF61827" Ref="C?"  Part="1" 
AR Path="/5D0E2176/5CF61827" Ref="C?"  Part="1" 
AR Path="/5D115D42/5CF61827" Ref="C?"  Part="1" 
AR Path="/5D115D5B/5CF61827" Ref="C?"  Part="1" 
AR Path="/5D1494D0/5CF61827" Ref="C?"  Part="1" 
AR Path="/5D1494E2/5CF61827" Ref="C?"  Part="1" 
AR Path="/5D160D66/5CF61827" Ref="C10"  Part="1" 
AR Path="/5D16695E/5CF61827" Ref="C18"  Part="1" 
AR Path="/5D1DAFAE/5CF61827" Ref="C?"  Part="1" 
AR Path="/5D1EFE44/5CF61827" Ref="C26"  Part="1" 
F 0 "C26" V 2400 5800 50  0000 C CNN
F 1 "C" V 2500 5800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2588 5450 50  0001 C CNN
F 3 "~" H 2550 5600 50  0001 C CNN
	1    2550 5600
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5CF6182E
P 2550 5900
AR Path="/5C9AAC30/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5CB83508/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5CB24539/5C9AAC30/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5CB24539/5CB83508/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5CB2800A/5CF6182E" Ref="C3"  Part="1" 
AR Path="/5CB2803C/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5CF6E8CA/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5CF713F4/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5CF73EB0/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5D0B9ED9/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5D0B9EEB/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5D0E2164/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5D0E2176/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5D115D42/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5D115D5B/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5D1494D0/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5D1494E2/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5D160D66/5CF6182E" Ref="C11"  Part="1" 
AR Path="/5D16695E/5CF6182E" Ref="C19"  Part="1" 
AR Path="/5D1DAFAE/5CF6182E" Ref="C?"  Part="1" 
AR Path="/5D1EFE44/5CF6182E" Ref="C27"  Part="1" 
F 0 "C27" V 2400 6100 50  0000 C CNN
F 1 "C" V 2500 6100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2588 5750 50  0001 C CNN
F 3 "~" H 2550 5900 50  0001 C CNN
	1    2550 5900
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5CF61835
P 2550 6200
AR Path="/5C9AAC30/5CF61835" Ref="C?"  Part="1" 
AR Path="/5CB83508/5CF61835" Ref="C?"  Part="1" 
AR Path="/5CB24539/5C9AAC30/5CF61835" Ref="C?"  Part="1" 
AR Path="/5CB24539/5CB83508/5CF61835" Ref="C?"  Part="1" 
AR Path="/5CB2800A/5CF61835" Ref="C4"  Part="1" 
AR Path="/5CB2803C/5CF61835" Ref="C?"  Part="1" 
AR Path="/5CF6E8CA/5CF61835" Ref="C?"  Part="1" 
AR Path="/5CF713F4/5CF61835" Ref="C?"  Part="1" 
AR Path="/5CF73EB0/5CF61835" Ref="C?"  Part="1" 
AR Path="/5D0B9ED9/5CF61835" Ref="C?"  Part="1" 
AR Path="/5D0B9EEB/5CF61835" Ref="C?"  Part="1" 
AR Path="/5D0E2164/5CF61835" Ref="C?"  Part="1" 
AR Path="/5D0E2176/5CF61835" Ref="C?"  Part="1" 
AR Path="/5D115D42/5CF61835" Ref="C?"  Part="1" 
AR Path="/5D115D5B/5CF61835" Ref="C?"  Part="1" 
AR Path="/5D1494D0/5CF61835" Ref="C?"  Part="1" 
AR Path="/5D1494E2/5CF61835" Ref="C?"  Part="1" 
AR Path="/5D160D66/5CF61835" Ref="C12"  Part="1" 
AR Path="/5D16695E/5CF61835" Ref="C20"  Part="1" 
AR Path="/5D1DAFAE/5CF61835" Ref="C?"  Part="1" 
AR Path="/5D1EFE44/5CF61835" Ref="C28"  Part="1" 
F 0 "C28" V 2400 6400 50  0000 C CNN
F 1 "C" V 2500 6400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2588 6050 50  0001 C CNN
F 3 "~" H 2550 6200 50  0001 C CNN
	1    2550 6200
	0    1    1    0   
$EndComp
Wire Wire Line
	2700 5350 3550 5350
Wire Wire Line
	3550 5350 3550 3800
Connection ~ 3550 3800
Wire Wire Line
	3650 3900 3650 5600
Wire Wire Line
	3650 5600 2700 5600
Connection ~ 3650 3900
Wire Wire Line
	2700 5900 3750 5900
Wire Wire Line
	3750 5900 3750 4000
Connection ~ 3750 4000
Wire Wire Line
	3850 4100 3850 6200
Wire Wire Line
	3850 6200 2700 6200
Connection ~ 3850 4100
Wire Wire Line
	1000 4100 1750 4100
Wire Wire Line
	900  4300 1750 4300
Wire Wire Line
	800  4500 1750 4500
Wire Wire Line
	1100 3950 1100 5350
Wire Wire Line
	1100 5350 2400 5350
Connection ~ 1100 3950
Wire Wire Line
	2400 5600 1000 5600
Wire Wire Line
	1000 5600 1000 4100
Connection ~ 1000 4100
Wire Wire Line
	900  4300 900  5900
Wire Wire Line
	900  5900 2400 5900
Connection ~ 900  4300
Wire Wire Line
	2400 6200 800  6200
Wire Wire Line
	800  6200 800  4500
Connection ~ 800  4500
Text HLabel 3550 1650 2    50   Output ~ 0
O1
Text HLabel 8450 1600 2    50   Output ~ 0
O2
Text HLabel 3650 1750 2    50   Output ~ 0
O3
Text HLabel 8550 1700 2    50   Output ~ 0
O4
Text HLabel 1100 2050 0    50   Input ~ 0
N1
Text HLabel 6000 2000 0    50   Input ~ 0
N2
Text HLabel 1000 2150 0    50   Input ~ 0
N3
Text HLabel 5900 2100 0    50   Input ~ 0
N4
$Comp
L 2019-04-26_20-18-08:ADG794BRQZ-REEL7 U7
U 1 1 5CF625EB
P 6650 3650
AR Path="/5CB2800A/5CF625EB" Ref="U7"  Part="1" 
AR Path="/5CF6E8CA/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5CF713F4/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5CF73EB0/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D0B9ED9/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D0B9EEB/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D0E2164/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D0E2176/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D115D42/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D115D5B/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D1494D0/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D1494E2/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D160D66/5CF625EB" Ref="U13"  Part="1" 
AR Path="/5D16695E/5CF625EB" Ref="U19"  Part="1" 
AR Path="/5D1DAFAE/5CF625EB" Ref="U?"  Part="1" 
AR Path="/5D1EFE44/5CF625EB" Ref="U25"  Part="1" 
F 0 "U25" H 7450 4137 60  0000 C CNN
F 1 "ADG794BRQZ-REEL7" H 7450 4031 60  0000 C CNN
F 2 "footprints:ADG794BRQZ-REEL7" H 7450 3990 60  0001 C CNN
F 3 "" H 6650 3650 60  0000 C CNN
	1    6650 3650
	1    0    0    -1  
$EndComp
$Comp
L 2019-04-26_19-51-24:AD8608ARUZ-REEL U6
U 1 1 5CF625F2
P 6650 1600
AR Path="/5CB2800A/5CF625F2" Ref="U6"  Part="1" 
AR Path="/5CF6E8CA/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5CF713F4/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5CF73EB0/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D0B9ED9/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D0B9EEB/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D0E2164/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D0E2176/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D115D42/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D115D5B/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D1494D0/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D1494E2/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D160D66/5CF625F2" Ref="U12"  Part="1" 
AR Path="/5D16695E/5CF625F2" Ref="U18"  Part="1" 
AR Path="/5D1DAFAE/5CF625F2" Ref="U?"  Part="1" 
AR Path="/5D1EFE44/5CF625F2" Ref="U24"  Part="1" 
F 0 "U24" H 7450 1987 60  0000 C CNN
F 1 "AD8608ARUZ-REEL" H 7450 1881 60  0000 C CNN
F 2 "footprints:AD8608ARUZ-REEL" H 7450 1840 60  0001 C CNN
F 3 "" H 6650 1600 60  0000 C CNN
	1    6650 1600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0132
U 1 1 5CF625F9
P 6350 3750
AR Path="/5CB2800A/5CF625F9" Ref="#PWR0132"  Part="1" 
AR Path="/5CF6E8CA/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CF625F9" Ref="#PWR0142"  Part="1" 
AR Path="/5D16695E/5CF625F9" Ref="#PWR0152"  Part="1" 
AR Path="/5D1DAFAE/5CF625F9" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CF625F9" Ref="#PWR0162"  Part="1" 
F 0 "#PWR0162" H 6350 3500 50  0001 C CNN
F 1 "GND" H 6200 3700 50  0000 C CNN
F 2 "" H 6350 3750 50  0001 C CNN
F 3 "" H 6350 3750 50  0001 C CNN
	1    6350 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3750 6650 3750
NoConn ~ 6650 3950
NoConn ~ 6650 4150
NoConn ~ 6650 4350
NoConn ~ 6650 4550
$Comp
L power:GND #PWR0133
U 1 1 5CF62604
P 8250 4350
AR Path="/5CB2800A/5CF62604" Ref="#PWR0133"  Part="1" 
AR Path="/5CF6E8CA/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CF62604" Ref="#PWR0143"  Part="1" 
AR Path="/5D16695E/5CF62604" Ref="#PWR0153"  Part="1" 
AR Path="/5D1DAFAE/5CF62604" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CF62604" Ref="#PWR0163"  Part="1" 
F 0 "#PWR0163" H 8250 4100 50  0001 C CNN
F 1 "GND" H 8255 4177 50  0000 C CNN
F 2 "" H 8250 4350 50  0001 C CNN
F 3 "" H 8250 4350 50  0001 C CNN
	1    8250 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 1600 6650 1700
Connection ~ 6650 1700
Wire Wire Line
	6650 1700 6650 1800
Connection ~ 6650 1800
Wire Wire Line
	6650 1800 6650 1900
Wire Wire Line
	6650 1600 6400 1600
Connection ~ 6650 1600
Text GLabel 6400 1600 0    50   Input ~ 0
bias_op_amps
Text GLabel 6650 3650 0    50   Input ~ 0
reset_op_amps
Wire Wire Line
	6000 2000 6650 2000
Wire Wire Line
	5900 4050 5900 2100
Wire Wire Line
	5900 2100 6650 2100
Wire Wire Line
	6650 2200 5800 2200
Wire Wire Line
	5800 2200 5800 4250
Wire Wire Line
	5700 4450 5700 2300
Wire Wire Line
	5700 2300 6650 2300
Wire Wire Line
	8250 3750 8450 3750
Wire Wire Line
	8450 3750 8450 1600
Wire Wire Line
	8450 1600 8250 1600
Wire Wire Line
	8250 1700 8550 1700
Wire Wire Line
	8550 1700 8550 3850
Wire Wire Line
	8550 3850 8250 3850
Wire Wire Line
	8250 3950 8650 3950
Wire Wire Line
	8650 3950 8650 1800
Wire Wire Line
	8650 1800 8250 1800
Wire Wire Line
	8250 1900 8750 1900
Wire Wire Line
	8750 1900 8750 4050
Wire Wire Line
	8750 4050 8250 4050
Wire Wire Line
	6650 3850 6500 3850
Wire Wire Line
	6500 3850 6500 3900
Wire Wire Line
	6500 3900 6000 3900
Wire Wire Line
	6000 2000 6000 3900
$Comp
L power:GND #PWR0134
U 1 1 5CF6262A
P 6500 2800
AR Path="/5CB2800A/5CF6262A" Ref="#PWR0134"  Part="1" 
AR Path="/5CF6E8CA/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CF6262A" Ref="#PWR0144"  Part="1" 
AR Path="/5D16695E/5CF6262A" Ref="#PWR0154"  Part="1" 
AR Path="/5D1DAFAE/5CF6262A" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CF6262A" Ref="#PWR0164"  Part="1" 
F 0 "#PWR0164" H 6500 2550 50  0001 C CNN
F 1 "GND" H 6350 2750 50  0000 C CNN
F 2 "" H 6500 2800 50  0001 C CNN
F 3 "" H 6500 2800 50  0001 C CNN
	1    6500 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2800 6650 2800
$Comp
L power:+3.3V #PWR0135
U 1 1 5CF62631
P 6650 4850
AR Path="/5CB2800A/5CF62631" Ref="#PWR0135"  Part="1" 
AR Path="/5CF6E8CA/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CF62631" Ref="#PWR0145"  Part="1" 
AR Path="/5D16695E/5CF62631" Ref="#PWR0155"  Part="1" 
AR Path="/5D1DAFAE/5CF62631" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CF62631" Ref="#PWR0165"  Part="1" 
F 0 "#PWR0165" H 6650 4700 50  0001 C CNN
F 1 "+3.3V" H 6665 5023 50  0000 C CNN
F 2 "" H 6650 4850 50  0001 C CNN
F 3 "" H 6650 4850 50  0001 C CNN
	1    6650 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 3000 6600 2900
Wire Wire Line
	6600 2900 6650 2900
Wire Wire Line
	6600 3000 6200 3000
Wire Wire Line
	6200 3000 6200 2750
$Comp
L power:+3.3V #PWR0136
U 1 1 5CF6263B
P 6200 2750
AR Path="/5CB2800A/5CF6263B" Ref="#PWR0136"  Part="1" 
AR Path="/5CF6E8CA/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5CF713F4/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5CF73EB0/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9ED9/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D0B9EEB/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2164/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D0E2176/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D115D42/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D115D5B/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D1494D0/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D1494E2/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D160D66/5CF6263B" Ref="#PWR0146"  Part="1" 
AR Path="/5D16695E/5CF6263B" Ref="#PWR0156"  Part="1" 
AR Path="/5D1DAFAE/5CF6263B" Ref="#PWR?"  Part="1" 
AR Path="/5D1EFE44/5CF6263B" Ref="#PWR0166"  Part="1" 
F 0 "#PWR0166" H 6200 2600 50  0001 C CNN
F 1 "+3.3V" H 6215 2923 50  0000 C CNN
F 2 "" H 6200 2750 50  0001 C CNN
F 3 "" H 6200 2750 50  0001 C CNN
	1    6200 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5CF62641
P 7450 5300
AR Path="/5C9AAC30/5CF62641" Ref="C?"  Part="1" 
AR Path="/5CB83508/5CF62641" Ref="C?"  Part="1" 
AR Path="/5CB24539/5C9AAC30/5CF62641" Ref="C?"  Part="1" 
AR Path="/5CB24539/5CB83508/5CF62641" Ref="C?"  Part="1" 
AR Path="/5CB2800A/5CF62641" Ref="C5"  Part="1" 
AR Path="/5CB2803C/5CF62641" Ref="C?"  Part="1" 
AR Path="/5CF6E8CA/5CF62641" Ref="C?"  Part="1" 
AR Path="/5CF713F4/5CF62641" Ref="C?"  Part="1" 
AR Path="/5CF73EB0/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D0B9ED9/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D0B9EEB/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D0E2164/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D0E2176/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D115D42/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D115D5B/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D1494D0/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D1494E2/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D160D66/5CF62641" Ref="C13"  Part="1" 
AR Path="/5D16695E/5CF62641" Ref="C21"  Part="1" 
AR Path="/5D1DAFAE/5CF62641" Ref="C?"  Part="1" 
AR Path="/5D1EFE44/5CF62641" Ref="C29"  Part="1" 
F 0 "C29" V 7300 5500 50  0000 C CNN
F 1 "C" V 7400 5500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7488 5150 50  0001 C CNN
F 3 "~" H 7450 5300 50  0001 C CNN
	1    7450 5300
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5CF62648
P 7450 5550
AR Path="/5C9AAC30/5CF62648" Ref="C?"  Part="1" 
AR Path="/5CB83508/5CF62648" Ref="C?"  Part="1" 
AR Path="/5CB24539/5C9AAC30/5CF62648" Ref="C?"  Part="1" 
AR Path="/5CB24539/5CB83508/5CF62648" Ref="C?"  Part="1" 
AR Path="/5CB2800A/5CF62648" Ref="C6"  Part="1" 
AR Path="/5CB2803C/5CF62648" Ref="C?"  Part="1" 
AR Path="/5CF6E8CA/5CF62648" Ref="C?"  Part="1" 
AR Path="/5CF713F4/5CF62648" Ref="C?"  Part="1" 
AR Path="/5CF73EB0/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D0B9ED9/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D0B9EEB/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D0E2164/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D0E2176/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D115D42/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D115D5B/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D1494D0/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D1494E2/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D160D66/5CF62648" Ref="C14"  Part="1" 
AR Path="/5D16695E/5CF62648" Ref="C22"  Part="1" 
AR Path="/5D1DAFAE/5CF62648" Ref="C?"  Part="1" 
AR Path="/5D1EFE44/5CF62648" Ref="C30"  Part="1" 
F 0 "C30" V 7300 5750 50  0000 C CNN
F 1 "C" V 7400 5750 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7488 5400 50  0001 C CNN
F 3 "~" H 7450 5550 50  0001 C CNN
	1    7450 5550
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5CF6264F
P 7450 5850
AR Path="/5C9AAC30/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5CB83508/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5CB24539/5C9AAC30/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5CB24539/5CB83508/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5CB2800A/5CF6264F" Ref="C7"  Part="1" 
AR Path="/5CB2803C/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5CF6E8CA/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5CF713F4/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5CF73EB0/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D0B9ED9/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D0B9EEB/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D0E2164/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D0E2176/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D115D42/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D115D5B/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D1494D0/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D1494E2/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D160D66/5CF6264F" Ref="C15"  Part="1" 
AR Path="/5D16695E/5CF6264F" Ref="C23"  Part="1" 
AR Path="/5D1DAFAE/5CF6264F" Ref="C?"  Part="1" 
AR Path="/5D1EFE44/5CF6264F" Ref="C31"  Part="1" 
F 0 "C31" V 7300 6050 50  0000 C CNN
F 1 "C" V 7400 6050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7488 5700 50  0001 C CNN
F 3 "~" H 7450 5850 50  0001 C CNN
	1    7450 5850
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5CF62656
P 7450 6150
AR Path="/5C9AAC30/5CF62656" Ref="C?"  Part="1" 
AR Path="/5CB83508/5CF62656" Ref="C?"  Part="1" 
AR Path="/5CB24539/5C9AAC30/5CF62656" Ref="C?"  Part="1" 
AR Path="/5CB24539/5CB83508/5CF62656" Ref="C?"  Part="1" 
AR Path="/5CB2800A/5CF62656" Ref="C8"  Part="1" 
AR Path="/5CB2803C/5CF62656" Ref="C?"  Part="1" 
AR Path="/5CF6E8CA/5CF62656" Ref="C?"  Part="1" 
AR Path="/5CF713F4/5CF62656" Ref="C?"  Part="1" 
AR Path="/5CF73EB0/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D0B9ED9/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D0B9EEB/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D0E2164/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D0E2176/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D115D42/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D115D5B/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D1494D0/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D1494E2/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D160D66/5CF62656" Ref="C16"  Part="1" 
AR Path="/5D16695E/5CF62656" Ref="C24"  Part="1" 
AR Path="/5D1DAFAE/5CF62656" Ref="C?"  Part="1" 
AR Path="/5D1EFE44/5CF62656" Ref="C32"  Part="1" 
F 0 "C32" V 7300 6350 50  0000 C CNN
F 1 "C" V 7400 6350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7488 6000 50  0001 C CNN
F 3 "~" H 7450 6150 50  0001 C CNN
	1    7450 6150
	0    1    1    0   
$EndComp
Wire Wire Line
	7600 5300 8450 5300
Wire Wire Line
	8450 5300 8450 3750
Connection ~ 8450 3750
Wire Wire Line
	8550 3850 8550 5550
Wire Wire Line
	8550 5550 7600 5550
Connection ~ 8550 3850
Wire Wire Line
	7600 5850 8650 5850
Wire Wire Line
	8650 5850 8650 3950
Connection ~ 8650 3950
Wire Wire Line
	8750 4050 8750 6150
Wire Wire Line
	8750 6150 7600 6150
Connection ~ 8750 4050
Wire Wire Line
	5900 4050 6650 4050
Wire Wire Line
	5800 4250 6650 4250
Wire Wire Line
	5700 4450 6650 4450
Wire Wire Line
	6000 3900 6000 5300
Wire Wire Line
	6000 5300 7300 5300
Connection ~ 6000 3900
Wire Wire Line
	7300 5550 5900 5550
Wire Wire Line
	5900 5550 5900 4050
Connection ~ 5900 4050
Wire Wire Line
	5800 4250 5800 5850
Wire Wire Line
	5800 5850 7300 5850
Connection ~ 5800 4250
Wire Wire Line
	7300 6150 5700 6150
Wire Wire Line
	5700 6150 5700 4450
Connection ~ 5700 4450
Text HLabel 3750 1850 2    50   Output ~ 0
O5
Text HLabel 8650 1800 2    50   Output ~ 0
O6
Text HLabel 3850 1950 2    50   Output ~ 0
O7
Text HLabel 8750 1900 2    50   Output ~ 0
O8
Text HLabel 900  2250 0    50   Input ~ 0
N5
Text HLabel 5800 2200 0    50   Input ~ 0
N6
Text HLabel 800  2350 0    50   Input ~ 0
N7
Text HLabel 5700 2300 0    50   Input ~ 0
N8
$EndSCHEMATC
