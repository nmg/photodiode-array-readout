EESchema Schematic File Version 4
LIBS:arduino_shield_template-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+3.3V #PWR?
U 1 1 5CC69989
P 3650 1900
AR Path="/5CC69989" Ref="#PWR?"  Part="1" 
AR Path="/5CC637E9/5CC69989" Ref="#PWR09"  Part="1" 
AR Path="/5CB24539/5CC637E9/5CC69989" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5CC69989" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 3650 1750 50  0001 C CNN
F 1 "+3.3V" H 3665 2073 50  0000 C CNN
F 2 "" H 3650 1900 50  0001 C CNN
F 3 "" H 3650 1900 50  0001 C CNN
	1    3650 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 1950 3650 1950
Wire Wire Line
	3650 1950 3650 1900
$Comp
L power:+3.3V #PWR?
U 1 1 5CC69998
P 5650 1700
AR Path="/5CC69998" Ref="#PWR?"  Part="1" 
AR Path="/5CC637E9/5CC69998" Ref="#PWR012"  Part="1" 
AR Path="/5CB24539/5CC637E9/5CC69998" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5CC69998" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 5650 1550 50  0001 C CNN
F 1 "+3.3V" H 5665 1873 50  0000 C CNN
F 2 "" H 5650 1700 50  0001 C CNN
F 3 "" H 5650 1700 50  0001 C CNN
	1    5650 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 1750 5650 1700
Wire Wire Line
	4050 2750 3750 2750
Wire Wire Line
	3750 2750 3750 2850
$Comp
L power:GND #PWR?
U 1 1 5CC699A2
P 3750 2850
AR Path="/5CC699A2" Ref="#PWR?"  Part="1" 
AR Path="/5CC637E9/5CC699A2" Ref="#PWR010"  Part="1" 
AR Path="/5CB24539/5CC637E9/5CC699A2" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5CC699A2" Ref="#PWR0105"  Part="1" 
F 0 "#PWR0105" H 3750 2600 50  0001 C CNN
F 1 "GND" H 3755 2677 50  0000 C CNN
F 2 "" H 3750 2850 50  0001 C CNN
F 3 "" H 3750 2850 50  0001 C CNN
	1    3750 2850
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OPA4340EA U?
U 5 1 5CC6DD3A
P 8550 3900
AR Path="/5CBABA99/5CC6DD3A" Ref="U?"  Part="5" 
AR Path="/5CC6DD3A" Ref="U?"  Part="5" 
AR Path="/5CC637E9/5CC6DD3A" Ref="U5"  Part="5" 
AR Path="/5CB24539/5CC637E9/5CC6DD3A" Ref="U?"  Part="5" 
AR Path="/5CB3ED8C/5CC6DD3A" Ref="U5"  Part="5" 
F 0 "U5" H 8508 3946 50  0000 L CNN
F 1 "OPA4340EA" H 8508 3855 50  0000 L CNN
F 2 "digikey-footprints:SSOP-16_W3.90mm" H 8550 3900 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa340.pdf" H 8550 3900 50  0001 C CNN
	5    8550 3900
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5CC6DD41
P 9250 2450
AR Path="/5CBABA99/5CC6DD41" Ref="#PWR?"  Part="1" 
AR Path="/5CC6DD41" Ref="#PWR?"  Part="1" 
AR Path="/5CC637E9/5CC6DD41" Ref="#PWR015"  Part="1" 
AR Path="/5CB24539/5CC637E9/5CC6DD41" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5CC6DD41" Ref="#PWR0106"  Part="1" 
F 0 "#PWR0106" H 9250 2300 50  0001 C CNN
F 1 "+3.3V" H 9265 2623 50  0000 C CNN
F 2 "" H 9250 2450 50  0001 C CNN
F 3 "" H 9250 2450 50  0001 C CNN
	1    9250 2450
	1    0    0    -1  
$EndComp
Connection ~ 9250 2450
$Comp
L Amplifier_Operational:OPA4340EA U?
U 5 1 5CC6DD48
P 8600 2750
AR Path="/5CBABA99/5CC6DD48" Ref="U?"  Part="5" 
AR Path="/5CC6DD48" Ref="U?"  Part="5" 
AR Path="/5CC637E9/5CC6DD48" Ref="U8"  Part="5" 
AR Path="/5CB24539/5CC637E9/5CC6DD48" Ref="U?"  Part="5" 
AR Path="/5CB3ED8C/5CC6DD48" Ref="U8"  Part="5" 
F 0 "U8" H 8558 2796 50  0000 L CNN
F 1 "OPA4340EA" H 8558 2705 50  0000 L CNN
F 2 "digikey-footprints:SSOP-16_W3.90mm" H 8600 2750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa340.pdf" H 8600 2750 50  0001 C CNN
	5    8600 2750
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OPA4340EA U?
U 5 1 5CC6DD56
P 10100 2750
AR Path="/5CBABA99/5CC6DD56" Ref="U?"  Part="5" 
AR Path="/5CC6DD56" Ref="U?"  Part="5" 
AR Path="/5CC637E9/5CC6DD56" Ref="U26"  Part="5" 
AR Path="/5CB24539/5CC637E9/5CC6DD56" Ref="U?"  Part="5" 
AR Path="/5CB3ED8C/5CC6DD56" Ref="U26"  Part="5" 
F 0 "U26" H 10058 2796 50  0000 L CNN
F 1 "OPA4340EA" H 10058 2705 50  0000 L CNN
F 2 "digikey-footprints:SSOP-16_W3.90mm" H 10100 2750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa340.pdf" H 10100 2750 50  0001 C CNN
	5    10100 2750
	1    0    0    -1  
$EndComp
Connection ~ 9250 3050
$Comp
L power:GND #PWR?
U 1 1 5CC6DD5E
P 9250 3050
AR Path="/5CBABA99/5CC6DD5E" Ref="#PWR?"  Part="1" 
AR Path="/5CC6DD5E" Ref="#PWR?"  Part="1" 
AR Path="/5CC637E9/5CC6DD5E" Ref="#PWR016"  Part="1" 
AR Path="/5CB24539/5CC637E9/5CC6DD5E" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5CC6DD5E" Ref="#PWR0107"  Part="1" 
F 0 "#PWR0107" H 9250 2800 50  0001 C CNN
F 1 "GND" H 9255 2877 50  0000 C CNN
F 2 "" H 9250 3050 50  0001 C CNN
F 3 "" H 9250 3050 50  0001 C CNN
	1    9250 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 2450 10000 2450
Wire Wire Line
	9250 2450 9500 2450
Connection ~ 9500 2450
Wire Wire Line
	10000 3050 9500 3050
Wire Wire Line
	9500 3050 9250 3050
Connection ~ 9500 3050
$Comp
L Amplifier_Operational:OPA4340EA U?
U 5 1 5CC6DD6A
P 9600 2750
AR Path="/5CBABA99/5CC6DD6A" Ref="U?"  Part="5" 
AR Path="/5CC6DD6A" Ref="U?"  Part="5" 
AR Path="/5CC637E9/5CC6DD6A" Ref="U20"  Part="5" 
AR Path="/5CB24539/5CC637E9/5CC6DD6A" Ref="U?"  Part="5" 
AR Path="/5CB3ED8C/5CC6DD6A" Ref="U20"  Part="5" 
F 0 "U20" H 9558 2796 50  0000 L CNN
F 1 "OPA4340EA" H 9558 2705 50  0000 L CNN
F 2 "digikey-footprints:SSOP-16_W3.90mm" H 9600 2750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa340.pdf" H 9600 2750 50  0001 C CNN
	5    9600 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 2450 9000 2450
Wire Wire Line
	9000 2450 9250 2450
Connection ~ 9000 2450
Wire Wire Line
	9000 3050 8500 3050
Wire Wire Line
	9250 3050 9000 3050
Connection ~ 9000 3050
$Comp
L Amplifier_Operational:OPA4340EA U?
U 5 1 5CC6DD77
P 9100 2750
AR Path="/5CC6DD77" Ref="U?"  Part="5" 
AR Path="/5CC637E9/5CC6DD77" Ref="U14"  Part="5" 
AR Path="/5CB24539/5CC637E9/5CC6DD77" Ref="U?"  Part="5" 
AR Path="/5CB3ED8C/5CC6DD77" Ref="U14"  Part="5" 
F 0 "U14" H 9058 2796 50  0000 L CNN
F 1 "OPA4340EA" H 9058 2705 50  0000 L CNN
F 2 "digikey-footprints:SSOP-16_W3.90mm" H 9100 2750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa340.pdf" H 9100 2750 50  0001 C CNN
	5    9100 2750
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OPA4340EA U?
U 5 1 5CC6DD7E
P 10050 3900
AR Path="/5CBABA99/5CC6DD7E" Ref="U?"  Part="5" 
AR Path="/5CC6DD7E" Ref="U?"  Part="5" 
AR Path="/5CC637E9/5CC6DD7E" Ref="U23"  Part="5" 
AR Path="/5CB24539/5CC637E9/5CC6DD7E" Ref="U?"  Part="5" 
AR Path="/5CB3ED8C/5CC6DD7E" Ref="U23"  Part="5" 
F 0 "U23" H 10008 3946 50  0000 L CNN
F 1 "OPA4340EA" H 10008 3855 50  0000 L CNN
F 2 "digikey-footprints:SSOP-16_W3.90mm" H 10050 3900 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa340.pdf" H 10050 3900 50  0001 C CNN
	5    10050 3900
	1    0    0    -1  
$EndComp
Connection ~ 9200 3600
$Comp
L power:+3.3V #PWR?
U 1 1 5CC6DD86
P 9200 3600
AR Path="/5CBABA99/5CC6DD86" Ref="#PWR?"  Part="1" 
AR Path="/5CC6DD86" Ref="#PWR?"  Part="1" 
AR Path="/5CC637E9/5CC6DD86" Ref="#PWR013"  Part="1" 
AR Path="/5CB24539/5CC637E9/5CC6DD86" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5CC6DD86" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0108" H 9200 3450 50  0001 C CNN
F 1 "+3.3V" H 9215 3773 50  0000 C CNN
F 2 "" H 9200 3600 50  0001 C CNN
F 3 "" H 9200 3600 50  0001 C CNN
	1    9200 3600
	1    0    0    -1  
$EndComp
Connection ~ 9200 4200
$Comp
L power:GND #PWR?
U 1 1 5CC6DD8D
P 9200 4200
AR Path="/5CBABA99/5CC6DD8D" Ref="#PWR?"  Part="1" 
AR Path="/5CC6DD8D" Ref="#PWR?"  Part="1" 
AR Path="/5CC637E9/5CC6DD8D" Ref="#PWR014"  Part="1" 
AR Path="/5CB24539/5CC637E9/5CC6DD8D" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5CC6DD8D" Ref="#PWR0109"  Part="1" 
F 0 "#PWR0109" H 9200 3950 50  0001 C CNN
F 1 "GND" H 9205 4027 50  0000 C CNN
F 2 "" H 9200 4200 50  0001 C CNN
F 3 "" H 9200 4200 50  0001 C CNN
	1    9200 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 3600 9950 3600
Wire Wire Line
	9200 3600 9450 3600
Connection ~ 9450 3600
Wire Wire Line
	9950 4200 9450 4200
Wire Wire Line
	9450 4200 9200 4200
Connection ~ 9450 4200
$Comp
L Amplifier_Operational:OPA4340EA U?
U 5 1 5CC6DD99
P 9550 3900
AR Path="/5CBABA99/5CC6DD99" Ref="U?"  Part="5" 
AR Path="/5CC6DD99" Ref="U?"  Part="5" 
AR Path="/5CC637E9/5CC6DD99" Ref="U17"  Part="5" 
AR Path="/5CB24539/5CC637E9/5CC6DD99" Ref="U?"  Part="5" 
AR Path="/5CB3ED8C/5CC6DD99" Ref="U17"  Part="5" 
F 0 "U17" H 9508 3946 50  0000 L CNN
F 1 "OPA4340EA" H 9508 3855 50  0000 L CNN
F 2 "digikey-footprints:SSOP-16_W3.90mm" H 9550 3900 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa340.pdf" H 9550 3900 50  0001 C CNN
	5    9550 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 3600 8950 3600
Wire Wire Line
	8950 3600 9200 3600
Connection ~ 8950 3600
Wire Wire Line
	8950 4200 8450 4200
Wire Wire Line
	9200 4200 8950 4200
Connection ~ 8950 4200
$Comp
L Amplifier_Operational:OPA4340EA U?
U 5 1 5CC6DDA6
P 9050 3900
AR Path="/5CC6DDA6" Ref="U?"  Part="5" 
AR Path="/5CC637E9/5CC6DDA6" Ref="U11"  Part="5" 
AR Path="/5CB24539/5CC637E9/5CC6DDA6" Ref="U?"  Part="5" 
AR Path="/5CB3ED8C/5CC6DDA6" Ref="U11"  Part="5" 
F 0 "U11" H 9008 3946 50  0000 L CNN
F 1 "OPA4340EA" H 9008 3855 50  0000 L CNN
F 2 "digikey-footprints:SSOP-16_W3.90mm" H 9050 3900 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa340.pdf" H 9050 3900 50  0001 C CNN
	5    9050 3900
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5CC6EBC6
P 5500 4650
AR Path="/5CC6EBC6" Ref="#PWR?"  Part="1" 
AR Path="/5CC637E9/5CC6EBC6" Ref="#PWR011"  Part="1" 
AR Path="/5CB24539/5CC637E9/5CC6EBC6" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5CC6EBC6" Ref="#PWR0110"  Part="1" 
F 0 "#PWR0110" H 5500 4500 50  0001 C CNN
F 1 "+3.3V" H 5515 4823 50  0000 C CNN
F 2 "" H 5500 4650 50  0001 C CNN
F 3 "" H 5500 4650 50  0001 C CNN
	1    5500 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 4700 5500 4650
$Comp
L power:+3.3V #PWR?
U 1 1 5CC6EBCD
P 3500 4850
AR Path="/5CC6EBCD" Ref="#PWR?"  Part="1" 
AR Path="/5CC637E9/5CC6EBCD" Ref="#PWR07"  Part="1" 
AR Path="/5CB24539/5CC637E9/5CC6EBCD" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5CC6EBCD" Ref="#PWR0111"  Part="1" 
F 0 "#PWR0111" H 3500 4700 50  0001 C CNN
F 1 "+3.3V" H 3515 5023 50  0000 C CNN
F 2 "" H 3500 4850 50  0001 C CNN
F 3 "" H 3500 4850 50  0001 C CNN
	1    3500 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 4900 3500 4900
Wire Wire Line
	3500 4900 3500 4850
Wire Wire Line
	3900 5700 3600 5700
Wire Wire Line
	3600 5700 3600 5800
Wire Wire Line
	3600 5800 3900 5800
$Comp
L power:GND #PWR?
U 1 1 5CC6EBD8
P 3600 5800
AR Path="/5CC6EBD8" Ref="#PWR?"  Part="1" 
AR Path="/5CC637E9/5CC6EBD8" Ref="#PWR08"  Part="1" 
AR Path="/5CB24539/5CC637E9/5CC6EBD8" Ref="#PWR?"  Part="1" 
AR Path="/5CB3ED8C/5CC6EBD8" Ref="#PWR0112"  Part="1" 
F 0 "#PWR0112" H 3600 5550 50  0001 C CNN
F 1 "GND" H 3605 5627 50  0000 C CNN
F 2 "" H 3600 5800 50  0001 C CNN
F 3 "" H 3600 5800 50  0001 C CNN
	1    3600 5800
	1    0    0    -1  
$EndComp
Connection ~ 3600 5800
Wire Wire Line
	4050 2850 3750 2850
Connection ~ 3750 2850
NoConn ~ 3900 5100
NoConn ~ 3900 4600
NoConn ~ 4050 3250
NoConn ~ 4050 2150
NoConn ~ 4050 1650
NoConn ~ 3900 6200
$Comp
L ADG732BCPZ:ADG732BCPZ U?
U 2 1 5CB13B1E
P 4050 1550
AR Path="/5CB13B1E" Ref="U?"  Part="2" 
AR Path="/5CC637E9/5CB13B1E" Ref="U27"  Part="2" 
AR Path="/5CB24539/5CC637E9/5CB13B1E" Ref="U?"  Part="2" 
AR Path="/5CB3ED8C/5CB13B1E" Ref="U27"  Part="2" 
F 0 "U27" H 4850 1937 60  0000 C CNN
F 1 "ADG732BCPZ" H 4850 1831 60  0000 C CNN
F 2 "footprints:ADG732BCPZ" H 4850 1890 60  0001 C CNN
F 3 "" H 4050 1550 60  0000 C CNN
	2    4050 1550
	1    0    0    -1  
$EndComp
$Comp
L ADG732BCPZ:ADG732BCPZ U?
U 2 1 5CBB1340
P 3900 4500
AR Path="/5CBB1340" Ref="U?"  Part="2" 
AR Path="/5CC637E9/5CBB1340" Ref="U?"  Part="1" 
AR Path="/5CB24539/5CBB1340" Ref="U?"  Part="1" 
AR Path="/5CB3ED8C/5CBB1340" Ref="U1"  Part="2" 
F 0 "U1" H 4400 5050 60  0000 R CNN
F 1 "ADG732BCPZ" H 4600 4900 60  0000 R CNN
F 2 "footprints:ADG732BCPZ" H 4700 4840 60  0001 C CNN
F 3 "" H 3900 4500 60  0000 C CNN
	2    3900 4500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
