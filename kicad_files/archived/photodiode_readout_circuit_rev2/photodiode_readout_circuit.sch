EESchema Schematic File Version 4
LIBS:photodiode_readout_circuit-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 4
Title "SiC Photodiode Readout Circuitry"
Date "2019-03-28"
Rev "V0"
Comp "CoolCAD Electronics, LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "MUX/DEMUX for Photodiode Array"
$EndDescr
Wire Wire Line
	1050 4400 1050 4500
Connection ~ 1050 4500
Wire Wire Line
	1050 4500 1050 4600
$Comp
L power:GND #PWR01
U 1 1 5C8C0501
P 600 4500
F 0 "#PWR01" H 600 4250 50  0001 C CNN
F 1 "GND" H 605 4327 50  0000 C CNN
F 2 "" H 600 4500 50  0001 C CNN
F 3 "" H 600 4500 50  0001 C CNN
	1    600  4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 4500 600  4500
$Sheet
S 2850 3350 1600 450 
U 5C9AAC30
F0 "Trans-impedance Amplifier Array A" 59
F1 "trans_impedance_amp_arr.sch" 31
F2 "N1" I B 2900 3800 50 
F3 "N2" I B 3000 3800 50 
F4 "N3" I B 3100 3800 50 
F5 "N4" I B 3200 3800 50 
F6 "N5" I B 3300 3800 50 
F7 "N6" I B 3400 3800 50 
F8 "N7" I B 3500 3800 50 
F9 "N8" I B 3600 3800 50 
F10 "N9" I B 3700 3800 50 
F11 "N10" I B 3800 3800 50 
F12 "N11" I B 3900 3800 50 
F13 "N12" I B 4000 3800 50 
F14 "N13" I B 4100 3800 50 
F15 "N14" I B 4200 3800 50 
F16 "N15" I B 4300 3800 50 
F17 "N16" I B 4400 3800 50 
F18 "O4" O T 3200 3350 50 
F19 "O3" O T 3100 3350 50 
F20 "O2" O T 3000 3350 50 
F21 "O1" O T 2900 3350 50 
F22 "O8" O T 3600 3350 50 
F23 "O7" O T 3500 3350 50 
F24 "O6" O T 3400 3350 50 
F25 "O5" O T 3300 3350 50 
F26 "O11" O T 3900 3350 50 
F27 "O10" O T 3800 3350 50 
F28 "O9" O T 3700 3350 50 
F29 "O16" O T 4400 3350 50 
F30 "O15" O T 4300 3350 50 
F31 "O14" O T 4200 3350 50 
F32 "O13" O T 4100 3350 50 
F33 "O12" O T 4000 3350 50 
$EndSheet
Wire Wire Line
	2900 3900 2900 3800
Wire Wire Line
	3000 3900 3000 3800
Wire Wire Line
	3100 3900 3100 3800
Wire Wire Line
	3200 3900 3200 3800
Wire Wire Line
	3300 3900 3300 3800
Wire Wire Line
	3400 3900 3400 3800
Wire Wire Line
	3500 3900 3500 3800
Wire Wire Line
	3600 3900 3600 3800
Wire Wire Line
	3700 3900 3700 3800
Wire Wire Line
	3800 3900 3800 3800
Wire Wire Line
	3900 3900 3900 3800
Wire Wire Line
	4000 3900 4000 3800
Wire Wire Line
	4100 3800 4100 3900
Wire Wire Line
	4200 3800 4200 3900
Wire Wire Line
	4300 3900 4300 3800
Wire Wire Line
	4400 3900 4400 3800
$Sheet
S 4550 3350 1600 450 
U 5CB83508
F0 "Trans-impdance Amplifier Array B" 28
F1 "trans_impedance_amp_arr.sch" 31
F2 "N1" I B 4600 3800 50 
F3 "N2" I B 4700 3800 50 
F4 "N3" I B 4800 3800 50 
F5 "N4" I B 4900 3800 50 
F6 "N5" I B 5000 3800 50 
F7 "N6" I B 5100 3800 50 
F8 "N7" I B 5200 3800 50 
F9 "N8" I B 5300 3800 50 
F10 "N9" I B 5400 3800 50 
F11 "N10" I B 5500 3800 50 
F12 "N11" I B 5600 3800 50 
F13 "N12" I B 5700 3800 50 
F14 "N13" I B 5800 3800 50 
F15 "N14" I B 5900 3800 50 
F16 "N15" I B 6000 3800 50 
F17 "N16" I B 6100 3800 50 
F18 "O4" O T 4900 3350 50 
F19 "O3" O T 4800 3350 50 
F20 "O2" O T 4700 3350 50 
F21 "O1" O T 4600 3350 50 
F22 "O8" O T 5300 3350 50 
F23 "O7" O T 5200 3350 50 
F24 "O6" O T 5100 3350 50 
F25 "O5" O T 5000 3350 50 
F26 "O11" O T 5600 3350 50 
F27 "O10" O T 5500 3350 50 
F28 "O9" O T 5400 3350 50 
F29 "O16" O T 6100 3350 50 
F30 "O15" O T 6000 3350 50 
F31 "O14" O T 5900 3350 50 
F32 "O13" O T 5800 3350 50 
F33 "O12" O T 5700 3350 50 
$EndSheet
Wire Wire Line
	4700 3900 4700 3800
Wire Wire Line
	4800 3900 4800 3800
Wire Wire Line
	4900 3900 4900 3800
Wire Wire Line
	5000 3900 5000 3800
Wire Wire Line
	5100 3900 5100 3800
Wire Wire Line
	5200 3900 5200 3800
Wire Wire Line
	5300 3900 5300 3800
Wire Wire Line
	5400 3900 5400 3800
Wire Wire Line
	5500 3900 5500 3800
Wire Wire Line
	5600 3900 5600 3800
Wire Wire Line
	5700 3800 5700 3900
Wire Wire Line
	5800 3800 5800 3900
Wire Wire Line
	4600 3900 4600 3800
Wire Wire Line
	5900 3800 5900 3900
Wire Wire Line
	6000 3800 6000 3900
Wire Wire Line
	6100 3900 6100 3800
Wire Wire Line
	2950 2950 2900 2950
Wire Wire Line
	2900 2950 2900 3350
Wire Wire Line
	3000 2950 3050 2950
Wire Wire Line
	3150 2950 3100 2950
Wire Wire Line
	3100 2950 3100 3350
Wire Wire Line
	3200 3350 3200 2950
Wire Wire Line
	3200 2950 3250 2950
Wire Wire Line
	3350 2950 3300 2950
Wire Wire Line
	3300 2950 3300 3350
Wire Wire Line
	3450 2950 3400 2950
Wire Wire Line
	3400 2950 3400 3350
Wire Wire Line
	3550 2950 3500 2950
Wire Wire Line
	3500 2950 3500 3350
Wire Wire Line
	3650 2950 3600 2950
Wire Wire Line
	3600 2950 3600 3350
Wire Wire Line
	3700 3350 3700 2950
Wire Wire Line
	3700 2950 3750 2950
Wire Wire Line
	3850 2950 3800 2950
Wire Wire Line
	3800 2950 3800 3350
Wire Wire Line
	3900 3350 3900 2950
Wire Wire Line
	3900 2950 3950 2950
Wire Wire Line
	4050 2950 4000 2950
Wire Wire Line
	4000 2950 4000 3350
Wire Wire Line
	4150 2950 4100 2950
Wire Wire Line
	4100 2950 4100 3350
Wire Wire Line
	4250 2950 4200 2950
Wire Wire Line
	4200 2950 4200 3350
Wire Wire Line
	4350 2950 4300 2950
Wire Wire Line
	4300 2950 4300 3350
Wire Wire Line
	4400 3350 4400 2950
Wire Wire Line
	4400 2950 4450 2950
Wire Wire Line
	4550 2950 4600 2950
Wire Wire Line
	4600 2950 4600 3350
Wire Wire Line
	4700 3350 4700 2950
Wire Wire Line
	4700 2950 4650 2950
Wire Wire Line
	4750 2950 4800 2950
Wire Wire Line
	4800 2950 4800 3350
Wire Wire Line
	4900 3350 4900 2950
Wire Wire Line
	4900 2950 4850 2950
Wire Wire Line
	4950 2950 5000 2950
Wire Wire Line
	5000 2950 5000 3350
Wire Wire Line
	5100 3350 5100 2950
Wire Wire Line
	5100 2950 5050 2950
Wire Wire Line
	5150 2950 5200 2950
Wire Wire Line
	5200 2950 5200 3350
Wire Wire Line
	5300 3350 5300 2950
Wire Wire Line
	5300 2950 5250 2950
Wire Wire Line
	5350 2950 5400 2950
Wire Wire Line
	5400 2950 5400 3350
Wire Wire Line
	5500 3350 5500 2950
Wire Wire Line
	5500 2950 5450 2950
Wire Wire Line
	5550 2950 5600 2950
Wire Wire Line
	5600 2950 5600 3350
Wire Wire Line
	5700 3350 5700 2950
Wire Wire Line
	5700 2950 5650 2950
Wire Wire Line
	5750 2950 5800 2950
Wire Wire Line
	5800 2950 5800 3350
Wire Wire Line
	5900 3350 5900 2950
Wire Wire Line
	5900 2950 5850 2950
Wire Wire Line
	5950 2950 6000 2950
Wire Wire Line
	6000 2950 6000 3350
Wire Wire Line
	6100 3350 6100 2950
Wire Wire Line
	6100 2950 6050 2950
Connection ~ 3250 1350
Wire Wire Line
	3250 1350 3250 850 
Wire Wire Line
	3250 850  3050 850 
Wire Wire Line
	3150 1350 3250 1350
Wire Wire Line
	3250 1350 3350 1350
Text Label 1050 5400 2    50   ~ 0
D1
Text Label 1050 5300 2    50   ~ 0
D2
Text Label 1050 5200 2    50   ~ 0
D3
Text Label 1050 5100 2    50   ~ 0
D4
Text Label 1050 5000 2    50   ~ 0
D5
Text Label 1050 4300 2    50   ~ 0
D6
Text Label 4150 1350 1    50   ~ 0
D6
Text Label 4050 1350 1    50   ~ 0
D7
Text Label 3950 1350 1    50   ~ 0
D8
Text Label 3850 1350 1    50   ~ 0
D9
Text Label 3750 1350 1    50   ~ 0
D10
Text Label 3050 1350 1    50   ~ 0
D11
$Comp
L power:GND #PWR03
U 1 1 5CBAB511
P 3050 850
F 0 "#PWR03" H 3050 600 50  0001 C CNN
F 1 "GND" H 3055 677 50  0000 C CNN
F 2 "" H 3050 850 50  0001 C CNN
F 3 "" H 3050 850 50  0001 C CNN
	1    3050 850 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5CBAB542
P 1050 5700
F 0 "#PWR02" H 1050 5450 50  0001 C CNN
F 1 "GND" H 1055 5527 50  0000 C CNN
F 2 "" H 1050 5700 50  0001 C CNN
F 3 "" H 1050 5700 50  0001 C CNN
	1    1050 5700
	1    0    0    -1  
$EndComp
Text Label 4450 1350 1    50   ~ 0
A1
Wire Wire Line
	3000 2950 3000 3350
$Comp
L ADG732BCPZ:ADG732BCPZ U27
U 1 1 5CC31BF4
P 1050 4000
F 0 "U27" H 1850 4487 60  0000 C CNN
F 1 "ADG732BCPZ" H 1850 4381 60  0000 C CNN
F 2 "footprints:ADG732BCPZ" H 1850 4340 60  0001 C CNN
F 3 "" H 1050 4000 60  0000 C CNN
	1    1050 4000
	1    0    0    -1  
$EndComp
$Sheet
S 9150 4900 1050 950 
U 5CC637E9
F0 "Power" 50
F1 "power.sch" 50
$EndSheet
$Comp
L ADG732BCPZ:ADG732BCPZ U28
U 1 1 5CC6FBD2
P 2750 1350
AR Path="/5CC6FBD2" Ref="U28"  Part="1" 
AR Path="/5CC637E9/5CC6FBD2" Ref="U?"  Part="1" 
F 0 "U28" V 3497 -2178 60  0000 R CNN
F 1 "ADG732BCPZ" V 3603 -2178 60  0000 R CNN
F 2 "footprints:ADG732BCPZ" H 3550 1690 60  0001 C CNN
F 3 "" H 2750 1350 60  0000 C CNN
	1    2750 1350
	0    -1   1    0   
$EndComp
$Comp
L photodiode_readout_circuit:photodiode_array U2
U 1 1 5CAB9E1B
P 4450 5750
F 0 "U2" H 4450 6150 50  0000 L CNN
F 1 "photodiode_array" H 4450 6000 50  0000 L CNN
F 2 "Package_QFP:LQFP-64_7x7mm_P0.4mm" H 4700 5650 50  0001 C CNN
F 3 "" H 4700 5650 50  0001 C CNN
	1    4450 5750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J1
U 1 1 5CB459D3
P 8050 1350
F 0 "J1" H 8130 1342 50  0000 L CNN
F 1 "Conn_01x08" H 8130 1251 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x08_P2.54mm_Vertical" H 8050 1350 50  0001 C CNN
F 3 "~" H 8050 1350 50  0001 C CNN
	1    8050 1350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J4
U 1 1 5CB45B29
P 9550 2450
F 0 "J4" H 9470 2967 50  0000 C CNN
F 1 "Conn_01x08" H 9470 2876 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x08_P2.54mm_Vertical" H 9550 2450 50  0001 C CNN
F 3 "~" H 9550 2450 50  0001 C CNN
	1    9550 2450
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x10 J3
U 1 1 5CB4AF82
P 9550 1300
F 0 "J3" H 9470 1917 50  0000 C CNN
F 1 "Conn_01x10" H 9470 1826 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x10_P2.54mm_Vertical" H 9550 1300 50  0001 C CNN
F 3 "~" H 9550 1300 50  0001 C CNN
	1    9550 1300
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J2
U 1 1 5CB4B0D7
P 8050 2350
F 0 "J2" H 8130 2342 50  0000 L CNN
F 1 "Conn_01x06" H 8130 2251 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Vertical" H 8050 2350 50  0001 C CNN
F 3 "~" H 8050 2350 50  0001 C CNN
	1    8050 2350
	1    0    0    -1  
$EndComp
NoConn ~ 7850 1050
NoConn ~ 7850 1150
NoConn ~ 7850 1250
NoConn ~ 7850 1450
NoConn ~ 7850 1750
NoConn ~ 7850 2350
NoConn ~ 7850 2450
NoConn ~ 7850 2550
NoConn ~ 7850 2650
NoConn ~ 9750 900 
NoConn ~ 9750 1000
NoConn ~ 9750 1100
NoConn ~ 9750 1300
NoConn ~ 9750 1400
$Comp
L power:+3.3V #PWR0101
U 1 1 5CB6F99F
P 7450 1300
F 0 "#PWR0101" H 7450 1150 50  0001 C CNN
F 1 "+3.3V" H 7465 1473 50  0000 C CNN
F 2 "" H 7450 1300 50  0001 C CNN
F 3 "" H 7450 1300 50  0001 C CNN
	1    7450 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 1350 7450 1350
Wire Wire Line
	7450 1350 7450 1300
$Comp
L power:GND #PWR0102
U 1 1 5CB724FE
P 7400 1800
F 0 "#PWR0102" H 7400 1550 50  0001 C CNN
F 1 "GND" H 7405 1627 50  0000 C CNN
F 2 "" H 7400 1800 50  0001 C CNN
F 3 "" H 7400 1800 50  0001 C CNN
	1    7400 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 1650 7400 1650
Wire Wire Line
	7400 1650 7400 1800
Wire Wire Line
	7400 1650 7400 1550
Wire Wire Line
	7400 1550 7850 1550
Connection ~ 7400 1650
Wire Wire Line
	9750 1200 9950 1200
$Comp
L power:GND #PWR0103
U 1 1 5CB7AE00
P 9950 1200
F 0 "#PWR0103" H 9950 950 50  0001 C CNN
F 1 "GND" H 9955 1027 50  0000 C CNN
F 2 "" H 9950 1200 50  0001 C CNN
F 3 "" H 9950 1200 50  0001 C CNN
	1    9950 1200
	1    0    0    -1  
$EndComp
Text Label 9750 1500 0    50   ~ 0
D11
Text Label 9750 1600 0    50   ~ 0
D10
Text Label 9750 1700 0    50   ~ 0
D9
Text Label 9750 1800 0    50   ~ 0
D8
Text Label 9750 2150 0    50   ~ 0
D7
Text Label 9750 2250 0    50   ~ 0
D6
Text Label 9750 2350 0    50   ~ 0
D5
Text Label 9750 2450 0    50   ~ 0
D4
Text Label 9750 2550 0    50   ~ 0
D3
Text Label 9750 2650 0    50   ~ 0
D2
Text Label 9750 2750 0    50   ~ 0
D1
Text Label 7850 2250 2    50   ~ 0
A1
Text GLabel 7500 2150 0    50   Input ~ 0
bias_op_amps
Wire Wire Line
	7500 2150 7850 2150
Text GLabel 9750 2850 2    50   Input ~ 0
reset_op_amps
Text Notes 9050 1950 2    50   ~ 10
Arduino Pin-out\n
$EndSCHEMATC
