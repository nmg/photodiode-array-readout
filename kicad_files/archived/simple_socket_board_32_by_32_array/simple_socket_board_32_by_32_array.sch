EESchema Schematic File Version 4
LIBS:simple_socket_board_32_by_32_array-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "QFN-64 Socket Board for UV 32x32 Photodiode Array"
Date "2019-05-21"
Rev "Revision 0"
Comp "CoolCAD Electronics LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "Nevo Magnezi"
$EndDescr
$Comp
L SamacSys_Parts:264-5205-01 J5
U 1 1 5CE4410F
P 4950 2000
F 0 "J5" H 5400 2265 50  0000 C CNN
F 1 "264-5205-01" H 5400 2174 50  0000 C CNN
F 2 "SamacSys_Parts:264-5205-01_1" H 5700 2100 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/264-5205-01.pdf" H 5700 2000 50  0001 L CNN
F 4 "3M - 264-5205-01 - IC SOCKET, QFN, 64 POSITION, 0.5MM, THROUGH HOLE" H 5700 1900 50  0001 L CNN "Description"
F 5 "" H 5700 1800 50  0001 L CNN "Height"
F 6 "517-264-5205-01" H 5700 1700 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=517-264-5205-01" H 5700 1600 50  0001 L CNN "Mouser Price/Stock"
F 8 "3M" H 5700 1500 50  0001 L CNN "Manufacturer_Name"
F 9 "264-5205-01" H 5700 1400 50  0001 L CNN "Manufacturer_Part_Number"
	1    4950 2000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J6
U 1 1 5CE4444F
P 6050 2400
F 0 "J6" H 6077 2376 50  0000 L CNN
F 1 "Conn_01x08_Female" H 6077 2285 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 6050 2400 50  0001 C CNN
F 3 "~" H 6050 2400 50  0001 C CNN
	1    6050 2400
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J7
U 1 1 5CE44504
P 6050 3200
F 0 "J7" H 6077 3176 50  0000 L CNN
F 1 "Conn_01x08_Female" H 6077 3085 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 6050 3200 50  0001 C CNN
F 3 "~" H 6050 3200 50  0001 C CNN
	1    6050 3200
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J8
U 1 1 5CE445CA
P 6050 4000
F 0 "J8" H 6077 3976 50  0000 L CNN
F 1 "Conn_01x08_Female" H 6077 3885 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 6050 4000 50  0001 C CNN
F 3 "~" H 6050 4000 50  0001 C CNN
	1    6050 4000
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J9
U 1 1 5CE445D1
P 6050 4800
F 0 "J9" H 6077 4776 50  0000 L CNN
F 1 "Conn_01x08_Female" H 6077 4685 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 6050 4800 50  0001 C CNN
F 3 "~" H 6050 4800 50  0001 C CNN
	1    6050 4800
	1    0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Female J1
U 1 1 5CE44772
P 4750 2300
F 0 "J1" H 4777 2276 50  0000 L CNN
F 1 "Conn_01x08_Female" H 4777 2185 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 4750 2300 50  0001 C CNN
F 3 "~" H 4750 2300 50  0001 C CNN
	1    4750 2300
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J2
U 1 1 5CE44779
P 4750 3100
F 0 "J2" H 4777 3076 50  0000 L CNN
F 1 "Conn_01x08_Female" H 4777 2985 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 4750 3100 50  0001 C CNN
F 3 "~" H 4750 3100 50  0001 C CNN
	1    4750 3100
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J3
U 1 1 5CE44780
P 4750 3900
F 0 "J3" H 4777 3876 50  0000 L CNN
F 1 "Conn_01x08_Female" H 4777 3785 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 4750 3900 50  0001 C CNN
F 3 "~" H 4750 3900 50  0001 C CNN
	1    4750 3900
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J4
U 1 1 5CE44787
P 4750 4700
F 0 "J4" H 4777 4676 50  0000 L CNN
F 1 "Conn_01x08_Female" H 4777 4585 50  0000 L CNN
F 2 "Socket_Arduino_Uno:Socket_Strip_Arduino_1x08" H 4750 4700 50  0001 C CNN
F 3 "~" H 4750 4700 50  0001 C CNN
	1    4750 4700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5300 5450 5300 5550
Wire Wire Line
	5300 5550 5400 5550
Wire Wire Line
	5500 5550 5500 5450
Wire Wire Line
	5400 5450 5400 5550
Connection ~ 5400 5550
Wire Wire Line
	5400 5550 5500 5550
$Comp
L power:GND #PWR?
U 1 1 5CED5B07
P 5400 5550
F 0 "#PWR?" H 5400 5300 50  0001 C CNN
F 1 "GND" H 5405 5377 50  0000 C CNN
F 2 "" H 5400 5550 50  0001 C CNN
F 3 "" H 5400 5550 50  0001 C CNN
	1    5400 5550
	1    0    0    -1  
$EndComp
$EndSCHEMATC
